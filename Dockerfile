FROM microsoft/dotnet:2.1-aspnetcore-runtime

ARG VERSION=0.1

ENV ENVIRONMENT=Production
ENV VERSION=${VERSION}

ENV INFLUX=influx:8086
ENV INFLUX_METRICS_DB=_batapi

ENV KAFKA=kafka:9092

ENV DB=database
ENV DB_USER=sa
ENV DB_PASSWORD=pass
ENV PRIME_OPERATOR_ID=bf46efc7-49a8-495b-856c-e8ffbd762f66
ENV COGNITO_DOMAIN=batid-dev
ENV COGNITO_REGION=eu-central-1
ENV COGNITO_POOL_ID=eu-central-1_271lLNsx2
ENV IAM_ID=AKIAICJEM3DRAQ3ZPE3A
ENV IAM_SECRET=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

ENV SENTRY_DSN=https://d1972b224df74b51ab9acd47127ddecb@sentry.io/1382977

ENV ASPNETCORE_ENVIRONMENT=${ENVIRONMENT}
ENV ASPNETCORE_URLS "http://*:80/"

WORKDIR /app
VOLUME /app/logs
COPY Bat.Api/Production/ .
COPY startup.sh .
RUN \
   apt update \
   && apt -y install gettext-base   
RUN for f in *.json; do cp "$f" "$f.in"; done \
    && for f in *.config; do cp "$f" "$f.in"; done 
RUN chmod a+x startup.sh

EXPOSE 80
ENTRYPOINT ["./startup.sh"]