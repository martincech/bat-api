﻿using System;
using System.Collections.Generic;

namespace MigrationV07.Model.Models
{
    public partial class Company
    {
        public Guid Id { get; set; }

        public Company()
        {
            Devices = new List<Device>();
        }

        public virtual ICollection<Device> Devices { get; }
    }
}
