﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MigrationV07.Model.Models
{
    public partial class Device
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string OldUid { get; set; }


        public virtual Company Company { get; set; }
    }
}
