﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using MigrationV07.Model.Models;

namespace MigrationV07.Model
{
    public class MigrationV07ModelContainer : DbContext, IDesignTimeDbContextFactory<MigrationV07ModelContainer>
    {
        public MigrationV07ModelContainer()
        {
        }

        public MigrationV07ModelContainer(DbContextOptions<MigrationV07ModelContainer> options)
            : base(options)
        {
        }


        public IQueryable<Company> CompaniesLazy => Companies
            .Include(i => i.Devices);


        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Device> Devices { get; set; }


        public MigrationV07ModelContainer CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MigrationV07ModelContainer>();
            builder.UseSqlServer("data source=localhost;initial catalog=VeitBatCloudMigration07;integrated security=False;user id=sa;password=Passw0rd!;multipleactiveresultsets=True;application name=Web");
            return new MigrationV07ModelContainer(builder.Options);
        }
    }
}
