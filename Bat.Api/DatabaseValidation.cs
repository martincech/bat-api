﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Bat.Cloud.Model;
using Bird.Model;
using Microsoft.EntityFrameworkCore;
using MigrationV07.Model;
using User.Model;
using User.Model.Enums;
using Bat.Api.Extensions;

namespace Bat.Api
{
    public class DatabaseValidation
    {
        private readonly UserModelContainer userModel;
        private readonly BirdModelContainer birdModel;
        private readonly MigrationV07ModelContainer migration07Model;

        public DatabaseValidation(UserModelContainer userModel,
                                  BirdModelContainer birdModel,
                                  MigrationV07ModelContainer migration07Model)
        {
            this.userModel = userModel ?? throw new ArgumentNullException(nameof(userModel));
            this.birdModel = birdModel ?? throw new ArgumentNullException(nameof(birdModel));
            this.migration07Model = migration07Model ?? throw new ArgumentNullException(nameof(migration07Model));
        }


        public void MigrateDatabases(string batModelConnectionString, string primeOperator)
        {
            //user DB must be ensured of creation before check migration other DBs
            var dbs = new List<DbContext>
            {
                userModel,
                birdModel,
                migration07Model
            };
            MigrateDb(dbs);

            birdModel.Seed();

            var companyContexts = GetAllComanyContexts(batModelConnectionString);
            MigrateDb(companyContexts);
            foreach (var context in companyContexts)
            {
                context.Dispose();
            }

            EnsureDefaultOperator(primeOperator);
        }

        private void EnsureDefaultOperator(string primeOperator)
        {
            if (!Guid.TryParse(primeOperator, out var primeOperatorId))
            {
                throw new ArgumentOutOfRangeException($"Create default user: [{primeOperator}] is not valid GUID!");
            }

            var defaultOperator = userModel.Users.FirstOrDefault(f => f.Id == primeOperatorId);
            if (defaultOperator != null)
            {
                return;
            }

            InsertPrimeOperator(primeOperatorId);
        }

        private void InsertPrimeOperator(Guid primeOperatorId)
        {
            var veitOperator = new User.Model.Models.User { Id = primeOperatorId };
            var roleName = DbRole.VeitOperator.ToString();
            var operatorRole = userModel.Roles.FirstOrDefault(f => f.Name == roleName);
            if (operatorRole == null)
            {
                throw new ArgumentNullException($"Role: {roleName} is not defined in user DB!");
            }

            var userRole = new User.Model.Models.UserRole
            {
                UserId = primeOperatorId,
                RoleId = operatorRole.Id
            };
            veitOperator.UserRoles.Add(userRole);
            userModel.Users.Add(veitOperator);
            userModel.SaveChanges();
        }


        private IEnumerable<DbContext> GetAllComanyContexts(string batModelConnectionString)
        {
            var contexts = new List<DbContext>();
            var companyIds = userModel.Companies.Select(s => s.Id);
            foreach (var companyId in companyIds)
            {
                var model = new BatCloudModelContainer(string.Format(CultureInfo.InvariantCulture, batModelConnectionString, companyId));
                contexts.Add(model);
            }
            return contexts;
        }

        private static void MigrateDb(IEnumerable<DbContext> storages)
        {
            foreach (var storage in storages)
            {
                if (!storage.Database.IsInMemory()) //for tests are use DB in memory
                {
                    storage.Database.Migrate();
                }
            }
        }
    }
}
