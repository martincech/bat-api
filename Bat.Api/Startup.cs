﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Extensions;
using Bat.Cloud.Model;
using Bird.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MigrationV07.Model;
using Newtonsoft.Json.Serialization;
using NLog;
using Statistic;
using User.Model;
using Veit.Cognito.Service;
using Veit.Cognito.User;
using Veit.Cognito.User.Model;
using Veit.JWT.Configuration;
using Veit.Kafka;

namespace Bat.Api
{
    public class Startup
    {
        public const string SwaggerEndpoint = "swagger-original.json";

        private readonly string batModelConnectionString;
        private readonly string userModelConnectionString;
        private readonly string birdModelConnectionString;
        private readonly string migrationV07ConnectionString;
        private readonly CognitoSettings cognitoSettings;
        private readonly AwsIam iam;
        private const string CORS_POLICY = "AllowOrigin";
        private const string CLAIM_DATABASE = "Database";
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private const string SWAGGER_NAME = "BAT API v1";
        private const string ERROR_PATH = "/Error";

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;

            batModelConnectionString = Configuration.GetCloudModelConnectionString();
            userModelConnectionString = Configuration.GetUserModelConnectionString();
            birdModelConnectionString = Configuration.GetBirdModelConnectionString();
            migrationV07ConnectionString = Configuration.GetMigrationV07ModelConnectionString();
            cognitoSettings = Configuration.GetSection("Cognito").Get<CognitoSettings>();
            iam = Configuration.GetSection(nameof(AwsIam)).Get<AwsIam>();

            if (iam == null || string.IsNullOrEmpty(iam.Id) || string.IsNullOrEmpty(iam.Secret))
            {
                logger.Fatal($"Configuration: {nameof(AwsIam)} in appsettings.json is invalid or missing");
                throw new InvalidCastException(nameof(AwsIam));
            }
            if (cognitoSettings == null)
            {
                logger.Fatal($"Configuration: {nameof(CognitoSettings)} in appsettings.json is missing");
                throw new InvalidCastException(nameof(CognitoSettings));
            }
        }


        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(CORS_POLICY,
                    builder => builder.AllowAnyOrigin()
                                      .AllowAnyMethod()
                                      .AllowAnyHeader());
            });

            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(GlobalExceptionFilterAttribute));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                    {
                        options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter { NamingStrategy = new CamelCaseNamingStrategy() });
                        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                    });

            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
            });
            ConfigureAuthentication(services);


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);
            services.AddSingleton<IContainerProvider, ContainerProvider>();
            services.AddTransient<IStatisticContext>(s => new InfluxContext(Configuration.GetInfluxUri()));
            services.AddSingleton<IKafkaSender>(k => new KafkaSender(Configuration.GetKafkaBroker(), Path.Combine(Environment.ContentRootPath, "Cache", "API.kafkaCache")));

            var cognitoOption = new AwsSettings
            {
                UserPoolId = cognitoSettings.UserPoolId,
                Region = cognitoSettings.Region,
                Id = iam.Id,
                Secret = iam.Secret
            };
            services.AddSingleton<IUserContext>(u => new UserContext(cognitoOption));

            services.AddDbContext<UserModelContainer>(o => o.UseSqlServer(userModelConnectionString));
            services.AddDbContext<BirdModelContainer>(o => o.UseSqlServer(birdModelConnectionString));
            services.AddDbContext<MigrationV07ModelContainer>(o => o.UseSqlServer(migrationV07ConnectionString));
            services.AddDbContext<BatCloudModelContainer>((provider, options) =>
            {
                var principal = provider.GetService<ClaimsPrincipal>();
                var database = principal.HasClaim(c => c.Type == CLAIM_DATABASE) ? principal.FindFirstValue(CLAIM_DATABASE) : null;
                if (!string.IsNullOrEmpty(database))
                {
                    options.UseSqlServer(string.Format(CultureInfo.InvariantCulture, batModelConnectionString, database));
                }
            });


            services.AddScoped<IConnectionFactory, ConnectionFactory>();
            services.AddTransient<DatabaseValidation>();
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            //Fetch JSON Web Key Set used for JWT verification
            var keys = JsonWebKeys.Get(cognitoSettings.Issuer);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = keys.LastOrDefault().Value.TokenValidationParameters;
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async ctx =>
                    {
                        var success = false;
                        var scopes = ctx.Principal.Claims.First(f => f.Type == "scope");
                        if (scopes != null && !cognitoSettings.Scopes.ToUpperInvariant().Split(" ").Except(scopes.Value.ToUpperInvariant().Split(" ")).Any())
                        {
                            var userId = ctx.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
                            var db = ctx.HttpContext.RequestServices.GetRequiredService<UserModelContainer>();

                            var user = await db.UsersLazy.FirstOrDefaultAsync(a => a.Id.ToString() == userId);
                            if (user != null)
                            {
                                var role = user.Roles.FirstOrDefault();
                                var claims = new List<Claim>
                                    {
                                        new Claim(ClaimTypes.Role, role?.Name ?? string.Empty),
                                        new Claim(CLAIM_DATABASE, user.Company?.Id.ToString() ?? string.Empty)
                                    };
                                var appIdentity = new ClaimsIdentity(claims);
                                ctx.Principal.AddIdentity(appIdentity);
                                success = true;
                            }
                        }

                        if (!success)
                        {
                            ctx.Fail($"Failed to authorize user.");
                        }
                    }
                };
            });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DatabaseValidation dbValidator)
        {
            dbValidator.MigrateDatabases(batModelConnectionString, Configuration.GetPrimeOperator());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStatusCodePagesWithReExecute(ERROR_PATH, "?status={0}");

            app.UseHttpsRedirection();
            app.UseCors(CORS_POLICY);
            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/{SwaggerEndpoint}", SWAGGER_NAME);
            });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

#if DEBUG
            app.UseMiddleware<EditSwaggerResponseBodyMiddleware>();
#endif

            app.UseMvc()
                .UseDefaultFiles()
                .UseStaticFiles();
        }
    }
}
