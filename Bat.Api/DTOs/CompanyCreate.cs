﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Bat.Api.DTOs
{
    [DataContract]
    public class CompanyCreate : Contact, IEquatable<CompanyCreate>
    {
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [Required]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Vat
        /// </summary>
        [DataMember(Name = "vat")]
        public string Vat { get; set; }

        /// <summary>
        /// Gets or Sets Address
        /// </summary>
        [DataMember(Name = "address")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or Sets Zip
        /// </summary>
        [DataMember(Name = "zip")]
        public string Zip { get; set; }

        /// <summary>
        /// Gets or Sets City
        /// </summary>
        [DataMember(Name = "city")]
        public string City { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        [DataMember(Name = "country")]
        public string Country { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(CompanyCreate)} {{");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        protected string ToInternalString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(Vat)}: {Vat}");
            sb.AppendLine($"\t{nameof(Address)}: {Address}");
            sb.AppendLine($"\t{nameof(Zip)}: {Zip}");
            sb.AppendLine($"\t{nameof(City)}: {City}");
            sb.AppendLine($"\t{nameof(Country)}: {Country}");
            sb.AppendLine($"\t{nameof(ContactName)}: {ContactName}");
            sb.AppendLine($"\t{nameof(Phone)}: {Phone}");
            sb.AppendLine($"\t{nameof(Email)}: {Email}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((CompanyCreate)obj);
        }

        /// <summary>
        /// Returns true if Company instances are equal
        /// </summary>
        /// <param name="other">Instance of Company to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(CompanyCreate other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Name.IsEqual(other.Name) &&
                   Vat.IsEqual(other.Vat) &&
                   Address.IsEqual(other.Address) &&
                   Zip.IsEqual(other.Zip) &&
                   City.IsEqual(other.City) &&
                   Country.IsEqual(other.Country) &&
                   ContactName.IsEqual(other.ContactName) &&
                   Phone.IsEqual(other.Phone) &&
                   Email.IsEqual(other.Email);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode(StringComparison.Ordinal);
                if (Vat != null)
                    hashCode = hashCode * 59 + Vat.GetHashCode(StringComparison.Ordinal);
                if (Address != null)
                    hashCode = hashCode * 59 + Address.GetHashCode(StringComparison.Ordinal);
                if (Zip != null)
                    hashCode = hashCode * 59 + Zip.GetHashCode(StringComparison.Ordinal);
                if (City != null)
                    hashCode = hashCode * 59 + City.GetHashCode(StringComparison.Ordinal);
                if (Country != null)
                    hashCode = hashCode * 59 + Country.GetHashCode(StringComparison.Ordinal);
                if (ContactName != null)
                    hashCode = hashCode * 59 + ContactName.GetHashCode(StringComparison.Ordinal);
                if (Phone != null)
                    hashCode = hashCode * 59 + Phone.GetHashCode(StringComparison.Ordinal);
                if (Email != null)
                    hashCode = hashCode * 59 + Email.GetHashCode(StringComparison.Ordinal);
                return hashCode;
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(CompanyCreate left, CompanyCreate right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CompanyCreate left, CompanyCreate right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
