﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs
{
    [DataContract]
    public class HelpPage : HelpPageBase
    {
        public HelpPage()
        {
        }

        [DataMember(Name = "content")]
        public string Content { get; set; }
    }
}
