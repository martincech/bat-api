﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Bat.Api.DTOs.Enums;
using Newtonsoft.Json;

namespace Bat.Api.DTOs
{
    [DataContract]
    public partial class DailyStatistic : IEquatable<DailyStatistic>
    {
        /// <summary>
        /// Day of the running weighing
        /// </summary>
        /// <value>Day of the running weighing</value>
        [DataMember(Name = "day", EmitDefaultValue = false)]
        public int? Day { get; set; }

        /// <summary>
        /// Date and Time when the stats have been generated.
        /// </summary>
        /// <value>Date and Time when the stats have been generated.</value>
        [Required]
        [DataMember(Name = "dateTime")]
        public DateTimeOffset DateTime { get; set; }

        /// <summary>
        /// Number of samples weighted during day.
        /// </summary>
        /// <value>Number of samples weighted during day.</value>
        [DataMember(Name = "count", EmitDefaultValue = false)]
        public int? Count { get; set; }

        /// <summary>
        /// Average weight in grams.
        /// </summary>
        /// <value>Average weight in grams.</value>
        [Required]
        [DataMember(Name = "average", EmitDefaultValue = false)]
        public int? Average { get; set; }

        /// <summary>
        /// Daily gain in grams.
        /// </summary>
        /// <value>Daily gain in grams.</value>
        [DataMember(Name = "gain", EmitDefaultValue = false)]
        public int? Gain { get; set; }

        /// <summary>
        /// Uniformity in percentage.
        /// </summary>
        /// <value>Uniformity in percentage.</value>
        [DataMember(Name = "uniformity", EmitDefaultValue = false)]
        public double? Uniformity { get; set; }

        /// <summary>
        /// Standard deviation in grams.
        /// </summary>
        /// <value>Standard deviation in grams.</value>
        [DataMember(Name = "sigma", EmitDefaultValue = false)]
        public int? Sigma { get; set; }

        /// <summary>
        /// Coefficient of variation in percentage.
        /// </summary>
        /// <value>Coefficient of variation in percentage.</value>
        [DataMember(Name = "cv", EmitDefaultValue = false)]
        public double? Cv { get; set; }

        /// <summary>
        /// Gets or Sets Sex
        /// </summary>
        [DataMember(Name = "sex", EmitDefaultValue = false)]
        public Sex? Sex { get; set; }

        /// <summary>
        /// Gets or Sets DeviceId
        /// </summary>
        [DataMember(Name = "deviceId")]
        public Guid? DeviceId { get; set; }

        /// <summary>
        /// Gets or Sets Device
        /// </summary>
        [DataMember(Name = "device", EmitDefaultValue = false)]
        public Device Device { get; set; }


        /// <summary>
        /// Gets or Sets FlockId
        /// </summary>
        [DataMember(Name = "flockId")]
        public Guid? FlockId { get; set; }

        /// <summary>
        /// Gets or Sets Flock
        /// </summary>
        [DataMember(Name = "flock", EmitDefaultValue = false)]
        public Flock Flock { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(DailyStatistic)} {{");
            sb.AppendLine($"{nameof(Day)}: {Day}");
            sb.AppendLine($"{nameof(DateTime)}: {DateTime}");
            sb.AppendLine($"{nameof(Count)}: {Count}");
            sb.AppendLine($"{nameof(Average)}: {Average}");
            sb.AppendLine($"{nameof(Gain)}: {Gain}");
            sb.AppendLine($"{nameof(Uniformity)}: {Uniformity}");
            sb.AppendLine($"{nameof(Sigma)}: {Sigma}");
            sb.AppendLine($"{nameof(Cv)}: {Cv}");
            sb.AppendLine($"{nameof(Sex)}: {Sex}");
            sb.AppendLine($"{nameof(DeviceId)}: {DeviceId}");
            sb.AppendLine($"{nameof(FlockId)}: {FlockId}");
            sb.AppendLine("}");
            return sb.ToString();
        }


        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DailyStatistic)obj);
        }

        /// <summary>
        /// Returns true if DailyStatistic instances are equal
        /// </summary>
        /// <param name="other">Instance of DailyStatistic to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(DailyStatistic other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Day.IsEqual(other.Day) &&
                   DateTime.IsEqual(other.DateTime) &&
                   Count.IsEqual(other.Count) &&
                   Average.IsEqual(other.Average) &&
                   Gain.IsEqual(other.Gain) &&
                   Uniformity.IsEqual(other.Uniformity) &&
                   Sigma.IsEqual(other.Sigma) &&
                   Cv.IsEqual(other.Cv) &&
                   Sex.IsEqual(other.Sex) &&
                   DeviceId.IsEqual(other.DeviceId) &&
                   FlockId.IsEqual(other.FlockId);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                hashCode = hashCode * 59 + Day.GetHashCode();
                hashCode = hashCode * 59 + DateTime.GetHashCode();
                hashCode = hashCode * 59 + Count.GetHashCode();
                hashCode = hashCode * 59 + Average.GetHashCode();
                hashCode = hashCode * 59 + Gain.GetHashCode();
                hashCode = hashCode * 59 + Uniformity.GetHashCode();
                hashCode = hashCode * 59 + Sigma.GetHashCode();
                hashCode = hashCode * 59 + Cv.GetHashCode();
                hashCode = hashCode * 59 + Sex.GetHashCode();
                if (DeviceId != null)
                    hashCode = hashCode * 59 + DeviceId.GetHashCode();
                if (FlockId != null)
                    hashCode = hashCode * 59 + FlockId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(DailyStatistic left, DailyStatistic right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DailyStatistic left, DailyStatistic right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
