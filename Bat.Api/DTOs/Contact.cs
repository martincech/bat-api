﻿using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Bat.Api.DTOs
{
    [DataContract]
    public partial class Contact : IEquatable<Contact>
    {
        /// <summary>
        /// Gets or Sets ContactName
        /// </summary>
        [DataMember(Name = "contactName", EmitDefaultValue = false)]
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or Sets Phone
        /// </summary>
        [DataMember(Name = "phone", EmitDefaultValue = false)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or Sets Email
        /// </summary>
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Contact)} {{");
            sb.AppendLine($"\t{nameof(ContactName)}: {ContactName}");
            sb.AppendLine($"\t{nameof(Phone)}: {Phone}");
            sb.AppendLine($"\t{nameof(Email)}: {Email}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Contact)obj);
        }

        /// <summary>
        /// Returns true if CompanyCreate instances are equal
        /// </summary>
        /// <param name="other">Instance of CompanyCreate to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(Contact other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return ContactName.IsEqual(other.ContactName) &&
                   Phone.IsEqual(other.Phone) &&
                   Email.IsEqual(other.Email);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (ContactName != null)
                    hashCode = hashCode * 59 + ContactName.GetHashCode(StringComparison.Ordinal);
                if (Phone != null)
                    hashCode = hashCode * 59 + Phone.GetHashCode(StringComparison.Ordinal);
                if (Email != null)
                    hashCode = hashCode * 59 + Email.GetHashCode(StringComparison.Ordinal);
                return hashCode;
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(Contact left, Contact right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Contact left, Contact right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
