﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Bat.Api.DTOs.Enums;
using Newtonsoft.Json;

namespace Bat.Api.DTOs
{
    [DataContract]
    public abstract class DeviceBase : IEquatable<DeviceBase>
    {
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [Required]
        [DataMember(Name = "type")]
        public DeviceType Type { get; set; }

        /// <summary>
        /// Gets or Sets Unit
        /// </summary>
        [Required]
        [DataMember(Name = "unit")]
        public DeviceUnit Unit { get; set; }

        /// <summary>
        /// Gets or Sets Description
        /// </summary>
        [DataMember(Name = "description", EmitDefaultValue = false)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or Sets PhoneNumber
        /// </summary>
        [DataMember(Name = "phoneNumber", EmitDefaultValue = false)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Device can be assign to farm without specification of concrete placement in a house
        /// </summary>
        /// <value>Device can be assign to farm without specification of concrete placement in a house</value>
        [DataMember(Name = "farmId")]
        public Guid? FarmId { get; set; }

        /// <summary>
        /// Gets or Sets Farm
        /// </summary>
        [DataMember(Name = "farm", EmitDefaultValue = false)]
        public Farm Farm { get; set; }

        /// <summary>
        /// Assign device to concrete house and its farm
        /// </summary>
        /// <value>Assign device to concrete house and its farm</value>
        [DataMember(Name = "houseId")]
        public Guid? HouseId { get; set; }

        /// <summary>
        /// Gets or Sets House
        /// </summary>
        [DataMember(Name = "house", EmitDefaultValue = false)]
        public House House { get; set; }

        /// <summary>
        /// Assign device to concrete parent
        /// </summary>
        /// <value>Assign device to concrete parent</value>
        [DataMember(Name = "parentId")]
        public Guid? ParentId { get; set; }

        /// <summary>
        /// Gets or Sets Parent
        /// </summary>
        [DataMember(Name = "parent", EmitDefaultValue = false)]
        public Device Parent { get; set; }



        protected string ToInternalString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(Type)}: {Type}");
            sb.AppendLine($"\t{nameof(Unit)}: {Unit}");
            sb.AppendLine($"\t{nameof(Description)}: {Description}");
            sb.AppendLine($"\t{nameof(PhoneNumber)}: {PhoneNumber}");
            sb.AppendLine($"\t{nameof(FarmId)}: {FarmId}");
            sb.AppendLine($"\t{nameof(HouseId)}: {HouseId}");
            sb.AppendLine($"\t{nameof(ParentId)}: {ParentId}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeviceBase)obj);
        }

        /// <summary>
        /// Returns true if DeviceCreate instances are equal
        /// </summary>
        /// <param name="other">Instance of DeviceCreate to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(DeviceBase other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Name.IsEqual(other.Name) &&
                   Type.IsEqual(other.Type) &&
                   Unit.IsEqual(other.Unit) &&
                   Description.IsEqual(other.Description) &&
                   PhoneNumber.IsEqual(other.PhoneNumber) &&
                   FarmId.IsEqual(other.FarmId) &&
                   HouseId.IsEqual(other.HouseId) &&
                   ParentId.IsEqual(other.ParentId);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Name != null)
                {
                    hashCode = hashCode * 59 + Name.GetHashCode(StringComparison.Ordinal);
                }
                hashCode = hashCode * 59 + Type.GetHashCode();
                hashCode = hashCode * 59 + Unit.GetHashCode();
                if (Description != null)
                    hashCode = hashCode * 59 + Description.GetHashCode(StringComparison.Ordinal);
                if (PhoneNumber != null)
                    hashCode = hashCode * 59 + PhoneNumber.GetHashCode(StringComparison.Ordinal);
                if (FarmId != null)
                    hashCode = hashCode * 59 + FarmId.GetHashCode();
                if (HouseId != null)
                    hashCode = hashCode * 59 + HouseId.GetHashCode();
                if (ParentId != null)
                    hashCode = hashCode * 59 + ParentId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(DeviceBase left, DeviceBase right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeviceBase left, DeviceBase right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
