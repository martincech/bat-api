﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using Bat.Api.DTOs;

namespace Bat.Api.DTOs
{
    [DataContract]
    public partial class Company : CompanyCreate, IEquatable<Company>
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Company)} {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }


        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Company)obj);
        }

        /// <summary>
        /// Returns true if Company instances are equal
        /// </summary>
        /// <param name="other">Instance of Company to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(Company other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Name.IsEqual(other.Name) &&
                   Vat.IsEqual(other.Vat) &&
                   Address.IsEqual(other.Address) &&
                   Zip.IsEqual(other.Zip) &&
                   City.IsEqual(other.City) &&
                   Country.IsEqual(other.Country) &&
                   ContactName.IsEqual(other.ContactName) &&
                   Phone.IsEqual(other.Phone) &&
                   Email.IsEqual(other.Email);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return base.GetHashCode() * 59 + Id.GetHashCode();
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(Company left, Company right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Company left, Company right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
