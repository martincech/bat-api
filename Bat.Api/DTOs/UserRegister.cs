﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Bat.Api.DTOs
{
    [DataContract]
    public partial class UserRegister : IEquatable<UserRegister>
    {
        /// <summary>
        /// Gets or Sets Email
        /// </summary>
        [Required]
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Company
        /// </summary>
        [DataMember(Name = "company", EmitDefaultValue = false)]
        public string Company { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(UserRegister)} {{");
            sb.AppendLine($"\t{nameof(Email)}: {Email}");
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(Company)}: {Company}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((UserRegister)obj);
        }

        /// <summary>
        /// Returns true if User instances are equal
        /// </summary>
        /// <param name="other">Instance of User to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(UserRegister other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Email.IsEqual(other.Email) &&
                   Name.IsEqual(other.Name) &&
                   Company.IsEqual(other.Company);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Email != null)
                    hashCode = hashCode * 59 + Email.GetHashCode(StringComparison.Ordinal);
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode(StringComparison.Ordinal);
                if (Company != null)
                    hashCode = hashCode * 59 + Company.GetHashCode(StringComparison.Ordinal);
                return hashCode;
            }
        }

        #region Operators

        public static bool operator ==(UserRegister left, UserRegister right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UserRegister left, UserRegister right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
