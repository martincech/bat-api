﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    public enum DeviceUnit
    {
        [EnumMember(Value = "g")]
        G = 0,
        [EnumMember(Value = "kg")]
        Kg = 1,
        [EnumMember(Value = "lb")]
        Lb = 2
    }
}
