﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    /// <summary>
    /// Gets or Sets Type
    /// </summary>
    public enum DeviceType
    {
        [EnumMember(Value = "bat2GsmScale")]
        Bat2GsmScale = 1,
        [EnumMember(Value = "bat2CableScale")]
        Bat2CableScale = 2,
        [EnumMember(Value = "temperatureSensor")]
        TemperatureSensor = 3,
        [EnumMember(Value = "humiditySensor")]
        HumiditySensor = 4,
        [EnumMember(Value = "carbonDioxideSensor")]
        CarbonDioxideSensor = 5,
        [EnumMember(Value = "batCollectorTerminal")]
        BatCollectorTerminal = 6,
        [EnumMember(Value = "batGateway")]
        BatGateway = 7
    }
}
