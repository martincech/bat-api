﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    /// <summary>
    /// Gets or Sets Sex
    /// </summary>
    public enum Sex
    {
        [EnumMember(Value = "irrelevant")]
        Irrelevant = 0,
        [EnumMember(Value = "female")]
        Female = 1,
        [EnumMember(Value = "male")]
        Male = 2
    }
}
