﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    public enum Role
    {
        [EnumMember(Value = "undefined")]
        Undefined = 0,
        [EnumMember(Value = "veitOperator")]
        VeitOperator = 1,
        [EnumMember(Value = "companyAdmin")]
        CompanyAdmin = 2
    }
}
