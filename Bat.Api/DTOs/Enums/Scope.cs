﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    /// <summary>
    /// Flag private means that the Bird was defined only for your company.
    /// </summary>
    /// <value>Flag private means that the Bird was defined only for your company.</value>
    public enum Scope
    {
        [EnumMember(Value = "global")]
        Global = 1,
        [EnumMember(Value = "private")]
        Private = 2
    }
}
