﻿using System.Runtime.Serialization;

namespace Bat.Api.DTOs.Enums
{
    /// <summary>
    /// Gets or Sets DateType
    /// </summary>
    public enum DateType
    {
        [EnumMember(Value = "day")]
        Day = 0,
        [EnumMember(Value = "week")]
        Week = 1
    }
}
