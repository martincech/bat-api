﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Bat.Api.DTOs
{
    [DataContract]
#pragma warning disable CA1724 // Type names should not match namespaces
    public partial class User : UserCreate, IEquatable<User>
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        // Email is override due to required attribute in base class.
        // Here it is non required parameter.
        /// <summary>
        /// Gets or Sets Email
        /// </summary>
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public override string Email { get { return base.Email; } set { base.Email = value; } }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(User)}: {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((User)obj);
        }

        /// <summary>
        /// Returns true if User instances are equal
        /// </summary>
        /// <param name="other">Instance of User to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(User other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Email.IsEqual(other.Email) &&
                   Name.IsEqual(other.Name) &&
                   CompanyId.IsEqual(other.CompanyId) &&
                   Role.IsEqual(other.Role);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return base.GetHashCode() * 59 + Id.GetHashCode();
            }
        }

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(User left, User right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(User left, User right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
