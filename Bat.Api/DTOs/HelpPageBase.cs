﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Bat.Api.DTOs
{
    [DataContract]
    public class HelpPageBase
    {
        public HelpPageBase()
        {
            Id = Guid.NewGuid();
        }

        [Key, DataMember(Name = "id")]
        public Guid Id { get; set; }
        [Required, DataMember(Name = "slug")]
        public string Slug { get; set; }
        [Required, DataMember(Name = "title")]
        public string Title { get; set; }
        [DataMember(Name = "disabled")]
        public bool Disabled { get; set; }
        [DataMember(Name = "actions")]
        public string Actions { get; set; }
    }
}
