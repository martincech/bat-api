﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bat.Api.DTOs
{
    public static class EqualsExtensions
    {
        public static bool IsEqual<T>(this T current, T other)
        {
            var nullCheck = true;
            if (!typeof(T).IsValueType)
            {
                nullCheck = current != null;
            }

            return nullCheck &&
                   current.Equals(other);
        }

        public static bool IsEqual(this string current, string other)
        {
            return Equals(current, other) ||
                   current != null &&
                   current.Equals(other, StringComparison.Ordinal);
        }


        public static bool IsEqual<T>(this IEnumerable<T> current, IEnumerable<T> other)
        {
            if (current == null && other == null)
            {
                return true;
            }
            if (current == null || other == null)
            {
                return false;
            }
            return current.SequenceEqual(other);
        }
    }
}
