﻿using System;
using Microsoft.Extensions.Configuration;

namespace Bat.Api.Extensions
{
    public static class ConfigurationExtensions
    {
        private const string CloudModel = "BatModelContainer";
        private const string UserModel = "UserModelContainer";
        private const string BirdModel = "BirdModelContainer";
        private const string MigrationV07Model = "MigrationV07ModelContainer";
        private const string Influx = "Influx";
        private const string Kafka = "Kafka";
        private const string PrimeOperator = "PrimeOperator";

        public static string GetCloudModelConnectionString(this IConfiguration configuration)
        {
            return configuration?.GetValue(CloudModel);
        }

        public static string GetUserModelConnectionString(this IConfiguration configuration)
        {
            return configuration?.GetValue(UserModel);
        }

        public static string GetBirdModelConnectionString(this IConfiguration configuration)
        {
            return configuration?.GetValue(BirdModel);
        }

        public static string GetMigrationV07ModelConnectionString(this IConfiguration configuration)
        {
            return configuration?.GetValue(MigrationV07Model);
        }

        public static Uri GetInfluxUri(this IConfiguration configuration)
        {
            return new Uri(configuration[Influx]);
        }

        public static string GetKafkaBroker(this IConfiguration configuration)
        {
            return configuration[Kafka];
        }

        public static string GetPrimeOperator(this IConfiguration configuration)
        {
            return configuration[PrimeOperator];
        }

        private static string GetValue(this IConfiguration configuration, string value)
        {
            return configuration?.GetConnectionString(value);
        }
    }
}
