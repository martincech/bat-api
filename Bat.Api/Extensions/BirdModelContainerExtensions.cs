﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bird.Model;
using Newtonsoft.Json;
using NLog;

namespace Bat.Api.Extensions
{
    public static class BirdModelContainerExtensions
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        const string BirdsDataFolder = "BirdsData";

        public static void Seed(this BirdModelContainer modelContainer)
        {
            try
            {
                if (DatabaseIsEmpty(modelContainer))
                {
                    modelContainer.Birds.AddRange(Load());
                    modelContainer.SaveChanges();
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to seed public bird data.");
            }
        }

        static bool DatabaseIsEmpty(BirdModelContainer modelContainer)
        {
            return !(modelContainer.Companies.Any() || modelContainer.Products.Any() || modelContainer.Birds.Any());
        }

        static List<Bird.Model.Models.Bird> Load()
        {
            var birds = new List<Bird.Model.Models.Bird>();
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BirdsDataFolder);
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path, "*.json");
                foreach (var file in files)
                {
                    try
                    {
                        var json = File.ReadAllText(file);
                        var bird = JsonConvert.DeserializeObject<Bird.Model.Models.Bird>(json);
                        birds.Add(bird);
                    }
                    catch (Exception e)
                    {
                        logger.Warn($"Failed to parse seed file {file}. {e.Message}");
                    }
                }
            }
            return birds;
        }
    }
}
