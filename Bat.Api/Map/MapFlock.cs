﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;

namespace Bat.Api.Map
{
    public static class MapFlock
    {
        public static IEnumerable<Flock> MapTo(this IEnumerable<Cloud.Model.Models.Flock> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        public static Flock MapTo(this Cloud.Model.Models.Flock model)
        {
            if (model == null)
            {
                return null;
            }

            return new Flock
            {
                Id = model.Id,
                Name = model.Name,
                InitialAge = model.InitialAge,
                InitialWeight = model.InitialWeight,
                TargetAge = model.TargetAge,
                TargetWeight = model.TargetWeight,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                BirdId = model.BirdId,
                HouseId = model.HouseId,
                DeviceId = model.DeviceId
            };
        }

        public static IEnumerable<Flock> MapTo(this IEnumerable<Cloud.Model.Models.Flock> models, Dictionary<string, string> expand)
        {
            return models?.Select(s => s.MapTo(expand)).Where(w => w != null).ToList();
        }

        public static Flock MapTo(this Cloud.Model.Models.Flock model, Dictionary<string, string> expand)
        {
            if (model == null)
            {
                return null;
            }

            var flock = new Flock
            {
                Id = model.Id,
                Name = model.Name,
                StartDate = model.StartDate,
                BirdId = model.BirdId,
                HouseId = model.HouseId,
                DeviceId = model.DeviceId
            };

            if (expand == null || !expand.Any())
            {   //no other expand properties are needed
                return flock;
            }

            if (expand.ContainsKey(nameof(flock.InitialAge)))
            {
                flock.InitialAge = model.InitialAge;
            }
            if (expand.ContainsKey(nameof(flock.InitialWeight)))
            {
                flock.InitialWeight = model.InitialWeight;
            }
            if (expand.ContainsKey(nameof(flock.TargetAge)))
            {
                flock.TargetAge = model.TargetAge;
            }
            if (expand.ContainsKey(nameof(flock.TargetWeight)))
            {
                flock.TargetWeight = model.TargetWeight;
            }
            if (expand.ContainsKey(nameof(flock.EndDate)))
            {
                flock.EndDate = model.EndDate;
            }

            //bird in model is not foreign key, only ID

            if (expand.ContainsKey(nameof(flock.House)))
            {
                flock.House = model.House.MapTo(null);
            }
            if (expand.ContainsKey(nameof(flock.Device)))
            {
                flock.Device = model.Device.MapTo(null);
            }
            return flock;
        }


        public static Cloud.Model.Models.Flock MapFrom(this Flock flock, Guid id)
        {
            var model = flock.MapFrom();
            if (model != null)
            {
                model.Id = id;
            }
            return model;
        }

        public static Cloud.Model.Models.Flock MapFrom(this FlockCreate flock)
        {
            if (flock == null)
            {
                return null;
            }

            return new Cloud.Model.Models.Flock
            {
                Name = flock.Name,
                InitialAge = flock.InitialAge ?? 0,
                InitialWeight = flock.InitialWeight ?? 0,
                TargetAge = flock.TargetAge,
                TargetWeight = flock.TargetWeight,
                StartDate = flock.StartDate,
                EndDate = flock.EndDate,
                BirdId = flock.BirdId,
                HouseId = flock.HouseId,
                DeviceId = flock.DeviceId
            };
        }

        public static void Update(this Cloud.Model.Models.Flock existed, Cloud.Model.Models.Flock update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.InitialAge = update.InitialAge;
            existed.InitialWeight = update.InitialWeight;
            existed.TargetAge = update.TargetAge;
            existed.TargetWeight = update.TargetWeight;
            existed.StartDate = update.StartDate;
            existed.EndDate = update.EndDate;
            existed.BirdId = update.BirdId;
            existed.HouseId = update.HouseId;
            existed.DeviceId = update.DeviceId;
        }
    }
}
