﻿using System;
using System.Collections.Generic;
using Bat.Api.DTOs;
using Statistic.Models;
using Statistic.Models.Enums;
using Veit.Bat.Common;

namespace Bat.Api.Map
{
    public static class MapDailyStatistic
    {
        public static IEnumerable<DailyStatistic> MapTo(this GroupData data)
        {
            if (data is null || data.Values is null)
            {
                return new List<DailyStatistic>();
            }

            var statistics = new List<DailyStatistic>();
            var (deviceId, flockId) = MapParent(data.Id, data.Group);
            foreach (var stat in data.Values)
            {
                var dayStat = stat.MapTo();
                dayStat.DeviceId = deviceId;
                dayStat.FlockId = flockId;
                statistics.Add(dayStat);
            }
            return statistics;
        }

        public static IEnumerable<DailyStatistic> MapTo(this IEnumerable<GroupData> data)
        {
            if (data is null)
            {
                return new List<DailyStatistic>();
            }

            var statistics = new List<DailyStatistic>();
            foreach (var stat in data)
            {
                statistics.AddRange(stat.MapTo());
            }
            return statistics;
        }


        private static (Guid? deviceId, Guid? flockId) MapParent(string id, StatisticGroup group)
        {
            if (!Guid.TryParse(id, out var guid))
            {
                throw new ArgumentException("Id is not valid type");
            }

            if (group == StatisticGroup.Device)
            {
                return (guid, null);
            }
            return (null, guid);
        }


        private static DailyStatistic MapTo(this Stat model)
        {
            return new DailyStatistic
            {
                Day = model.Day,
                DateTime = model.TimeStamp,
                Count = model.Count,
                Average = (int?)model.Average,
                Gain = (int?)model.Gain,
                Uniformity = model.Uniformity,
                Sigma = (int?)model.Sigma,
                Cv = model.Cv,
                Sex = model.Sex.MapTo()
            };
        }

        private static DTOs.Enums.Sex? MapTo(this Sex? model)
        {
            if (model == null)
            {
                return DTOs.Enums.Sex.Irrelevant;
            }

            switch(model)
            {
                case Sex.Undefined:
                    return DTOs.Enums.Sex.Irrelevant;
                case Sex.Female:
                    return DTOs.Enums.Sex.Female;
                case Sex.Male:
                    return DTOs.Enums.Sex.Male;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }
    }
}
