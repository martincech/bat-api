﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;
using Bat.Api.DTOs.Enums;

namespace Bat.Api.Map
{
    public static class MapBird
    {
        #region MapTo

        public static IEnumerable<DTOs.Bird> MapTo(this IEnumerable<Bird.Model.Models.Bird> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        public static DTOs.Bird MapTo(this Bird.Model.Models.Bird model)
        {
            try
            {
                if (model == null)
                {
                    return null;
                }

                return new DTOs.Bird
                {
                    Id = model.Id,
                    Company = model.Product.Company.Name,
                    Product = model.Product.Name,
                    Name = model.Name,
                    Sex = model.Sex.MapTo(),
                    Duration = model.Duration,
                    DateType = model.DateType.MapTo(),
                    Scope = Scope.Private,
                    CurvePoints = model.CurvePoints.MapTo()
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static IEnumerable<DTOs.Bird> MapTo(this IEnumerable<Bird.Model.Models.Bird> models, Dictionary<string, string> expand, bool isTemplate)
        {
            return models?.Select(s => s.MapTo(expand, isTemplate)).Where(w => w != null).ToList();
        }

        public static DTOs.Bird MapTo(this Bird.Model.Models.Bird model, Dictionary<string, string> expand, bool isTemplate)
        {
            try
            {
                if (model == null)
                {
                    return null;
                }

                var bird = new DTOs.Bird
                {
                    Id = model.Id,
                    Company = model.Product.Company.Name,
                    Product = model.Product.Name,
                    Name = model.Name
                };

                if (expand == null || !expand.Any())
                {   //no other expand properties are needed
                    return bird;
                }

                if (expand.ContainsKey(nameof(bird.Sex)))
                {
                    bird.Sex = model.Sex.MapTo();
                }
                if (expand.ContainsKey(nameof(bird.Duration)))
                {
                    bird.Duration = model.Duration;
                }
                if (expand.ContainsKey(nameof(bird.DateType)))
                {
                    bird.DateType = model.DateType.MapTo();
                }
                if (expand.ContainsKey(nameof(bird.Scope)))
                {
                    bird.Scope = isTemplate ? Scope.Global : Scope.Private;
                }
                if (expand.ContainsKey(nameof(bird.CurvePoints)))
                {
                    bird.CurvePoints = model.CurvePoints.MapTo();
                }
                return bird;
            }
            catch (Exception)
            {
                return null;
            }
        }



        private static Sex? MapTo(this Bird.Model.Enums.Sex model)
        {
            switch (model)
            {
                case Bird.Model.Enums.Sex.Female:
                    return Sex.Female;
                case Bird.Model.Enums.Sex.Male:
                    return Sex.Male;
                case Bird.Model.Enums.Sex.Irrelevant:
                    return Sex.Irrelevant;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        private static DateType? MapTo(this Bird.Model.Enums.DateType model)
        {
            switch(model)
            {
                case Bird.Model.Enums.DateType.Day:
                    return DateType.Day;
                case Bird.Model.Enums.DateType.Week:
                    return DateType.Week;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        private static IEnumerable<CurvePoint> MapTo(this IEnumerable<Bird.Model.Models.CurvePoint> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        private static CurvePoint MapTo(this Bird.Model.Models.CurvePoint model)
        {
            if (model == null)
            {
                return null;
            }

            return new CurvePoint
            {
                Day = model.Day,
                Weight = model.Weight
            };
        }

        #endregion

        #region MapFrom

        public static Bird.Model.Models.Bird MapFrom(this DTOs.Bird bird, Guid birdId)
        {
            var model = bird.MapFrom();
            if (model != null)
            {
                model.Id = birdId;
            }
            return model;
        }

        public static Bird.Model.Models.Bird MapFrom(this BirdCreate bird)
        {
            if (bird == null)
            {
                return null;
            }

            var model = new Bird.Model.Models.Bird
            {
                Name = bird.Name,
                Sex = bird.Sex.MapFrom(),
                Duration = bird.Duration ?? 0,
                DateType = bird.DateType.MapFrom()
            };

            AddCurvePoints(ref model, bird.CurvePoints.MapFrom());
            return model;
        }

        private static void AddCurvePoints(ref Bird.Model.Models.Bird model, IEnumerable<Bird.Model.Models.CurvePoint> points)
        {
            model.CurvePoints.Clear();
            if (points == null)
            {
                return;
            }
            foreach (var point in points)
            {
                model.CurvePoints.Add(point);
            }
        }

        private static Bird.Model.Enums.Sex MapFrom(this Sex? sex)
        {
            if (!sex.HasValue)
            {
                return Bird.Model.Enums.Sex.Irrelevant;
            }

            switch (sex.Value)
            {
                case Sex.Female:
                    return Bird.Model.Enums.Sex.Female;
                case Sex.Male:
                    return Bird.Model.Enums.Sex.Male;
                case Sex.Irrelevant:
                    return Bird.Model.Enums.Sex.Irrelevant;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sex));
            }
        }

        private static Bird.Model.Enums.DateType MapFrom(this DateType? dateType)
        {
            if (!dateType.HasValue)
            {
                return Bird.Model.Enums.DateType.Day;
            }

            switch(dateType.Value)
            {
                case DateType.Day:
                    return Bird.Model.Enums.DateType.Day;
                case DateType.Week:
                    return Bird.Model.Enums.DateType.Week;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dateType));
            }
        }

        private static ICollection<Bird.Model.Models.CurvePoint> MapFrom(this IEnumerable<CurvePoint> points)
        {
            return points?.Select(MapFrom).Where(w => w != null).ToList();
        }

        private static Bird.Model.Models.CurvePoint MapFrom(this CurvePoint point)
        {
            if (point == null)
            {
                return null;
            }

            return new Bird.Model.Models.CurvePoint
            {
                Day = point.Day,
                Weight = point.Weight
            };
        }


        #endregion

        public static void Update(this Bird.Model.Models.Bird existed, Bird.Model.Models.Bird update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.Sex = update.Sex;
            existed.Duration = update.Duration;
            existed.DateType = update.DateType;
            existed.Product = update.Product;
            AddCurvePoints(ref existed, update.CurvePoints);
        }
    }
}
