﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;

namespace Bat.Api.Map
{
    public static class MapCompany
    {
        public static IEnumerable<Company> MapTo(this IEnumerable<User.Model.Models.Company> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }


        public static Company MapTo(this User.Model.Models.Company model)
        {
            if (model == null)
            {
                return null;
            }

            return new Company
            {
                Id = model.Id,
                Name = model.Name,
                Vat = model.Vat,
                Address = model.Address,
                Zip = model.Zip,
                City = model.City,
                Country = model.Country,
                ContactName = model.ContactName,
                Phone = model.Phone,
                Email = model.Email
            };
        }

        public static User.Model.Models.Company MapFrom(this Company company, Guid id)
        {
            var model = company.MapFrom();
            if (model != null)
            {
                model.Id = id;
            }
            return model;
        }

        public static User.Model.Models.Company MapFrom(this CompanyCreate company)
        {
            if (company == null)
            {
                return null;
            }

            return new User.Model.Models.Company
            {
                Name = company.Name,
                Vat = company.Vat,
                Address = company.Address,
                Zip = company.Zip,
                City = company.City,
                Country = company.Country,
                ContactName = company.ContactName,
                Phone = company.Phone,
                Email = company.Email
            };
        }

        public static void Update(this User.Model.Models.Company existed, User.Model.Models.Company update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.Vat = update.Vat;
            existed.Address = update.Address;
            existed.Zip = update.Zip;
            existed.City = update.City;
            existed.Country = update.Country;
            existed.ContactName = update.ContactName;
            existed.Phone = update.Phone;
            existed.Email = update.Email;
        }
    }
}
