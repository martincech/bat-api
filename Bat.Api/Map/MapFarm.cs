﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;

namespace Bat.Api.Map
{
    public static class MapFarm
    {
        public static IEnumerable<Farm> MapTo(this IEnumerable<Cloud.Model.Models.Farm> models, Dictionary<string, string> expand)
        {
            return models?.Select(s => s.MapTo(expand)).Where(w => w != null).ToList();
        }


        public static Farm MapTo(this Cloud.Model.Models.Farm model, Dictionary<string, string> expand)
        {
            if (model == null)
            {
                return null;
            }

            var farm = new Farm
            {
                Id = model.Id,
                Name = model.Name
            };

            if (expand == null || !expand.Any())
            {
                return farm;    //no other expand properties are needed
            }


            if (expand.ContainsKey(nameof(farm.Address)))
            {
                farm.Address = model.Address;
            }
            if (expand.ContainsKey(nameof(farm.Country)))
            {
                farm.Country = model.Country;
            }
            if (expand.ContainsKey(nameof(farm.ContactName)))
            {
                farm.ContactName = model.ContactName;
            }
            if (expand.ContainsKey(nameof(farm.Phone)))
            {
                farm.Phone = model.Phone;
            }
            if (expand.ContainsKey(nameof(farm.Email)))
            {
                farm.Email = model.Email;
            }
            return farm;
        }

        public static IEnumerable<Farm> MapTo(this IEnumerable<Cloud.Model.Models.Farm> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        public static Farm MapTo(this Cloud.Model.Models.Farm model)
        {
            if (model == null)
            {
                return null;
            }

            return new Farm
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address,
                Country = model.Country,
                ContactName = model.ContactName,
                Email = model.Email,
                Phone = model.Phone
            };
        }
        public static Cloud.Model.Models.Farm MapFrom(this Farm farm, Guid id)
        {
            var model = farm.MapFrom();
            if (model != null)
            {
                model.Id = id;
            }
            return model;
        }

        public static Cloud.Model.Models.Farm MapFrom(this FarmCreate farm)
        {
            if (farm == null)
            {
                return null;
            }

            return new Cloud.Model.Models.Farm
            {
                Name = farm.Name,
                Address = farm.Address,
                Country = farm.Country,
                ContactName = farm.ContactName,
                Phone = farm.Phone,
                Email = farm.Email
            };
        }

        public static void Update(this Cloud.Model.Models.Farm existed, Cloud.Model.Models.Farm update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.Address = update.Address;
            existed.Country = update.Country;
            existed.ContactName = update.ContactName;
            existed.Email = update.Email;
            existed.Phone = update.Phone;
        }
    }
}
