﻿using System;
using System.Collections.Generic;
using System.Linq;
using User.Model.Enums;
using User.Model.Models;

namespace Bat.Api.Map
{
    public static class MapUser
    {
        public static IEnumerable<DTOs.User> MapTo(this IEnumerable<User.Model.Models.User> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }


        public static DTOs.User MapTo(this User.Model.Models.User model)
        {
            if (model == null)
            {
                return null;
            }

            return new DTOs.User
            {
                Id = model.Id,
                CompanyId = model.CompanyId,
                Company = model.Company.MapTo(),
                Role = model.Roles.MapTo()
            };
        }

        public static DTOs.User MapTo(this DTOs.User user, Veit.Cognito.User.Model.CognitoUser cognitoUser)
        {
            if (user == null)
            {
                return null;
            }

            if (cognitoUser != null)
            {
                user.Name = cognitoUser.Name;
                user.Email = cognitoUser.Email;
            }
            return user;
        }

        public static IEnumerable<User.Model.Models.User> MapFrom(this IEnumerable<DTOs.User> users, IEnumerable<Role> roles)
        {
            return users?.Select(s => s.MapFrom(roles)).Where(w => w != null).ToList();
        }

        public static User.Model.Models.User MapFrom(this DTOs.User user, IEnumerable<Role> roles)
        {
            if (user == null)
            {
                return null;
            }

            var model = new User.Model.Models.User
            {
                Id = user.Id,
                CompanyId = user.CompanyId
            };
            AddRoles(ref model, user.Role.MapFrom(user.Id, roles));
            return model;
        }

        public static User.Model.Models.User MapFrom(this DTOs.UserCreate user, IEnumerable<Role> roles, Guid id)
        {
            if (user == null)
            {
                return null;
            }

            var model = new User.Model.Models.User
            {
                Id = id,
                CompanyId = user.CompanyId
            };
            AddRoles(ref model, user.Role.MapFrom(id, roles));
            return model;
        }


        private static void AddRoles(ref User.Model.Models.User user, IEnumerable<UserRole> roles)
        {
            user.UserRoles.Clear();
            if (roles == null)
            {
                return;
            }
            foreach (var role in roles)
            {
                user.UserRoles.Add(role);
            }
        }

        public static void Update(this User.Model.Models.User existed, User.Model.Models.User update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            //only company can be changed
            existed.CompanyId = update.CompanyId;
            AddRoles(ref existed, update.UserRoles);
        }


        private static DTOs.Enums.Role MapTo(this IEnumerable<Role> roles)
        {
            if (roles == null || !roles.Any())
            {
                return DTOs.Enums.Role.Undefined;
            }

            return roles.FirstOrDefault().MapTo();
        }

        private static DTOs.Enums.Role MapTo(this Role role)
        {
            if (!Enum.TryParse<DbRole>(role.Name, out var model))
            {
                return DTOs.Enums.Role.Undefined;
            }

            switch (model)
            {
                case DbRole.VeitOperator:
                    return DTOs.Enums.Role.VeitOperator;
                case DbRole.CompanyAdmin:
                    return DTOs.Enums.Role.CompanyAdmin;
                case DbRole.Undefined:
                default:
                    return DTOs.Enums.Role.Undefined;
            }
        }

        private static ICollection<UserRole> MapFrom(this DTOs.Enums.Role? role, Guid userId, IEnumerable<Role> roles)
        {
            var list = new List<UserRole>();
            if (role == DTOs.Enums.Role.Undefined)
            {
                return list;
            }

            var modelRole = role.MapFrom().ToString();
            var match = roles.FirstOrDefault(f => f.Name == modelRole);
            if (match != null)
            {
                list.Add(new UserRole
                {
                    UserId = userId,
                    RoleId = match.Id,
                });
            }
            return list;
        }

        private static DbRole MapFrom(this DTOs.Enums.Role? role)
        {
            if (!role.HasValue)
            {
                return DbRole.Undefined;
            }

            switch (role.Value)
            {
                case DTOs.Enums.Role.VeitOperator:
                    return DbRole.VeitOperator;
                case DTOs.Enums.Role.CompanyAdmin:
                    return DbRole.CompanyAdmin;
                case DTOs.Enums.Role.Undefined:
                default:
                    return DbRole.Undefined;
            }
        }

        public static Veit.Cognito.User.Model.CognitoUser MapFrom(this DTOs.User user)
        {
            return new Veit.Cognito.User.Model.CognitoUser
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email
            };
        }

        public static Veit.Cognito.User.Model.CognitoUser MapFrom(this DTOs.UserCreate user)
        {
            return new Veit.Cognito.User.Model.CognitoUser
            {
                Name = user.Name,
                Email = user.Email
            };
        }

        public static Veit.Cognito.User.Model.CognitoUser MapFrom(this DTOs.UserRegister user)
        {
            return new Veit.Cognito.User.Model.CognitoUser
            {
                Name = string.IsNullOrEmpty(user.Name) ? user.Email : user.Name,
                Email = user.Email
            };
        }
    }
}
