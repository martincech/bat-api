﻿using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;

namespace Bat.Api.Map
{
    public static class MapHelpPage
    {
        public static HelpPage MapTo(this User.Model.Models.HelpPage model)
        {
            if (model == null)
            {
                return null;
            }

            return new HelpPage
            {
                Id = model.Id,
                Slug = model.Slug,
                Title = model.Title,
                Content = model.Content,
                Actions = model.Actions,
                Disabled = model.Disabled,
            };
        }

        public static IEnumerable<HelpPageBase> MapToBase(this IEnumerable<User.Model.Models.HelpPage> models)
        {
            return models?.Select(MapToBase).Where(w => w != null).ToList();
        }

        public static HelpPageBase MapToBase(this User.Model.Models.HelpPage model)
        {
            if (model == null)
            {
                return null;
            }

            return new HelpPageBase
            {
                Id = model.Id,
                Slug = model.Slug,
                Title = model.Title,
                Actions = model.Actions,
                Disabled = model.Disabled,
            };
        }

        public static User.Model.Models.HelpPage MapFrom(this HelpPageBase model)
        {
            if (model == null)
            {
                return null;
            }

            return new User.Model.Models.HelpPage
            {
                Id = model.Id,
                Slug = model.Slug,
                Title = model.Title,
                Actions = model.Actions,
                Disabled = model.Disabled,
            };
        }

        public static void Update(this User.Model.Models.HelpPage existed, User.Model.Models.HelpPage update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Title = update.Title;
            existed.Slug = update.Slug;
            existed.Actions = update.Actions;
            existed.Disabled = update.Disabled;
        }
    }
}
