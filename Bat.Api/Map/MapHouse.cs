﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;

namespace Bat.Api.Map
{
    public static class MapHouse
    {
        public static IEnumerable<House> MapTo(this IEnumerable<Cloud.Model.Models.House> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        public static House MapTo(this Cloud.Model.Models.House model)
        {
            if (model == null)
            {
                return null;
            }

            return new House
            {
                Id = model.Id,
                Name = model.Name,
                FarmId = model.FarmId
            };
        }

        public static IEnumerable<House> MapTo(this IEnumerable<Cloud.Model.Models.House> models, Dictionary<string, string> expand)
        {
            return models?.Select(s => s.MapTo(expand)).Where(w => w != null).ToList();
        }

        public static House MapTo(this Cloud.Model.Models.House model, Dictionary<string, string> expand)
        {
            if (model == null)
            {
                return null;
            }

            var house = new House
            {
                Id = model.Id,
                Name = model.Name,
                FarmId = model.FarmId
            };

            if (expand == null || !expand.Any())
            {   //no other expand properties are needed
                return house;
            }

            if (expand.ContainsKey(nameof(house.Farm)))
            {
                house.Farm = model.Farm.MapTo(null);
            }

            return house;
        }



        public static Cloud.Model.Models.House MapFrom(this House house, Guid id)
        {
            var model = house.MapFrom();
            if (model != null)
            {
                model.Id = id;
            }
            return model;
        }


        public static Cloud.Model.Models.House MapFrom(this HouseCreate house)
        {
            if (house == null)
            {
                return null;
            }

            return new Cloud.Model.Models.House
            {
                Name = house.Name,
                FarmId = house.FarmId
            };
        }

        public static void Update(this Cloud.Model.Models.House existed, Cloud.Model.Models.House update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.FarmId = update.FarmId;
        }
    }
}
