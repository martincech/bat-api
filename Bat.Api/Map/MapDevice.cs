﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Api.DTOs;
using Bat.Api.DTOs.Enums;
using NLog;

namespace Bat.Api.Map
{
    public static class MapDevice
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static IEnumerable<Device> MapTo(this IEnumerable<Cloud.Model.Models.Device> models)
        {
            return models?.Select(MapTo).Where(w => w != null).ToList();
        }

        public static Device MapTo(this Cloud.Model.Models.Device model)
        {
            if (model == null)
            {
                return null;
            }

            try
            {
                return new Device
                {
                    Id = model.Id,
                    Name = model.Name,
                    Type = model.Type.MapTo(),
                    Unit = model.Unit.MapTo(),
                    Description = model.Description,
                    PhoneNumber = model.PhoneNumber,
                    FarmId = model.House?.FarmId ?? model.FarmId,
                    HouseId = model.HouseId,
                    ParentId = model.ParentId
                };
            }
            catch (Exception e)
            {
                logger.Debug(e, "Device mapping from DB model was fail");
                return null;
            }
        }

        public static IEnumerable<Device> MapTo(this IEnumerable<Cloud.Model.Models.Device> models, Dictionary<string, string> expand)
        {
            return models?.Select(s => s.MapTo(expand, s.GetParent(models))).Where(w => w != null).ToList();
        }

        public static Device MapTo(this Cloud.Model.Models.Device model, Dictionary<string, string> expand, Cloud.Model.Models.Device parent = null)
        {
            if (model == null)
            {
                return null;
            }

            try
            {
                var device = new Device
                {
                    Id = model.Id,
                    Name = model.Name,
                    Type = model.Type.MapTo(),
                    Unit = model.Unit.MapTo(),
                    PhoneNumber = model.PhoneNumber,
                    FarmId = parent?.House?.FarmId ?? parent?.FarmId ?? model.House?.FarmId ?? model.FarmId,
                    HouseId = parent?.HouseId ?? model.HouseId,
                    ParentId = model.ParentId
                };

                if (expand == null || !expand.Any())
                {   //no other expand properties are needed
                    return device;
                }

                if (expand.ContainsKey(nameof(device.Description)))
                {
                    device.Description = model.Description;
                }
                if (expand.ContainsKey(nameof(device.Farm)))
                {
                    var farm = parent?.House?.Farm ?? parent?.Farm ?? model.House?.Farm ?? model.Farm;
                    device.Farm = farm.MapTo(null);
                }
                if (expand.ContainsKey(nameof(device.House)))
                {
                    var house = parent?.House ?? model.House;
                    device.House = house.MapTo(null);
                }
                if (expand.ContainsKey(nameof(device.Parent)))
                {
                    device.Parent = model.Parent.MapTo(null);
                }

                return device;
            }
            catch (Exception e)
            {
                logger.Debug(e, "Device mapping from DB model was fail");
                return null;
            }
        }

        public static Cloud.Model.Models.Device GetParent(this Cloud.Model.Models.Device model, IEnumerable<Cloud.Model.Models.Device> models)
        {
            if (model == null || model.ParentId == null || models == null) return null;
            return models.FirstOrDefault(f => f.Id == model.ParentId);
        }

        private static DeviceType MapTo(this Cloud.Model.Enums.DeviceType model)
        {
            switch (model)
            {
                case Cloud.Model.Enums.DeviceType.Bat2GsmScale:
                    return DeviceType.Bat2GsmScale;
                case Cloud.Model.Enums.DeviceType.Bat2CableScale:
                    return DeviceType.Bat2CableScale;
                case Cloud.Model.Enums.DeviceType.TemperatureSensor:
                    return DeviceType.TemperatureSensor;
                case Cloud.Model.Enums.DeviceType.HumiditySensor:
                    return DeviceType.HumiditySensor;
                case Cloud.Model.Enums.DeviceType.Co2Sensor:
                    return DeviceType.CarbonDioxideSensor;
                case Cloud.Model.Enums.DeviceType.BatCollectorTerminal:
                    return DeviceType.BatCollectorTerminal;
                case Cloud.Model.Enums.DeviceType.BatGateway:
                    return DeviceType.BatGateway;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        private static Cloud.Model.Enums.DeviceType MapFrom(this DeviceType type)
        {
            switch (type)
            {
                case DeviceType.Bat2GsmScale:
                    return Cloud.Model.Enums.DeviceType.Bat2GsmScale;
                case DeviceType.Bat2CableScale:
                    return Cloud.Model.Enums.DeviceType.Bat2CableScale;
                case DeviceType.TemperatureSensor:
                    return Cloud.Model.Enums.DeviceType.TemperatureSensor;
                case DeviceType.HumiditySensor:
                    return Cloud.Model.Enums.DeviceType.HumiditySensor;
                case DeviceType.CarbonDioxideSensor:
                    return Cloud.Model.Enums.DeviceType.Co2Sensor;
                case DeviceType.BatCollectorTerminal:
                    return Cloud.Model.Enums.DeviceType.BatCollectorTerminal;
                case DeviceType.BatGateway:
                    return Cloud.Model.Enums.DeviceType.BatGateway;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }

        private static DeviceUnit MapTo(this Cloud.Model.Enums.DeviceUnit model)
        {
            switch (model)
            {
                case Cloud.Model.Enums.DeviceUnit.G:
                    return DeviceUnit.G;
                case Cloud.Model.Enums.DeviceUnit.Kg:
                    return DeviceUnit.Kg;
                case Cloud.Model.Enums.DeviceUnit.Lb:
                    return DeviceUnit.Lb;

                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        private static Cloud.Model.Enums.DeviceUnit MapFrom(this DeviceUnit type)
        {
            switch (type)
            {
                case DeviceUnit.G:
                    return Cloud.Model.Enums.DeviceUnit.G;
                case DeviceUnit.Kg:
                    return Cloud.Model.Enums.DeviceUnit.Kg;
                case DeviceUnit.Lb:
                    return Cloud.Model.Enums.DeviceUnit.Lb;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }

        public static Cloud.Model.Models.Device MapFrom(this Device device, Guid id)
        {
            var model = device.MapFrom();
            if (model != null)
            {
                model.Id = id;
            }
            return model;
        }

        public static Cloud.Model.Models.Device MapFrom(this DeviceCreate device, Guid? id)
        {
            var model = device.MapFrom();
            if (model != null && id.HasValue)
            {
                model.Id = id.Value;
            }
            return model;
        }

        public static Cloud.Model.Models.Device MapFrom(this DeviceBase device)
        {
            if (device == null)
            {
                return null;
            }

            try
            {
                return new Cloud.Model.Models.Device
                {
                    Name = device.Name,
                    Type = device.Type.MapFrom(),
                    Unit = device.Unit.MapFrom(),
                    Description = device.Description,
                    PhoneNumber = StandardizePhoneNumber(device.PhoneNumber),
                    FarmId = device.FarmId,
                    HouseId = device.HouseId,
                    ParentId = device.ParentId
                };
            }
            catch (Exception e)
            {
                logger.Debug(e, "Device mapping to DB model was fail");
                return null;
            }
        }


        public static void Update(this Cloud.Model.Models.Device existed, Cloud.Model.Models.Device update)
        {
            if (existed == null || update == null)
            {
                return;
            }

            existed.Name = update.Name;
            existed.Type = update.Type;
            existed.Unit = update.Unit;
            existed.Description = update.Description;
            existed.PhoneNumber = StandardizePhoneNumber(update.PhoneNumber);
            existed.FarmId = update.FarmId;
            existed.HouseId = update.HouseId;
            existed.ParentId = update.ParentId;
        }

        public static string StandardizePhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return null;
            var phone = new string(phoneNumber.Where(c => char.IsDigit(c)).ToArray());
            return phone.Length == 0 ? null : $"+{phone}";
        }
    }
}
