﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Bat.Api.Connection;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Statistic.Models;
using Statistic.Models.Enums;

namespace Bat.Api.Controllers.Helpers
{
    [Authorize]
    [Route("v1/[controller]")]
    [ApiController]
    public abstract class BaseApiController : ControllerBase
    {
        private const char EXPAND_PROPERTY_SEPARATOR = ',';
        private const string EXPAND_ALL = "all";
        private const int NOT_SPECIFIC_FLOCK_DURATION = 3;  //years

        protected const string AdminRole = "VeitOperator";

        protected BaseApiController(IConnectionFactory connectionFactory)
        {
            Connection = connectionFactory;
        }

        protected NLog.Logger Logger { get; } = NLog.LogManager.GetCurrentClassLogger();

        public IConnectionFactory Connection { get; private set; }

        protected static bool GuidIsValid(Guid? id)
        {
            return id.GetValueOrDefault() != Guid.Empty;
        }


        #region Response methods

        /// <summary>
        /// Return response <see cref="ApiResponse"/>
        /// </summary>
        /// <param name="code"><see cref="HttpStatusCode"/>Status code</param>
        /// <param name="message">Description</param>
        /// <returns></returns>
        protected ActionResult<ApiResponse> ApiResponse(HttpStatusCode code, string message)
        {
            var statusCode = (int)code;
            Logger.Debug($"API response: code: {statusCode}, message: {message}");
            return StatusCode(statusCode, new ApiResponse
            {
                Code = statusCode,
                Message = message
            });
        }

        /// <summary>
        /// Return response <see cref="ApiResponse"/> with status code 200 (OK)
        /// and concrete data.
        /// </summary>
        /// <typeparam name="T">type of returning data</typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ActionResult<ApiResponse> ApiResponse<T>(T data)
        {
            var response = new ApiResponse
            {
                Code = (int)HttpStatusCode.OK,
                Data = data
            };
            return Ok(response);
        }

        /// <summary>
        /// Return response <see cref="ApiResponse"/> with status code 200 (OK)
        /// </summary>
        /// <returns></returns>
        protected ActionResult<ApiResponse> ApiResponse()
        {
            return Ok(new ApiResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = Properties.Resources.Success
            });
        }



        /// <summary>
        /// Wrap create action into API response <see cref="ApiCreateResponse"/>
        /// </summary>
        /// <param name="routeName"></param>
        /// <param name="routeValue"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ActionResult<ApiResponse> CreateResponse(string routeName, object routeValue, object data)
        {
            var response = new ApiResponse
            {
                Code = (int)HttpStatusCode.Created,
                Message = Properties.Resources.Created,
                Data = data
            };
            return CreatedAtRoute(routeName, routeValue, response);
        }

        protected ActionResult<ApiResponse> UpdateResponse(object data)
        {
            var response = new ApiResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = Properties.Resources.Updated,
                Data = data
            };
            return Ok(response);
        }

        #endregion

        #region Expand helpers

        /// <summary>
        /// Return list of mapped properties names with its Attribute name
        /// </summary>
        /// <typeparam name="T">type of class which properties we wanted</typeparam>
        /// <param name="attributes"></param>
        /// <returns></returns>
        protected static Dictionary<string, string> GetMappedPropertiesWithDataMemberAttributes<T>(string attributes)
        {
            var dict = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(attributes))
            {
                return dict;
            }

            var parseAttributes = ParseExpandProperties(attributes);
            var mapping = MapPropertiesNamesWithDataMemberAttributes<T>();

            if (parseAttributes.Contains(EXPAND_ALL))
            {   //return all properties
                return mapping;
            }

            foreach (var item in mapping)
            {
                if (parseAttributes.Contains(item.Value))
                {
                    dict.Add(item.Key, item.Value);
                }
            }
            return dict;
        }

        protected static string[] ParseExpandProperties(string expand)
        {
            if (string.IsNullOrEmpty(expand))
            {
                return default(string[]);
            }
            return expand.Split(EXPAND_PROPERTY_SEPARATOR);
        }

        private static Dictionary<string, string> MapPropertiesNamesWithDataMemberAttributes<T>()
        {
            var dict = new Dictionary<string, string>();

            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                var attr = (DataMemberAttribute)property.GetCustomAttributes(typeof(DataMemberAttribute), true).FirstOrDefault(f => f is DataMemberAttribute);
                if (attr != null)
                {
                    dict.Add(property.Name, attr.Name);
                }
            }
            return dict;
        }

        #endregion


        #region Common data operation

        protected void RemoveDeviceReferencesOnHouse(Guid houseId)
        {
            var devices = Connection.CloudModel.DevicesLazy.Where(f => f.HouseId == houseId);
            if (!devices.Any())
            {
                return;
            }

            foreach (var device in devices)
            {
                device.House = null;
                device.HouseId = null;
            }
            Connection.CloudModel.Devices.UpdateRange(devices);
        }

        protected void RemoveFlockReferencesOnHouse(Guid houseId)
        {
            var flocks = Connection.CloudModel.FlocksLazy.Where(f => f.HouseId == houseId);
            if (!flocks.Any())
            {
                return;
            }

            foreach (var flock in flocks)
            {
                flock.House = null;
                flock.HouseId = null;
            }
            Connection.CloudModel.Flocks.UpdateRange(flocks);
        }

        protected void RemoveFlockReferencesOnDevice(Guid deviceId)
        {
            var flocks = Connection.CloudModel.FlocksLazy.Where(f => f.DeviceId == deviceId);
            if (!flocks.Any())
            {
                return;
            }

            foreach (var flock in flocks)
            {
                flock.Device = null;
                flock.DeviceId = null;
            }
            Connection.CloudModel.Flocks.UpdateRange(flocks);
        }

        protected bool UpdateStatisticGroupsForDevice(Cloud.Model.Models.Device device)
        {
            return UpdateStatisticGroupsForDevices(new List<Cloud.Model.Models.Device> { device });
        }

        protected bool UpdateStatisticGroupsForDevices(IEnumerable<Cloud.Model.Models.Device> devices)
        {
            var dictionary = new Dictionary<Guid, IEnumerable<FlockEvent>>();
            foreach (var device in devices)
            {
                if (device.Type == Cloud.Model.Enums.DeviceType.BatCollectorTerminal || dictionary.ContainsKey(device.Id))
                {   //Terminal is not used as statistic group, only standard sensors can be
                    continue;
                }

                var events = GetDeviceFlockEvents(device);
                dictionary.Add(device.Id, events);
            }
            return Connection.GroupProducer.UpdateDeviceGroups(dictionary);
        }

        private IEnumerable<FlockEvent> GetDeviceFlockEvents(Cloud.Model.Models.Device device)
        {
            var deviceFlockEvents = new List<FlockEvent>();
            var activeOrFutureFlocks = Connection.CloudModel.FlocksLazy.Where(w => !w.EndDate.HasValue || DateTimeOffset.Compare(w.EndDate.Value, DateTime.Now) > 0);
            foreach (var flock in activeOrFutureFlocks)
            {
                if (!IsDeviceBelongsToFlock(flock, device))
                {
                    continue;
                }
                deviceFlockEvents.Add(MapFlockEvent(flock));
            }
            return deviceFlockEvents;
        }


        private bool IsDeviceBelongsToFlock(Cloud.Model.Models.Flock flock, Cloud.Model.Models.Device device)
        {
            if (IsDeviceBelongsToFlockByDeviceReference(flock, device))
            {
                return true;
            }
            return IsDeviceBelongsToFlockByHouseReference(flock, device);
        }

        private static bool IsDeviceBelongsToFlockByDeviceReference(Cloud.Model.Models.Flock flock, Cloud.Model.Models.Device device)
        {
            return (flock.DeviceId.HasValue &&                                                      //referenced by device:
                   (flock.DeviceId.Value == device.Id ||                                            //-directly
                   (device.ParentId.HasValue && device.ParentId.Value == flock.DeviceId.Value)));   //-by terminal
        }

        private bool IsDeviceBelongsToFlockByHouseReference(Cloud.Model.Models.Flock flock, Cloud.Model.Models.Device device)
        {
            if (!flock.HouseId.HasValue)
            {
                return false;
            }
            if (device.HouseId.HasValue && device.HouseId.Value == flock.HouseId.Value)
            {
                return true;
            }
            return IsDeviceBelongsToFlockByTerminalReference(flock, device);
        }

        private bool IsDeviceBelongsToFlockByTerminalReference(Cloud.Model.Models.Flock flock, Cloud.Model.Models.Device device)
        {
            if (device.ParentId.HasValue)                                               //-by terminal
            {
                var parent = Connection.CloudModel.Devices.FirstOrDefault(f => f.Id == device.ParentId.Value);
                if (parent != null && parent.HouseId.HasValue && parent.HouseId.Value == flock.HouseId.Value)
                {
                    return true;
                }
            }
            return false;
        }

        protected static FlockEvent MapFlockEvent(Cloud.Model.Models.Flock flock)
        {
            var endDate = flock.EndDate ?? flock.StartDate.Date.AddYears(NOT_SPECIFIC_FLOCK_DURATION);
            return new FlockEvent
            {
                Id = flock.Id,
                From = flock.StartDate,
                To = endDate
            };
        }


        protected async Task<IEnumerable<DailyStatistic>> GetLastWeightStatisticsAsync(IEnumerable<Guid> ids, StatisticGroup group, AggregationInterval interval)
        {
            var companyId = GetCurrentUserCompany();
            var stats = await Connection.StatisticContext.ReadLastWeightStatisticsAsync(
                companyId.ToString(), ids, group, interval);
            var dailyStatistics = stats.MapTo();
            if (group == StatisticGroup.Device)
            {
                dailyStatistics = DeviceStatisticsFromActiveFlock(dailyStatistics);
            }
            return dailyStatistics;
        }

        /// <summary>
        /// Filter device statistics. Returns only data from an active flock.
        /// If device has no active flock, returns empty collection.
        /// </summary>
        /// <param name="dailyStatistics"></param>
        /// <returns></returns>
        private IEnumerable<DailyStatistic> DeviceStatisticsFromActiveFlock(IEnumerable<DailyStatistic> dailyStatistics)
        {
            var deviceStatisticsFromActiveFlock = new List<DailyStatistic>();
            var groupStatistics = dailyStatistics.Where(w => w.DeviceId.HasValue).GroupBy(g => g.DeviceId.Value);
            foreach (var group in groupStatistics)
            {
                var device = Connection.CloudModel.DevicesLazy.FirstOrDefault(f => f.Id == group.Key);
                if (device == null)
                {
                    continue;
                }
                var events = GetDeviceFlockEvents(device);
                foreach (var stats in group)
                {
                    if (IsStatisticBelongsToEvent(stats, events))
                    {
                        deviceStatisticsFromActiveFlock.Add(stats);
                    }
                }
            }
            return deviceStatisticsFromActiveFlock;
        }


        private static bool IsStatisticBelongsToEvent(DailyStatistic stats, IEnumerable<FlockEvent> events)
        {
            if (events == null || !events.Any())
            {
                return false;
            }
            return events.FirstOrDefault(f => stats.DateTime >= f.From && stats.DateTime < f.To) != null;
        }

        protected Guid GetCurrentUserCompany()
        {
            var currentUser = Connection.CurrentUser;
            if (currentUser == null)
            {
                throw new MemberAccessException("No user is logged!");
            }
            if (!currentUser.CompanyId.HasValue)
            {
                throw new MemberAccessException("Current logged user has assigned no company!");
            }
            return currentUser.CompanyId.Value;
        }

        protected async Task<Cloud.Model.Models.Device> GetParentAsync(Cloud.Model.Models.Device device)
        {
            return device?.ParentId != null
                ? await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id.Equals(device.ParentId))
                : null;
        }

        #endregion
    }
}
