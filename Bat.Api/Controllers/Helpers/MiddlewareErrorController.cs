﻿using System.Net;
using Bat.Api.DTOs;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace Bat.Api.Controllers.Helpers
{
    [ApiController]
    [Route("/Error")]
    public class MiddlewareErrorController : ControllerBase
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        [HttpPost]
        [HttpPut]
        [HttpDelete]
        public IActionResult Error([FromQuery] int status = 400)
        {
            var msg = "";
            switch(status)
            {
                case (int)HttpStatusCode.NotFound:
                    msg = GetOriginalRequestUrl();
                    break;
                default:
                    msg = GetMessageFromHeaders();
                    break;
            }


            logger.Warn($"Error controller: code: {status}, message: {msg}");
            return new ObjectResult(new ApiResponse
            {
                Code = status,
                Message = msg
            });
        }

        private string GetMessageFromHeaders()
        {
            var builder = new System.Text.StringBuilder();
            foreach (var header in HttpContext.Response.Headers)
            {
                builder.Append($"{header.Key}={header.Value};");
            }
            return builder.ToString();
        }

        private string GetOriginalRequestUrl()
        {
            var feature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            return $"{Request.Scheme}://{Request.Host}{feature?.OriginalPath}{feature?.OriginalQueryString}";
        }
    }
}
