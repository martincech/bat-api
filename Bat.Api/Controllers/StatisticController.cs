﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Statistic.Models.Enums;

namespace Bat.Api.Controllers
{
    [CommonValidation]
    public class StatisticController : BaseApiController
    {
        public StatisticController(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("allByDevice/{deviceId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAllStatisticByDeviceIdAsync([FromRoute][Required]Guid deviceId)
        {
            var device = await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id.Equals(deviceId));
            if (device == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Device)));
            }

            var currentDeviceFlock = GetDeviceCurrentFlock(device);
            if (currentDeviceFlock == null)
            {   //device is not assigned to any active flock
                return ApiResponse(new List<DailyStatistic>());
            }

            var companyId = GetCurrentUserCompany();
            var stats = await Connection.StatisticContext.ReadAllWeightStatisticsAsync(
                companyId.ToString(), deviceId, StatisticGroup.Device, AggregationInterval.Day, currentDeviceFlock.StartDate, currentDeviceFlock.EndDate);

            UpdateStatsDay(stats, currentDeviceFlock);

            return ApiResponse(stats.MapTo());
        }

        private Cloud.Model.Models.Flock GetDeviceCurrentFlock(Cloud.Model.Models.Device device)
        {
            var current = DateTimeOffset.Now;
            return Connection.CloudModel.FlocksLazy.FirstOrDefault(f =>
                   (f.DeviceId.HasValue && f.DeviceId.Value == device.Id ||                                     //referenced by device
                       device.HouseId.HasValue && f.HouseId.HasValue && f.HouseId == device.HouseId.Value) &&   //referenced by house
                   (DateTimeOffset.Compare(f.StartDate, current) <= 0 &&
                        (!f.EndDate.HasValue || DateTimeOffset.Compare(f.EndDate.Value, current) > 0)));              //active flock
        }

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("allByFlock/{flockId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAllStatisticByFlockIdAsync([FromRoute][Required]Guid flockId)
        {
            var flock = await Connection.CloudModel.FlocksLazy.FirstOrDefaultAsync(f => f.Id.Equals(flockId));
            if (flock == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Flock)));
            }

            var companyId = GetCurrentUserCompany();
            var stats = await Connection.StatisticContext.ReadAllWeightStatisticsAsync(
                    companyId.ToString(), flockId, StatisticGroup.Flock, AggregationInterval.Day, flock.StartDate, flock.EndDate);

            UpdateStatsDay(stats, flock);

            return ApiResponse(stats.MapTo());
        }

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <param name="body">collection of UUID v4 ID of Flocks</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("lastByFlock")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetLastStatisticsByFlockIdsAsync([FromBody]IEnumerable<Guid> body)
        {
            if (!body.Any())
            {
                ApiResponse<IEnumerable<DailyStatistic>>(null);
            }

            var existedFlockIds = Connection.CloudModel.Flocks.Where(w => body.Contains(w.Id)).Select(s => s.Id);
            var stats = await GetLastWeightStatisticsAsync(existedFlockIds, StatisticGroup.Flock, AggregationInterval.Day);
            return ApiResponse(stats);
        }

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <param name="body">collection of UUID v4 ID of Devices</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("lastByDevice")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetLastStatisticsByDeviceIdsAsync([FromBody]IEnumerable<Guid> body)
        {
            if (!body.Any())
            {
                ApiResponse<IEnumerable<DailyStatistic>>(null);
            }

            var existedDeviceIds = Connection.CloudModel.Devices.Where(w => body.Contains(w.Id)).Select(s => s.Id);
            var stats = await GetLastWeightStatisticsAsync(existedDeviceIds, StatisticGroup.Device, AggregationInterval.Day);
            return ApiResponse(stats);
        }

        static void UpdateStatsDay(Statistic.Models.GroupData stats, Cloud.Model.Models.Flock flock)
        {
            if (stats?.Values != null && flock != null)
            {
                foreach (var stat in stats.Values.Where(w => !w.Day.HasValue))
                {
                    stat.Day = flock.InitialAge + (int)(stat.TimeStamp.Date - flock.StartDate.Date).TotalDays;
                }
            }
        }
    }
}
