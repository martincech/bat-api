﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Bird.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Statistic.Models.Enums;

namespace Bat.Api.Controllers
{
    [CommonValidation("flockId")]
    public class FlockController : BaseApiController
    {
        private const string GET_BY_ID = "GetFlockById";
        private readonly BirdModelContainer birdModel;

        public FlockController(IConnectionFactory connectionFactory, BirdModelContainer birdModel)
            : base(connectionFactory)
        {
            this.birdModel = birdModel ?? throw new ArgumentOutOfRangeException(nameof(birdModel));
        }


        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromQuery]string expand = null)
        {
            var flocks = await Connection.CloudModel.FlocksLazy.ToListAsync();

            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<Flock>(expand);
            var mappedFlocks = await MapFlockAsync(flocks, expandDictionary);
            return ApiResponse(mappedFlocks);
        }

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{flockId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid flockId, [FromQuery]string expand = null)
        {
            var flock = await Connection.CloudModel.FlocksLazy.FirstOrDefaultAsync(f => f.Id.Equals(flockId));
            if (flock == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Flock)));
            }

            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<Flock>(expand);
            var mappedFlock = await MapFlockAsync(flock, expandDictionary);
            return ApiResponse(mappedFlock);
        }


        private async Task<Flock> MapFlockAsync(Cloud.Model.Models.Flock model, Dictionary<string, string> expandDictionary)
        {
            var mappedFlocks = await MapFlockAsync(new List<Cloud.Model.Models.Flock> { model }, expandDictionary);
            return mappedFlocks.FirstOrDefault();
        }

        private async Task<IEnumerable<Flock>> MapFlockAsync(IEnumerable<Cloud.Model.Models.Flock> models, Dictionary<string, string> expandDictionary)
        {
            var mappedFlocks = models.MapTo(expandDictionary);
            if (expandDictionary.ContainsKey(nameof(Flock.Bird)))
            {
                LoadBirdsToFlocks(mappedFlocks);
            }
            if (expandDictionary.ContainsKey(nameof(Flock.LastStatistics)))
            {
                await LoadLastStatisticsToFlocksAsync(mappedFlocks);
            }
            return mappedFlocks;
        }

        private void LoadBirdsToFlocks(IEnumerable<Flock> flocks)
        {
            foreach (var flock in flocks)
            {
                LoadBirdToFlock(flock);
            }
        }

        private void LoadBirdToFlock(Flock flock)
        {
            if (!flock.BirdId.HasValue)
            {
                flock.Bird = null;
                return;
            }

            DTOs.Bird mappedBird = null;
            var scope = DTOs.Enums.Scope.Private;
            var bird = Connection.CloudModel.BirdsLazy.FirstOrDefault(f => f.Id == flock.BirdId.Value);
            if (bird == null)
            {
                bird = birdModel.BirdsLazy.FirstOrDefault(f => f.Id == flock.BirdId.Value);
                if (bird == null)
                {
                    Logger.Error($"Flock contains bird reference: {flock.BirdId.Value} on NOT existing object!");
                    flock.BirdId = null;
                    return;
                }
                scope = DTOs.Enums.Scope.Global;
            }

            mappedBird = bird.MapTo();
            mappedBird.Scope = scope;
            flock.Bird = mappedBird;
        }

        #region Load statistics

        private async Task LoadLastStatisticsToFlocksAsync(IEnumerable<Flock> flocks)
        {
            var ids = flocks.Select(s => s.Id);
            var stats = await GetLastWeightStatisticsAsync(ids, StatisticGroup.Flock, AggregationInterval.Day);

            foreach (var group in stats.GroupBy(g => g.FlockId))
            {
                var flock = flocks.FirstOrDefault(f => f.Id == group.Key.Value);
                if (flock == null)
                {
                    Logger.Warn($"Statistic for non exist flock was read, flock id: {group.Key}");
                    continue;
                }
                flock.LastStatistics = group.ToList();
            }
        }

        #endregion


        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>&#39;You may create a flock.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]FlockCreate body)
        {
            var newFlock = body.MapFrom();
            if (!IsValidRelationshipsProperties(ref newFlock, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                await Connection.CloudModel.Flocks.AddAsync(newFlock);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroups(newFlock))
                {
                    transaction.Commit();
                    return CreateResponse(GET_BY_ID, new { flockId = newFlock.Id }, newFlock.MapTo());
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(Flock)));
            }
        }


        private bool UpdateStatisticGroups(Cloud.Model.Models.Flock flock)
        {
            var devices = GetFlockDevices(flock);
            return UpdateStatisticGroupsForDevices(devices);
        }


        /// <summary>
        /// Return flock's devices
        /// </summary>
        /// <param name="flock"></param>
        /// <returns></returns>
        private IEnumerable<Cloud.Model.Models.Device> GetFlockDevices(Cloud.Model.Models.Flock flock)
        {
            if (flock.Device != null)
            {
                return GetTerminalSensors(flock.Device);
            }

            //flock cannot have both references (on device and house together)
            if (flock.House != null)
            {
                return GetHouseSensors(flock.HouseId.Value);
            }
            return new List<Cloud.Model.Models.Device>();
        }

        private IEnumerable<Cloud.Model.Models.Device> GetTerminalSensors(Cloud.Model.Models.Device device)
        {
            if (device.Type != Cloud.Model.Enums.DeviceType.BatCollectorTerminal)
            {   //device is standard sensor
                return new List<Cloud.Model.Models.Device> { device };
            }

            //device is terminal, get its sensors
            return Connection.CloudModel.DevicesLazy.Where(w => w.ParentId.HasValue && w.ParentId.Value == device.Id);
        }


        private IEnumerable<Cloud.Model.Models.Device> GetHouseSensors(Guid houseId)
        {
            var sensorDevices = new List<Cloud.Model.Models.Device>();
            var houseDevices = Connection.CloudModel.DevicesLazy.Where(w => w.HouseId != null && w.HouseId == houseId).ToList();
            foreach (var device in houseDevices)
            {
                if (device.Type != Cloud.Model.Enums.DeviceType.BatCollectorTerminal)
                {
                    sensorDevices.Add(device);
                }
                else
                {   //Device type is terminal
                    var terminalDevices = Connection.CloudModel.DevicesLazy.Where(w => w.ParentId.HasValue && w.ParentId.Value == device.Id);
                    sensorDevices.AddRange(terminalDevices);
                }
            }
            return sensorDevices;
        }

        /// <summary>
        /// Check if foreign keys referencing on valid properties.
        /// </summary>
        /// <param name="flock"></param>
        /// <param name="errMessage"></param>
        /// <param name="checkRefernce"></param>
        /// <returns></returns>
        private bool IsValidRelationshipsProperties(ref Cloud.Model.Models.Flock flock, out string errMessage, bool checkRefernce = true)
        {
            errMessage = Properties.Resources.PreconditionError;
            if (flock == null)
            {
                return false;
            }

            if (checkRefernce && !VerifyReferenceInDb(ref flock, ref errMessage))
            {
                return false;
            }

            if (!flock.TargetAge.HasValue && !flock.TargetWeight.HasValue)
            {
                errMessage = "Flock must have set target age or target weight";
                return false;
            }
            return true;
        }


        /// <summary>
        /// Check referenced objects if its really exist in DB
        /// </summary>
        /// <param name="flock"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private bool VerifyReferenceInDb(ref Cloud.Model.Models.Flock flock, ref string errMessage)
        {
            var houseIsValid = GuidIsValid(flock.HouseId);
            var deviceIsValid = GuidIsValid(flock.DeviceId);
            if (!houseIsValid && !deviceIsValid)
            {
                errMessage = "Flock must have reference on a house or a device";
                return false;
            }


            if (houseIsValid)
            {
                var houseId = flock.HouseId.Value;
                flock.House = Connection.CloudModel.Houses.FirstOrDefault(f => f.Id == houseId);
                if (flock.House == null)
                {
                    errMessage = "Referenced house is not found";
                    return false;
                }
            }

            if (deviceIsValid)
            {
                var deviceId = flock.DeviceId.Value;
                flock.Device = Connection.CloudModel.Devices.FirstOrDefault(f => f.Id == deviceId);
                if (flock.Device == null)
                {
                    errMessage = "Referenced device is not found";
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{flockId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute][Required]Guid flockId, [FromBody]Flock body)
        {
            var updateModel = body.MapFrom(flockId);

            var existedModel = await Connection.CloudModel.FlocksLazy.FirstOrDefaultAsync(f => f.Id == flockId);
            var hasReference = existedModel == null || existedModel.HouseId != null || existedModel.DeviceId != null;
            if (!IsValidRelationshipsProperties(ref updateModel, out var errMessage, hasReference))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Flock)));
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                var existedDevices = GetFlockDevices(existedModel).ToList();
                existedModel.Update(updateModel);
                Connection.CloudModel.Flocks.Update(existedModel);
                await Connection.CloudModel.SaveChangesAsync();

                var updatedDevices = GetFlockDevices(updateModel);
                if (UpdateStatisticGroupsForDevices(GetUniqueDevices(existedDevices, updatedDevices)))
                {
                    transaction.Commit();
                    return UpdateResponse(existedModel.MapTo());
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(Flock)));
            }
        }

        private static IEnumerable<Cloud.Model.Models.Device> GetUniqueDevices(List<Cloud.Model.Models.Device> devices1, IEnumerable<Cloud.Model.Models.Device> devices2)
        {
            var list = devices1.ToList();
            foreach (var device in devices2)
            {
                if (list.FirstOrDefault(f => f.Id == device.Id) == null)
                {
                    list.Add(device);
                }
            }
            return list;
        }

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{flockId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute][Required]Guid flockId)
        {
            var flock = await Connection.CloudModel.FlocksLazy.FirstOrDefaultAsync(f => f.Id.Equals(flockId));
            if (flock == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Flock)));
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                var existedDevices = GetFlockDevices(flock).ToList();
                Connection.CloudModel.Flocks.Remove(flock);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroupsForDevices(existedDevices))
                {
                    transaction.Commit();
                    return ApiResponse();
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(Flock)));
            }
        }
    }
}
