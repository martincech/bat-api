﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.DTOs.Enums;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.Model.Enums;

namespace Bat.Api.Controllers
{
    [CommonValidation("companyId")]
    public class CompanyController : BaseApiController
    {
        private const string GET_BY_ID = "GetCompanyById";
        private readonly IContainerProvider containerProvider;

        public CompanyController(IConnectionFactory connectionFactory, IContainerProvider containerProvider)
            : base(connectionFactory)
        {
            this.containerProvider = containerProvider ?? throw new ArgumentNullException(nameof(containerProvider));
        }


        /// <summary>
        /// List All Companies
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync()
        {
            if (!Connection.IsInRole(Role.VeitOperator))
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }

            var companies = await Connection.UserModel.Companies.ToListAsync();
            return ApiResponse(companies.MapTo());
        }


        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{companyId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid companyId)
        {
            var currentCompanyId = Connection.CurrentUser?.CompanyId;
            if (!Connection.IsInRole(Role.VeitOperator) &&                                // administrator: can get any company
               (!currentCompanyId.HasValue || currentCompanyId.Value != companyId))     // common user: can get only his company
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }

            return await GetCompanyResponseAsync(companyId);
        }


        /// <summary>
        /// Update Company info
        /// </summary>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{companyId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute][Required]Guid companyId, [FromBody]Company body)
        {
            var currentCompanyId = Connection.CurrentUser?.CompanyId;
            var updateModel = body?.MapFrom();
            if (currentCompanyId.HasValue)
            {
                updateModel.Id = currentCompanyId.Value;
            }
            if (!CanUpdateCompany(ref updateModel, companyId, out var errCode, out var errMessage))
            {
                return ApiResponse(errCode, errMessage);
            }

            return await UpdateCompanyRoutineAsync(companyId, updateModel);
        }

        private async Task<ActionResult<ApiResponse>> UpdateCompanyRoutineAsync(Guid companyId, User.Model.Models.Company updateModel)
        {
            var existedModel = await Connection.UserModel.Companies.FirstOrDefaultAsync(f => f.Id == companyId);
            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Company)));
            }

            existedModel.Update(updateModel);
            Connection.UserModel.Companies.Update(existedModel);
            await Connection.UserModel.SaveChangesAsync();
            return UpdateResponse(existedModel.MapTo());
        }


        private bool CanUpdateCompany(ref User.Model.Models.Company company, Guid? companyId, out HttpStatusCode errCode, out string errMessage)
        {
            errMessage = Properties.Resources.PreconditionError;
            errCode = HttpStatusCode.PreconditionFailed;
            if (company == null)
            {
                return false;
            }

            var isAdmin = Connection.IsInRole(Role.VeitOperator);
            if (!isAdmin)
            {
                if (!companyId.HasValue)
                {
                    errCode = HttpStatusCode.InternalServerError;
                    errMessage = "User doesn't have assigned any company";
                    return false;
                }
                else if (companyId.Value != company.Id)
                {
                    errCode = HttpStatusCode.Forbidden;
                    errMessage = "You are trying update a foreign company!";
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// Create a new company
        /// </summary>
        /// <remarks>&#39;You may create a company.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody][Required]CompanyCreate body)
        {
            if (!Connection.IsInRole(Role.VeitOperator))
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }

            var newCompany = body.MapFrom();
            if (newCompany == null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, Properties.Resources.PreconditionError);
            }

            //create influx db
            if (!await CreateStatisticContextAsync(newCompany.Id))
            {
                var errorMessage = Properties.Resources.InfluxCreateFailed;
                Logger.Error(errorMessage);
                return ApiResponse(HttpStatusCode.InternalServerError, errorMessage);
            }

            //create company in user DB
            await Connection.UserModel.Companies.AddAsync(newCompany);
            await Connection.UserModel.SaveChangesAsync();

            //create company DB
            await containerProvider.CreateCompanyDatabaseAsync(newCompany.Id);
            return CreateResponse(GET_BY_ID, new { companyId = newCompany.Id }, newCompany.MapTo());
        }


        private async Task<bool> CreateStatisticContextAsync(Guid companyId)
        {
            var name = companyId.ToString();
            if (!await Connection.StatisticContext.ExistCompanyContextAsync(name) && !await Connection.StatisticContext.CreateCompanyContextAsync(name))
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Delete Company
        /// </summary>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{companyId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync(Guid companyId)
        {
            if (!Connection.IsInRole(Role.VeitOperator))
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }

            //delete from user DB
            var company = await Connection.UserModel.Companies.FirstOrDefaultAsync(f => f.Id.Equals(companyId));
            if (company == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Company)));
            }
            Connection.UserModel.Companies.Remove(company);

            //remove all company users except super administrators
            var excludeRole = DbRole.VeitOperator.ToString();
            var users = Connection.UserModel.UsersLazy.Where(w => w.CompanyId == companyId && w.Roles.FirstOrDefault(f => f.Name == excludeRole) == null).ToList();
            Connection.UserModel.Users.RemoveRange(users);
            await Connection.UserModel.SaveChangesAsync();

            //delete influx db
            if (!await DropStatisticContextAsync(companyId))
            {
                var errorMessage = Properties.Resources.InfluxDropFailed;
                Logger.Error(errorMessage);
                return ApiResponse(HttpStatusCode.InternalServerError, errorMessage);
            }

            //delete whole company model DB
            await containerProvider.DeleteCompanyDatabaseAsync(companyId);
            return ApiResponse();
        }

        private async Task<bool> DropStatisticContextAsync(Guid companyId)
        {
            var name = companyId.ToString();
            if (!await Connection.StatisticContext.ExistCompanyContextAsync(name))
            {
                return true;
            }
            return await Connection.StatisticContext.DropCompanyContextAsync(name);
        }


        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("current")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetCurrentAsync()
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!companyId.HasValue)
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Current user has no company assigned!");
            }

            return await GetCompanyResponseAsync(companyId.Value);
        }

        private async Task<ActionResult<ApiResponse>> GetCompanyResponseAsync(Guid companyId)
        {
            var company = await Connection.UserModel.Companies.FirstOrDefaultAsync(f => f.Id == companyId);
            if (company == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Company)));
            }
            return ApiResponse(company.MapTo());
        }

        /// <summary>
        /// Update Company info
        /// </summary>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("current")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutCurrentAsync([FromBody][Required]CompanyCreate body)
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            var updateModel = body?.MapFrom();
            if (companyId.HasValue)
            {
                updateModel.Id = companyId.Value;
            }
            if (!CanUpdateCompany(ref updateModel, companyId, out var errCode, out var errMessage))
            {
                return ApiResponse(errCode, errMessage);
            }

            return await UpdateCompanyRoutineAsync(companyId.Value, updateModel);
        }
    }
}
