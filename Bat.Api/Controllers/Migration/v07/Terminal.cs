﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Bat.Api.Controllers.Migration.v07
{
    [DataContract]
    public class Terminal
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }


        /// <summary>
        /// Gets or Sets old id
        /// </summary>
        [Required]
        [DataMember(Name = "oldId")]
        public string OldId { get; set; }
    }
}
