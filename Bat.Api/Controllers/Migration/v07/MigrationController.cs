﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using MigrationV07.Model;

namespace Bat.Api.Controllers.Migration.v07
{
    [CommonValidation("oldId")]
    public class MigrationController : BaseApiController
    {
        private const string MigrationVersion = "0.7";
        private readonly MigrationV07ModelContainer migrationContainer;

        public MigrationController(IConnectionFactory connectionFactory, MigrationV07ModelContainer migrationContainer)
            : base(connectionFactory)
        {
            this.migrationContainer = migrationContainer ?? throw new ArgumentNullException(nameof(migrationContainer));
        }


        /// <summary>
        /// Retrieve Device new UUID
        /// </summary>
        /// <param name="oldId">Old ID of Device to return (format of id is: 'device_address-T-old_terminal_id')</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet(MigrationVersion + "/device/{oldId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetDeviceIdAsync([FromRoute][Required]string oldId)
        {
            return await Task.Run(() =>
            {
                if (!MigrationDatabaseExist())
                {
                    return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(MigrationV07.Model.Models.Device)));
                }

                if (!TryGetCurrentCompany(out var company))
                {
                    return ApiResponse(HttpStatusCode.NotFound, Properties.Resources.NoAssignedCompany);
                }

                if (!oldId.Contains("-T-", StringComparison.Ordinal))
                {
                    return ApiResponse(HttpStatusCode.BadRequest, $"Selected id: '{oldId}' is not in correct form 'DEVICE_ADDRESS-T-TERMINAL_ID'");
                }

                var record = company.Devices.FirstOrDefault(f => string.Equals(f.OldUid, oldId, StringComparison.OrdinalIgnoreCase));
                if (record == null)
                {
                    return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(MigrationV07.Model.Models.Device)));
                }
                return ApiResponse(record.Id);
            });
        }

        private bool MigrationDatabaseExist()
        {
            try
            {
                if (migrationContainer.Database.IsInMemory())
                {   //for unit tests is used in memory DB
                    return true;
                }

                var exist = (migrationContainer.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
                if (!exist)
                {
                    Logger.Info("Migration database v07 doesn't exist!");
                }
                return exist;
            }
            catch (Exception e)
            {
                Logger.Warn(e, "Check migration DB exist exception");
                return false;
            }
        }

        private bool TryGetCurrentCompany(out MigrationV07.Model.Models.Company company)
        {
            company = null;
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!GuidIsValid(companyId))
            {
                return false;
            }

            company = migrationContainer.CompaniesLazy.FirstOrDefault(f => f.Id == companyId);
            return company != null;
        }

        /// <summary>
        /// Create Terminal ID mapping.
        /// </summary>
        /// <param name="oldId">Old ID of Terminal</param>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut(MigrationVersion + "/terminal/{oldId}")]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutTerminalAsync([FromRoute][Required]string oldId, [FromBody][Required]Terminal body)
        {
            if (!MigrationDatabaseExist())
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Terminal)));
            }

            if (!TryGetCurrentCompany(out var company))
            {
                return ApiResponse(HttpStatusCode.NotFound, Properties.Resources.NoAssignedCompany);
            }

            var migrationRecord = company.Devices.FirstOrDefault(f => string.Equals(f.OldUid, oldId, StringComparison.OrdinalIgnoreCase));
            if (migrationRecord == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Terminal)));
            }

            if (migrationRecord.Id == body.Id)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, "Selected terminal is already updated.");
            }

            if (await UpdateTerminalIdAsync(company, migrationRecord, body.Id, oldId))
            {
                return UpdateResponse(body);
            }
            return ApiResponse(HttpStatusCode.PreconditionFailed, "Updated terminal is not used. Migration is redundant.");
        }

        private async Task<bool> UpdateTerminalIdAsync(MigrationV07.Model.Models.Company company, MigrationV07.Model.Models.Device migrationRecord, Guid newId, string oldId)
        {
            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                try
                {
                    var terminal = Connection.CloudModel.Devices.FirstOrDefault(f => f.Id == migrationRecord.Id);
                    if (terminal == null)
                    {
                        await UpdateTerminalMigrationAsync(company, migrationRecord, newId, oldId);
                        transaction.Commit();
                        return false;
                    }

                    //add new record, update references and delete the old one
                    Connection.CloudModel.Devices.Add(new Cloud.Model.Models.Device
                    {
                        Id = newId,
                        Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal
                    });
                    UpdateTerminalReferences(migrationRecord.Id, newId);
                    Connection.CloudModel.Devices.Remove(terminal);
                    Connection.CloudModel.SaveChanges();

                    await UpdateTerminalMigrationAsync(company, migrationRecord, newId, oldId);
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Update Terminal ID exception");
                    transaction.Rollback();
                    throw;
                }
            }
        }

        private async Task UpdateTerminalMigrationAsync(MigrationV07.Model.Models.Company company, MigrationV07.Model.Models.Device migrationRecord, Guid newId, string oldId)
        {
            company.Devices.Add(new MigrationV07.Model.Models.Device
            {
                Id = newId,
                OldUid = oldId,
                Company = company
            });
            migrationContainer.Devices.Remove(migrationRecord);
            await migrationContainer.SaveChangesAsync();
        }

        private void UpdateTerminalReferences(Guid oldTerminalId, Guid updatedTerminalId)
        {
            UpdateTerminalReferencesOnDevices(oldTerminalId, updatedTerminalId);
            UpdateTerminalReferencesOnFlocks(oldTerminalId, updatedTerminalId);
        }

        private void UpdateTerminalReferencesOnDevices(Guid oldTerminalId, Guid updatedTerminalId)
        {
            var terminalDevices = Connection.CloudModel.Devices.Where(w => w.ParentId == oldTerminalId);
            foreach (var dev in terminalDevices)
            {
                dev.ParentId = updatedTerminalId;
            }
            Connection.CloudModel.Devices.UpdateRange(terminalDevices);
        }

        private void UpdateTerminalReferencesOnFlocks(Guid oldTerminalId, Guid updatedTerminalId)
        {
            var terminalFlocks = Connection.CloudModel.Flocks.Where(w => w.DeviceId == oldTerminalId);
            foreach (var flock in terminalFlocks)
            {
                flock.DeviceId = updatedTerminalId;
            }
            Connection.CloudModel.Flocks.UpdateRange(terminalFlocks);
        }
    }
}
