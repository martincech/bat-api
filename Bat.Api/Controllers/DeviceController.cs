﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Statistic.Models;
using Statistic.Models.Enums;
using Veit.Kafka.Topics;

namespace Bat.Api.Controllers
{
    [CommonValidation("deviceId")]
    public class DeviceController : BaseApiController
    {
        private const string GET_BY_ID = "GetDeviceById";

        public DeviceController(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }


        /// <summary>
        /// List All Devices
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromQuery]string expand = null)
        {
            var devices = await Connection.CloudModel.DevicesLazy.ToListAsync();
            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<DTOs.Device>(expand);
            var mappedDevices = devices.MapTo(expandDictionary);
            if (expandDictionary.ContainsKey(nameof(DTOs.Device.LastStatistics)))
            {
                await LoadLastStatisticsToDevicesAsync(mappedDevices);
            }
            return ApiResponse(mappedDevices);
        }


        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{deviceId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid deviceId, [FromQuery]string expand = null)
        {
            var device = await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id.Equals(deviceId));
            if (device == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Device)));
            }

            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<DTOs.Device>(expand);
            var parent = await GetParentAsync(device);
            var mappedDevice = device.MapTo(expandDictionary, parent);
            if (expandDictionary.ContainsKey(nameof(DTOs.Device.LastStatistics)))
            {
                await LoadLastStatisticsToDevicesAsync(mappedDevice);
            }
            return ApiResponse(mappedDevice);
        }


        #region Load statistics

        private async Task LoadLastStatisticsToDevicesAsync(IEnumerable<DTOs.Device> devices)
        {
            var ids = devices.Where(w => w.Type != DTOs.Enums.DeviceType.BatCollectorTerminal).Select(s => s.Id);   //exclude terminals (don't have statistics)
            var stats = await GetLastWeightStatisticsAsync(ids, StatisticGroup.Device, AggregationInterval.Day);

            foreach (var group in stats.GroupBy(g => g.DeviceId))
            {
                var device = devices.FirstOrDefault(f => f.Id == group.Key.Value);
                if (device == null)
                {
                    Logger.Warn($"Statistic for non exist device was read, device id: {group.Key}");
                    continue;
                }
                device.LastStatistics = group.ToList();
            }
        }

        private async Task LoadLastStatisticsToDevicesAsync(DTOs.Device device)
        {
            await LoadLastStatisticsToDevicesAsync(new List<DTOs.Device> { device });
        }

        #endregion


        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>&#39;You may create a device.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]DeviceCreate body)
        {
            var newDevice = body.MapFrom(body?.Id);
            if (!IsValidRelationshipsProperties(ref newDevice, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }
            if (body.Id.HasValue && Connection.CloudModel.Devices.FirstOrDefault(f => f.Id == body.Id.Value) != null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Selected GUID: '{body.Id.Value}' has been already used for another device!");
            }
            if (body.Type == DTOs.Enums.DeviceType.Bat2GsmScale && !IsPhoneNumberAvailable(body.PhoneNumber))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"GSM scale with phone number {body.PhoneNumber} already exist!");
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                CopyReferencesFromParent(newDevice);

                await Connection.CloudModel.Devices.AddAsync(newDevice);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroupsForDevice(newDevice) && UpdateDeviceMap(newDevice))
                {
                    transaction.Commit();
                    var parent = await GetParentAsync(newDevice);
                    return CreateResponse(GET_BY_ID, new { deviceId = newDevice.Id }, newDevice.MapTo(null, parent));
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(DTOs.Device)));
            }
        }

        private bool IsPhoneNumberAvailable(string phoneNumber, Guid? deviceId = null)
        {
            var phone = MapDevice.StandardizePhoneNumber(phoneNumber);
            if (!string.IsNullOrEmpty(phone))
            {
                return !Connection
                 .CloudModel
                 .Devices
                 .Where(w => w.Type == Cloud.Model.Enums.DeviceType.Bat2GsmScale && (deviceId == null || deviceId.Value != w.Id))
                 .Select(s => MapDevice.StandardizePhoneNumber(s.PhoneNumber))
                 .Contains(phone);
            }
            return true;
        }

        private void CopyReferencesFromParent(Cloud.Model.Models.Device newDevice)
        {
            if (!newDevice.ParentId.HasValue)
            {
                return;
            }
            var parent = Connection.CloudModel.DevicesLazy.FirstOrDefault(f => f.Id == newDevice.ParentId.Value);
            if (parent == null)
            {
                return;
            }

            newDevice.FarmId = parent.FarmId;
            newDevice.HouseId = parent.HouseId;
        }

        /// <summary>
        /// Check if foreign keys referencing on valid properties.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private bool IsValidRelationshipsProperties(ref Cloud.Model.Models.Device device, out string errMessage)
        {
            errMessage = Properties.Resources.PreconditionError;
            if (device == null)
            {
                return false;
            }

            //Device doesn't have to reference on farm or house immediately after it is created,
            //but if contains ID, check it
            if (GuidIsValid(device.FarmId))
            {
                var farmId = device.FarmId.Value;
                device.Farm = Connection.CloudModel.Farms.FirstOrDefault(f => f.Id == farmId);
                if (device.Farm == null)
                {
                    errMessage = "Referenced farm is not found";
                    return false;
                }
            }

            if (GuidIsValid(device.HouseId))
            {
                var houseId = device.HouseId.Value;
                device.House = Connection.CloudModel.Houses.FirstOrDefault(f => f.Id == houseId);
                if (device.House == null)
                {
                    errMessage = "Referenced house is not found";
                    return false;
                }
                else
                {
                    // Remove reference to farm if device is assigned to house
                    device.Farm = null;
                    device.FarmId = null;
                }
            }
            return true;
        }

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{deviceId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute][Required]Guid deviceId, [FromBody]DTOs.Device body)
        {
            var updateModel = body.MapFrom(deviceId);
            var existedModel = await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id == deviceId);
            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Device)));
            }
            if (!IsValidRelationshipsProperties(ref updateModel, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }
            if (!CanUpdate(existedModel, updateModel, out var updateErrorMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, updateErrorMessage);
            }
            if (body.Type == DTOs.Enums.DeviceType.Bat2GsmScale && !IsPhoneNumberAvailable(body.PhoneNumber, existedModel.Id))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"GSM scale with phone number {body.PhoneNumber} already exist!");
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                var phoneNumber = existedModel.PhoneNumber;
                var type = existedModel.Type;
                var unit = existedModel.Unit;

                CopyReferencesToChildren(updateModel);
                existedModel.Update(updateModel);
                Connection.CloudModel.Devices.Update(existedModel);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroupsForDevice(existedModel) && UpdateDeviceMap(existedModel, phoneNumber, type, unit))
                {
                    transaction.Commit();
                    var parent = await GetParentAsync(existedModel);
                    return UpdateResponse(existedModel.MapTo(null, parent));
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(DTOs.Device)));
            }
        }

        /// <summary>
        /// Synchronize device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("sync/{deviceId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> SyncDeviceAsync([FromRoute][Required]Guid deviceId, [FromBody]DTOs.Device body)
        {
            if (deviceId == Guid.Empty)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Device id cannot be empty!");
            }
            if (body?.Name == null || body.ParentId == null || body.ParentId == Guid.Empty)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Name and ParentId are required!");
            }

            var device = await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id == deviceId);
            if (device != null)
            {
                device.Name = body.Name;
                await Connection.CloudModel.SaveChangesAsync();
                return UpdateResponse(device);
            }

            return await PostAsync(new DeviceCreate
            {
                Id = deviceId,
                ParentId = body.ParentId,
                Type = DTOs.Enums.DeviceType.Bat2CableScale,
                Name = body.Name
            });
        }

        private bool CanUpdate(Cloud.Model.Models.Device existedModel, Cloud.Model.Models.Device updateModel, out string updateErrorMessage)
        {
            updateErrorMessage = Properties.Resources.PreconditionError;
            if (!existedModel.ParentId.HasValue)    //check only devices with parent
            {
                return true;
            }

            if (existedModel.Type == Cloud.Model.Enums.DeviceType.Bat2CableScale &&
               (!updateModel.ParentId.HasValue || existedModel.ParentId.Value != updateModel.ParentId.Value))
            {
                updateErrorMessage = "Cannot remove or change parent device for BAT2 cable scale";
                return false;
            }
            var parent = Connection.CloudModel.DevicesLazy.FirstOrDefault(f => f.Id == existedModel.ParentId.Value);
            if (parent == null)
            {
                throw new InvalidOperationException("Device has reference on not existing parent!");
            }
            return CheckDeviceReferenceswithParent(updateModel, parent, out updateErrorMessage);
        }

        private bool CheckDeviceReferenceswithParent(Cloud.Model.Models.Device updateModel, Cloud.Model.Models.Device parent, out string updateErrorMessage)
        {
            if (parent.HouseId.HasValue && parent.HouseId != updateModel.HouseId)
            {
                updateErrorMessage = "Cannot change reference on house, because it must be the same as for terminal";
                return false;
            }

            if (!updateModel.FarmId.HasValue && !updateModel.HouseId.HasValue &&
                (parent.HouseId.HasValue || parent.FarmId.HasValue))
            {
                updateErrorMessage = "Cannot remove references from this device, must be the same as for terminal";
                return false;
            }

            if (parent.FarmId.HasValue)
            {
                if (updateModel.FarmId.HasValue && updateModel.FarmId.Value != parent.FarmId.Value)
                {
                    updateErrorMessage = "Cannot change reference on a farm, because it must be the same as for terminal. You can change only house which belongs this farm.";
                    return false;
                }
                if (updateModel.HouseId.HasValue && Connection.CloudModel.Houses.FirstOrDefault(f => f.Id == updateModel.HouseId.Value && f.FarmId == parent.FarmId.Value) == null)
                {
                    updateErrorMessage = "Cannot change reference on this house, because this house is not a part of the farm, which has assigned to terminal";
                    return false;
                }
            }

            updateErrorMessage = string.Empty;
            return true;
        }


        private void CopyReferencesToChildren(Cloud.Model.Models.Device updateModel)
        {
            if (updateModel.Type != Cloud.Model.Enums.DeviceType.BatCollectorTerminal &&
                updateModel.Type != Cloud.Model.Enums.DeviceType.BatGateway)
            {
                return;
            }

            var children = Connection.CloudModel.Devices.Where(w => w.ParentId == updateModel.Id);
            foreach (var child in children)
            {
                child.FarmId = updateModel.FarmId;
                child.HouseId = updateModel.HouseId;
            }
            Connection.CloudModel.Devices.UpdateRange(children);
        }


        /// <summary>
        /// Delete Device
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{deviceId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute][Required]Guid deviceId)
        {
            var device = await Connection.CloudModel.DevicesLazy.FirstOrDefaultAsync(f => f.Id.Equals(deviceId));
            if (device == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Device)));
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                RemoveDeviceReferencesOnTerminal(device);
                RemoveFlockReferencesOnDevice(device.Id);
                Connection.CloudModel.Devices.Remove(device);
                await Connection.CloudModel.SaveChangesAsync();

                if (RemoveDeviceStatisticGroups(device) && UpdateDeviceMap(device, true))
                {
                    transaction.Commit();
                    return ApiResponse();
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(DTOs.Device)));
            }
        }

        private bool RemoveDeviceStatisticGroups(Cloud.Model.Models.Device device)
        {
            if (device.Type == Cloud.Model.Enums.DeviceType.BatCollectorTerminal)
            {
                return true;
            }

            var dictionary = new Dictionary<Guid, IEnumerable<FlockEvent>>
            {
                { device.Id, new List<FlockEvent>() }
            };
            return Connection.GroupProducer.UpdateDeviceGroups(dictionary);
        }

        private void RemoveDeviceReferencesOnTerminal(Cloud.Model.Models.Device terminal)
        {
            if (terminal.Type != Cloud.Model.Enums.DeviceType.BatCollectorTerminal)
            {
                return;
            }

            var sensors = Connection.CloudModel.Devices.Where(w => w.ParentId.HasValue && w.ParentId.Value == terminal.Id);
            foreach (var sensor in sensors)
            {
                sensor.Parent = null;
                sensor.ParentId = null;
            }
            Connection.CloudModel.Devices.UpdateRange(sensors);
        }

        private bool UpdateDeviceMap(Cloud.Model.Models.Device device, string originalPhoneNumber, Cloud.Model.Enums.DeviceType originalType, Cloud.Model.Enums.DeviceUnit originalUnit)
        {
            if (device.Type != Cloud.Model.Enums.DeviceType.Bat2GsmScale && originalType != Cloud.Model.Enums.DeviceType.Bat2GsmScale)
            {
                return true;
            }
            if (device.Type == Cloud.Model.Enums.DeviceType.Bat2GsmScale && originalType != Cloud.Model.Enums.DeviceType.Bat2GsmScale)
            {
                return UpdateDeviceMap(device);
            }
            if (device.Type != Cloud.Model.Enums.DeviceType.Bat2GsmScale)
            {
                return UpdateDeviceMap(originalPhoneNumber, null, null);
            }

            if (device.PhoneNumber != originalPhoneNumber)
            {
                return UpdateDeviceMap(originalPhoneNumber, null, null) && UpdateDeviceMap(device);
            }
            return device.Unit == originalUnit || UpdateDeviceMap(device);
        }

        private bool UpdateDeviceMap(Cloud.Model.Models.Device device, bool remove = false)
        {
            return device.Type != Cloud.Model.Enums.DeviceType.Bat2GsmScale
                || UpdateDeviceMap(device.PhoneNumber, remove ? (Guid?)null : device.Id, device.Unit.ToString());
        }

        private bool UpdateDeviceMap(string phoneNumber, Guid? deviceId, string unit)
        {
            return Connection.KafkaSender.Send(new List<DeviceMap>
            {
                new DeviceMap { Id = deviceId, Uid = phoneNumber, Unit = unit }
            });
        }
    }
}
