﻿using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bat.Api.Controllers
{
    /// <summary>
    /// More specific queries on Bird data
    /// </summary>
    public partial class BirdController : BaseApiController
    {
        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("company")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdCompaniesAsync()
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!GuidIsValid(companyId))
            {
                return ApiResponse(HttpStatusCode.BadRequest, Properties.Resources.NoAssignedCompany);
            }

            var companies = await Connection.CloudModel.BirdCompanies.Select(s => s.Name).ToListAsync();
            return ApiResponse(companies);
        }

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("template/company")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdTemplateCompaniesAsync()
        {
            var companies = await birdModel.Companies.Select(s => s.Name).ToListAsync();
            return ApiResponse(companies);
        }


        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <param name="name">Company name</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("company/{name}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdCompanyProductsAsync(string name)
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!GuidIsValid(companyId))
            {
                return ApiResponse(HttpStatusCode.BadRequest, Properties.Resources.NoAssignedCompany);
            }
            if (string.IsNullOrEmpty(name))
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Invalid bird company name");
            }

            return await GetBirdCompanyProductsRoutineAsync(name, false);
        }


        /// <summary>
        /// List All Products of Bird template company
        /// </summary>
        /// <param name="name">Company name</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("template/company/{name}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdTemplateCompanyProductsAsync(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Invalid bird company name");
            }

            return await GetBirdCompanyProductsRoutineAsync(name, true);
        }

        private async Task<ActionResult<ApiResponse>> GetBirdCompanyProductsRoutineAsync(string birdCompanyName, bool isTemplate = false)
        {
            Bird.Model.Models.Company company = null;
            company = isTemplate ? await birdModel.CompaniesLazy.FirstOrDefaultAsync(f => f.Name == birdCompanyName)
                                 : await Connection.CloudModel.BirdCompaniesLazy.FirstOrDefaultAsync(f => f.Name == birdCompanyName);


            if (company == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird.Company)));
            }
            var products = company.Products.Select(s => s.Name).ToList();
            return ApiResponse(products);

        }

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <param name="name">Product name</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("byProduct/{name}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdsByProductAsync(string name)
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!GuidIsValid(companyId))
            {
                return ApiResponse(HttpStatusCode.BadRequest, Properties.Resources.NoAssignedCompany);
            }
            if (string.IsNullOrEmpty(name))
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Invalid bird product name");
            }

            var product = await Connection.CloudModel.BirdProductsLazy.FirstOrDefaultAsync(f => f.Name == name);
            if (product == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird.Product)));
            }

            var birds = product.Birds.ToList();
            return ApiResponse(birds.MapTo(null, false));
        }

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <param name="name">Product name</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("template/byProduct/{name}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetBirdTemplatesByProductAsync(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Invalid bird product name");
            }

            var product = await birdModel.ProductsLazy.FirstOrDefaultAsync(f => f.Name == name);
            if (product == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird.Product)));
            }

            var birds = product.Birds.ToList();
            return ApiResponse(birds.MapTo(null, true));
        }
    }
}
