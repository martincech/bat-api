﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Statistic;
using Statistic.Models;
using Veit.Kafka.Topics;

namespace Bat.Api.Controllers
{
    [Authorize(Roles = "VeitOperator")]
    public class ManageController : BaseApiController
    {

        private readonly IContainerProvider containerProvider;

        public ManageController(IConnectionFactory connectionFactory, IContainerProvider containerProvider)
            : base(connectionFactory)
        {
            this.containerProvider = containerProvider ?? throw new ArgumentNullException(nameof(containerProvider));
        }

        /// <summary>
        /// Send updated device groups to kafka
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("flock-groups")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> UpdateGroupsAsync()
        {
            var companies = await Connection.UserModel.Companies.ToListAsync();
            var results = new Dictionary<Guid, (bool Success, int DevicesUpdated)>();
            foreach (var company in companies)
            {
                try
                {
                    var groups = new Dictionary<Guid, IEnumerable<FlockEvent>>();
                    using (var ctx = containerProvider.GetCompanyContainer(company.Id))
                    {
                        var flocks = ctx.Flocks.Where(w => !(w.EndDate.HasValue && w.EndDate < DateTime.Now)).ToList();
                        var devices = ctx
                            .Devices
                            .Where(w => w.Type == Cloud.Model.Enums.DeviceType.Bat2CableScale || w.Type == Cloud.Model.Enums.DeviceType.Bat2GsmScale)
                            .ToList();

                        foreach (var device in devices)
                        {
                            var af = flocks
                                .Where(w => w.DeviceId == device.Id || (w.HouseId.HasValue && w.HouseId == device.HouseId))
                                .Select(MapFlockEvent);
                            groups.Add(device.Id, af);
                        }
                    }
                    var producer = new GroupProducer(Connection.KafkaSender, company.Id.ToString());
                    var result = producer.UpdateDeviceGroups(groups);
                    results.Add(company.Id, (result, groups.Count));
                }
                catch (Exception e)
                {
                    Logger.Warn($"Failed to regenerate groups for company {company.Id}. ErrorMessage : {e.Message}");
                    results.Add(company.Id, (false, -1));
                }
            }
            return ApiResponse(results.Select(s => new { CompanyId = s.Key, s.Value.Success, s.Value.DevicesUpdated }));
        }

        /// <summary>
        /// Updated device id mapping to kafka
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("device-map")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> UpdateDeviceMapAsync()
        {
            var companies = await Connection.UserModel.Companies.ToListAsync();
            var results = new Dictionary<string, (Guid companyId, Guid deviceId, Cloud.Model.Enums.DeviceUnit unit)>();
            foreach (var company in companies)
            {
                var devices = GetDevicePhoneNumbersByCompanyId(company.Id);
                foreach (var device in devices)
                {
                    var phoneNumber = MapDevice.StandardizePhoneNumber(device.PhoneNumber);
                    if (!string.IsNullOrEmpty(phoneNumber))
                    {
                        if (results.ContainsKey(phoneNumber))
                        {
                            var (companyId, deviceId, unit) = results[phoneNumber];
                            Logger.Error($"Exist multiple devices with same phone number : {phoneNumber}. \n CompanyID: {companyId} \n CompanyID: {company.Id}");
                            continue;
                        }
                        results.Add(phoneNumber, (company.Id, device.Id, device.Unit));
                    }
                }
            }

            if (!Connection.KafkaSender.Send(results.Select(s => new DeviceMap { Id = s.Value.deviceId, Uid = s.Key, Unit = s.Value.unit.ToString() }).ToList()))
            {
                return ApiResponse(HttpStatusCode.InternalServerError, "Failed to update kafka device mapping.");
            }

            return ApiResponse(results);
        }

        private IList<Cloud.Model.Models.Device> GetDevicePhoneNumbersByCompanyId(Guid companyId)
        {
            try
            {
                using (var ctx = containerProvider.GetCompanyContainer(companyId))
                {
                    return ctx
                        .Devices
                        .Where(w => w.Type == Cloud.Model.Enums.DeviceType.Bat2GsmScale).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Failed to get list of phone numbers for company : {companyId}");
            }
            return new List<Cloud.Model.Models.Device>();
        }
    }
}
