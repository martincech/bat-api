﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Veit.Cognito.User;

namespace Bat.Api.Controllers
{
    [CommonValidation("userId")]
    public class UserController : BaseApiController
    {
        private const string GetById = "GetUserById";
        private readonly IUserContext userContext;
        private const string DefaultCompanyName = "NewCompany";
        private readonly IContainerProvider containerProvider;

        public UserController(IConnectionFactory connectionFactory, IUserContext userContext, IContainerProvider containerProvider)
            : base(connectionFactory)
        {
            this.userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
            this.containerProvider = containerProvider ?? throw new ArgumentNullException(nameof(containerProvider));
        }

        /// <summary>
        /// List All Users
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync()
        {
            var users = GetVisibleUsers();
            var mappedUsers = await LoadUserDataAsync(users.ToList());
            return ApiResponse(mappedUsers);
        }

        private async Task<IEnumerable<DTOs.User>> LoadUserDataAsync(List<User.Model.Models.User> users)
        {
            var cognitoUsers = await userContext.GetCognitoUserAsync(users.Select(s => s.Id));
            if (cognitoUsers == null)
            {
                Logger.Error("Cognito returns null data. Check AWS settings in appsettings!");
                throw new InvalidOperationException("Cognito returns null data!");
            }

            var mappedUsers = users.MapTo();

            foreach (var user in mappedUsers) //add additional info
            {
                var cognitoUser = cognitoUsers.FirstOrDefault(f => f.Id == user.Id);
                user.MapTo(cognitoUser);
            }
            return mappedUsers;
        }


        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{userId}", Name = GetById)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid userId)
        {
            var users = GetVisibleUsers();
            var user = users.FirstOrDefault(f => f.Id.Equals(userId));
            if (user == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }

            var cognitoUser = await userContext.GetCognitoUserAsync(userId);
            var mappedUser = user.MapTo();
            return ApiResponse(mappedUser.MapTo(cognitoUser));
        }


        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>&#39;You may create a user.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]UserCreate body)
        {
            if (body == null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, Properties.Resources.PreconditionError);
            }
            if (!CanCreateUser(body, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.Forbidden, errMessage);
            }

            if (!CanCreateCognitoUser(body?.Email, out var errorMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errorMessage);
            }

            var cognitoUser = body.MapFrom();
            var newUserId = await userContext.CreateUserAsync(cognitoUser);
            if (!newUserId.HasValue)
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.CreateUserFailed);
            }

            var newUser = body.MapFrom(Connection.UserModel.Roles, newUserId.Value);    //ID for DB gets from cognito
            await Connection.UserModel.Users.AddAsync(newUser);
            await Connection.UserModel.SaveChangesAsync();
            return CreateResponse(GetById, new { userId = newUser.Id }, newUser.MapTo().MapTo(cognitoUser));
        }

        private bool CanCreateCognitoUser(string userEmail, out string errorMessage)
        {
            errorMessage = Properties.Resources.PreconditionError;
            if (string.IsNullOrEmpty(userEmail))
            {
                errorMessage = "Email is not set";
                return false;
            }

            var task = userContext.ExistUserAsync(userEmail);
            task.Wait();
            if (task.Result)
            {
                errorMessage = "User with the same email already exist!";
                return false;
            }
            return true;
        }


        private bool CanCreateUser(UserCreate body, out string errorMessage)
        {
            errorMessage = Properties.Resources.LowPermissions;
            return IsRoleValid(body.Role, ref errorMessage);
        }

        private bool CanUpdateUser(DTOs.User body, User.Model.Models.User existed, out string errorMessage)
        {
            errorMessage = Properties.Resources.LowPermissions;
            var iAmOperator = Connection.IsInRole(DTOs.Enums.Role.VeitOperator);

            if (iAmOperator)
            {
                return true;
            }

            if (!IsRoleValid(existed.MapTo().Role, ref errorMessage) || !IsRoleValid(body.Role, ref errorMessage))
            {
                return false;
            }

            //I want to update yourself
            return Connection.CurrentUser != null &&
                   CanUpdateUserValues(body, existed, ref errorMessage);
        }

        /// <summary>
        /// Check properties which can't be changed if user is not operator (e.g. company)
        /// </summary>
        /// <param name="body"></param>
        /// <param name="existed"></param>
        /// <param name="errorMessage"></param>
        private static bool CanUpdateUserValues(DTOs.User body, User.Model.Models.User existed, ref string errorMessage)
        {
            var companyChanged = body.CompanyId == existed.CompanyId;
            if (!companyChanged)
            {
                errorMessage = $"{Properties.Resources.LowPermissions}. Unprivileged user can't changed company.";
            }
            return companyChanged;
        }


        private bool IsRoleValid(DTOs.Enums.Role? role, ref string errorMessage)
        {
            if (role == DTOs.Enums.Role.VeitOperator)
            {
                errorMessage = "Only unprivileged user can be created";
                return false;
            }

            if (role == DTOs.Enums.Role.CompanyAdmin
                && !Connection.IsInRole(DTOs.Enums.Role.VeitOperator) && !Connection.IsInRole(DTOs.Enums.Role.CompanyAdmin))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Update User details
        /// </summary>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{userId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync(Guid userId, [FromBody][Required]DTOs.User body)
        {
            var updateModel = body.MapFrom(Connection.UserModel.Roles);
            var errMessage = Properties.Resources.PreconditionError;
            if (updateModel == null || !CanUpdate(body, out errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            var existedModel = GetVisibleUsers().FirstOrDefault(f => f.Id == userId);
            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }
            if (!CanUpdateUser(body, existedModel, out var message))
            {
                return ApiResponse(HttpStatusCode.Forbidden, message);
            }

            var existedCognitoUser = await userContext.GetCognitoUserAsync(existedModel.Id);
            var updateCognitoUser = body.MapFrom();

            //not changeable properties
            updateCognitoUser.Id = userId;
            updateCognitoUser.Email = existedCognitoUser.Email;

            if (!await userContext.UpdateUserAsync(updateCognitoUser))
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.UpdateUserFailed);
            }

            existedModel.Update(updateModel);
            Connection.UserModel.Users.Update(existedModel);

            await Connection.UserModel.SaveChangesAsync();
            return UpdateResponse(existedModel.MapTo().MapTo(updateCognitoUser));
        }


        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{userId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute][Required]Guid userId)
        {
            var user = GetVisibleUsers().FirstOrDefault(f => f.Id.Equals(userId));
            if (user == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }
            // cannot delete user with role VeitOperator
            if (!CanDeleteUser(user))
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }

            if (!await userContext.DeleteUserAsync(userId))
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.DeleteUserFailed);
            }

            Connection.UserModel.Users.Remove(user);
            await Connection.UserModel.SaveChangesAsync();
            return ApiResponse();
        }

        private bool CanDeleteUser(User.Model.Models.User user)
        {
            if (IsUserInRole(user, DTOs.Enums.Role.VeitOperator))
            {
                return false;
            }

            if (IsUserInRole(user, DTOs.Enums.Role.CompanyAdmin))
            {
                return Connection.IsInRole(DTOs.Enums.Role.VeitOperator) || Connection.IsInRole(DTOs.Enums.Role.CompanyAdmin);
            }

            return true;
        }

        /// <summary>
        /// Check if selected user contains required role.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="role"></param>
        private static bool IsUserInRole(User.Model.Models.User user, DTOs.Enums.Role role)
        {
            var roleName = role.ToString();
            var requiredRole = user.Roles.FirstOrDefault(f => f.Name == roleName);
            return requiredRole != null;
        }


        /// <summary>
        /// Retrieve Current User Details
        /// </summary>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("current")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetCurrentAsync()
        {
            var user = Connection.CurrentUser;
            if (user == null)
            {
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }
            var cognitoUser = await userContext.GetCognitoUserAsync(user.Id);
            await CheckConfirmRegistrationAsync(user.CompanyId);
            return ApiResponse(user.MapTo(cognitoUser));
        }

        /// <summary>
        /// Check if selected company exists and it has created statistic context (influx DB).
        /// These databases will be created when user logs in for the first time.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        private async Task CheckConfirmRegistrationAsync(Guid? companyId)
        {
            if (!companyId.HasValue)
            {
                return;
            }

            await CheckCompanyStatisticContextAsync(companyId.Value);
            await CheckCompanyDatabaseAsync(companyId.Value);
        }

        private async Task CheckCompanyStatisticContextAsync(Guid companyId)
        {
            var contextName = companyId.ToString();
            if (await Connection.StatisticContext.ExistCompanyContextAsync(contextName))
            {
                return;
            }
            await Connection.StatisticContext.CreateCompanyContextAsync(contextName);
        }

        private async Task CheckCompanyDatabaseAsync(Guid companyId)
        {
            if (Connection.CloudModel.Database.IsInMemory() || //for unit tests
               (Connection.CloudModel.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
            {
                return;
            }
            await containerProvider.CreateCompanyDatabaseAsync(companyId);
        }


        /// <summary>
        /// Update Current User details
        /// </summary>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("current")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutCurrentAsync([FromBody][Required]DTOs.User body)
        {
            var existed = Connection.CurrentUser;
            if (existed == null)
            {
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }
            var existedCognitoUser = await userContext.GetCognitoUserAsync(existed.Id);
            existed = existed.MapTo(existedCognitoUser);

            var errMessage = Properties.Resources.PreconditionError;
            if (body == null || !CanUpdate(body, out errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            var updateCognitoUser = body.MapFrom();
            if (!await userContext.UpdateUserAsync(updateCognitoUser))
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.UpdateUserFailed);
            }

            return UpdateResponse(existed.MapTo(updateCognitoUser));
        }

        private static bool CanUpdate(DTOs.User update, out string errorMessage)
        {
            errorMessage = Properties.Resources.PreconditionError;

            //check only name, other properties cannot be changed
            if (string.IsNullOrEmpty(update.Name))
            {
                errorMessage = "User name cannot be empty!";
                return false;
            }

            return true;
        }


        /// <summary>
        /// Reset user password.
        /// </summary>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("resetAccount/{userId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> ResetAccountAsync(Guid userId)
        {
            var user = await Connection.UserModel.UsersLazy.FirstOrDefaultAsync(f => f.Id == userId);
            if (user == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.User)));
            }
            if (!CanResetPassword(user))
            {
                return ApiResponse(HttpStatusCode.Forbidden, Properties.Resources.LowPermissions);
            }
            if (!await userContext.ResetUserPasswordAsync(user.Id))
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.ResetAccountFailed);
            }
            return ApiResponse();
        }

        private bool CanResetPassword(User.Model.Models.User user)
        {
            if (Connection.IsInRole(DTOs.Enums.Role.VeitOperator))
            {
                return true;
            }

            if (IsUserInRole(user, DTOs.Enums.Role.CompanyAdmin) && !Connection.IsInRole(DTOs.Enums.Role.CompanyAdmin))
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>&#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost("registration")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostRegistrationAsync([FromBody]UserRegister body)
        {
            if (!CanCreateCognitoUser(body?.Email, out var errorMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errorMessage);
            }

            var cognitoUser = body.MapFrom();
            var newUserId = await userContext.CreateUserAsync(cognitoUser);
            if (!newUserId.HasValue)
            {
                return ApiResponse(HttpStatusCode.InternalServerError, Properties.Resources.CreateUserFailed);
            }

            var company = await CreateCompanyForUserAsync(body.Company);
            var newUser = await CreateUserAsync(newUserId.Value, company.Id);
            return CreateResponse(GetById, new { userId = newUser.Id }, newUser.MapTo().MapTo(cognitoUser));
        }


        private async Task<User.Model.Models.Company> CreateCompanyForUserAsync(string companyName)
        {
            var company = new User.Model.Models.Company
            {
                Name = string.IsNullOrEmpty(companyName) ? DefaultCompanyName : companyName
            };

            Connection.UserModel.Companies.Add(company);
            await Connection.UserModel.SaveChangesAsync();
            return company;
        }

        private async Task<User.Model.Models.User> CreateUserAsync(Guid userId, Guid companyId)
        {
            var newUser = new User.Model.Models.User
            {
                Id = userId,
                CompanyId = companyId
            };

            await Connection.UserModel.Users.AddAsync(newUser);
            await Connection.UserModel.SaveChangesAsync();
            return newUser;
        }

        private IEnumerable<User.Model.Models.User> GetVisibleUsers()
        {
            if (Connection.IsInRole(DTOs.Enums.Role.VeitOperator))
            {
                return Connection.UserModel.UsersLazy;
            }

            var currentUserCompanyId = Connection.CurrentUser?.CompanyId;
            var companyUsers = Connection.UserModel.UsersLazy.Where(w => w.CompanyId == currentUserCompanyId).ToList();
            // filter users by IsUserInRole must be executed on loaded data (Linq to SQL is not working correctly with this filter)
            return companyUsers.Where(w => !IsUserInRole(w, DTOs.Enums.Role.VeitOperator));
        }
    }
}
