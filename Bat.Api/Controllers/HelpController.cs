﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bat.Api.Controllers
{
    public class HelpController : BaseApiController
    {
        public HelpController(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        [HttpGet]
        public async Task<ActionResult<ApiResponse>> GetAsync()
        {
            var isAdmin = User.IsInRole(AdminRole);
            var pages = await Connection.UserModel.HelpPages.Where(w => isAdmin || !w.Disabled).ToListAsync();
            return ApiResponse(pages.MapToBase());
        }

        [HttpGet("{slug}")]
        public async Task<ActionResult<ApiResponse>> GetByAsync(string slug)
        {
            var isAdmin = User.IsInRole(AdminRole);
            var pages = await Connection.UserModel.HelpPages.Where(w => isAdmin || !w.Disabled).FirstOrDefaultAsync(s => s.Slug == slug);
            return ApiResponse(pages.MapTo());
        }

        [HttpPost, Authorize(Roles = AdminRole)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]HelpPageBase body)
        {
            if (string.IsNullOrWhiteSpace(body?.Slug))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, "Invalid request body.");
            }
            var page = await Connection.UserModel.HelpPages.FirstOrDefaultAsync(f => f.Slug == body.Slug);
            if (page != null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Help page with address '{body.Slug}' already exiset.");
            }

            var newPage = body.MapFrom();
            await Connection.UserModel.HelpPages.AddAsync(newPage);
            await Connection.UserModel.SaveChangesAsync();

            return ApiResponse(newPage);
        }

        [HttpPut("{id}"), Authorize(Roles = AdminRole)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute]Guid id, HelpPageBase body)
        {
            var page = await Connection.UserModel.HelpPages.FirstOrDefaultAsync(f => f.Id == id);
            if (page == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, $"Help page with id '{id}' was not found.");
            }

            page.Update(body.MapFrom());
            await Connection.UserModel.SaveChangesAsync();

            return ApiResponse(page.MapTo());
        }


        [HttpPut("content/{id}"), Authorize(Roles = AdminRole)]
        public async Task<ActionResult<ApiResponse>> PutContentAsync([FromRoute]Guid id, [FromBody]string body)
        {
            var page = await Connection.UserModel.HelpPages.FirstOrDefaultAsync(f => f.Id == id);
            if (page == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, $"Help page with id '{id}' was not found.");
            }

            page.Content = body;
            await Connection.UserModel.SaveChangesAsync();
            return ApiResponse(page.MapTo());
        }

        [HttpDelete("{id}"), Authorize(Roles = AdminRole)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute]Guid id)
        {
            var page = await Connection.UserModel.HelpPages.FirstOrDefaultAsync(f => f.Id == id);
            if (page == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, $"Help page with id '{id}' was not found.");
            }

            Connection.UserModel.Remove(page);
            await Connection.UserModel.SaveChangesAsync();

            return ApiResponse();
        }
    }
}
