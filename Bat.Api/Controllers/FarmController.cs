﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Farm = Bat.Api.DTOs.Farm;

namespace Bat.Api.Controllers
{
    [CommonValidation("farmId")]
    public class FarmController : BaseApiController
    {
        private const string GET_BY_ID = "GetFarmById";

        public FarmController(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromQuery]string expand = null)
        {
            var farms = await Connection.CloudModel.Farms.ToListAsync();
            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<Farm>(expand);
            var mappedFarms = farms.MapTo(expandDictionary);
            LoadFarmsExtendedStatistics(expandDictionary, mappedFarms);
            return ApiResponse(mappedFarms);
        }

        private void LoadFarmsExtendedStatistics(Dictionary<string, string> expandDictionary, IEnumerable<Farm> farms)
        {
            foreach (var farm in farms)
            {
                LoadFarmsExtendedStatistics(expandDictionary, farm);
            }
        }

        private void LoadFarmsExtendedStatistics(Dictionary<string, string> expandDictionary, Farm farm)
        {
            if (expandDictionary.ContainsKey(nameof(Farm.ActiveFlocksCount)))
            {
                farm.ActiveFlocksCount = GetActiveFlocksCount(farm);
            }
            if (expandDictionary.ContainsKey(nameof(Farm.ScheduledFlocksCount)))
            {
                farm.ScheduledFlocksCount = GetScheduledFlocksCount(farm);
            }
            if (expandDictionary.ContainsKey(nameof(Farm.HousesCount)))
            {
                farm.HousesCount = Connection.CloudModel.Houses.Count(h => h.FarmId == farm.Id);
            }
            if (expandDictionary.ContainsKey(nameof(Farm.DevicesCount)))
            {
                farm.DevicesCount = GetDevicesCount(farm);
            }
        }

        private int GetActiveFlocksCount(Farm farm)
        {
            return Connection.CloudModel.FlocksLazy.Count(w =>
                   ((w.House != null && w.House.FarmId == farm.Id) ||
                    (w.Device != null && (w.Device.FarmId == farm.Id || (w.Device.House != null && w.Device.House.FarmId == farm.Id))))
                           && IsFlockActive(w.StartDate, w.EndDate));
        }

        private int GetScheduledFlocksCount(Farm farm)
        {
            return Connection.CloudModel.FlocksLazy.Count(w =>
                    ((w.House != null && w.House.FarmId == farm.Id) ||
                     (w.Device != null && (w.Device.FarmId == farm.Id || (w.Device.House != null && w.Device.House.FarmId == farm.Id))))
                            && IsFlockScheduled(w.StartDate));
        }

        private int GetDevicesCount(Farm farm)
        {
            return Connection.CloudModel.DevicesLazy.Count(w =>
                    w.FarmId == farm.Id ||                              //direct reference
                    (w.House != null && w.House.FarmId == farm.Id) ||   //reference by house
                    (w.Parent != null && (w.Parent.FarmId == farm.Id || (w.Parent.House != null && w.Parent.House.FarmId == farm.Id)))    //reference by parent device
                    );
        }

        private static bool IsFlockActive(DateTimeOffset from, DateTimeOffset? till)
        {
            var current = DateTimeOffset.Now;
            return DateTimeOffset.Compare(from, current) <= 0 &&
                (!till.HasValue || DateTimeOffset.Compare(till.Value, current) > 0);
        }

        private static bool IsFlockScheduled(DateTimeOffset from)
        {
            return DateTimeOffset.Compare(from, DateTimeOffset.Now) > 0;
        }

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{farmId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync(Guid farmId, [FromQuery]string expand = null)
        {
            var farm = await Connection.CloudModel.Farms.FirstOrDefaultAsync(f => f.Id.Equals(farmId));
            if (farm == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Farm)));
            }
            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<Farm>(expand);
            var mappedFarm = farm.MapTo(expandDictionary);
            LoadFarmsExtendedStatistics(expandDictionary, mappedFarm);
            return ApiResponse(mappedFarm);
        }



        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>&#39;You may create a farm.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody][Required]FarmCreate body)
        {
            var newFarm = body.MapFrom();
            if (newFarm == null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, Properties.Resources.PreconditionError);
            }

            await Connection.CloudModel.Farms.AddAsync(newFarm);
            await Connection.CloudModel.SaveChangesAsync();
            return CreateResponse(GET_BY_ID, new { farmId = newFarm.Id }, newFarm.MapTo());
        }

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{farmId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync(Guid farmId, [FromBody][Required]Farm body)
        {
            var updateModel = body.MapFrom(farmId);
            if (updateModel == null)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, Properties.Resources.PreconditionError);
            }

            var existedModel = await Connection.CloudModel.Farms.FirstOrDefaultAsync(f => f.Id == farmId);
            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Farm)));
            }

            existedModel.Update(updateModel);
            Connection.CloudModel.Farms.Update(existedModel);
            await Connection.CloudModel.SaveChangesAsync();
            return UpdateResponse(existedModel.MapTo());
        }



        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{farmId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync(Guid farmId)
        {
            var farm = await Connection.CloudModel.Farms.FirstOrDefaultAsync(f => f.Id.Equals(farmId));
            if (farm == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(Farm)));
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                var farmDevices = GetFarmDevices(farmId);
                RemoveDeviceReferencesOnFarm(farmId);
                DeleteFarmHouses(farmId);

                Connection.CloudModel.Farms.Remove(farm);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroupsForDevices(farmDevices))
                {
                    transaction.Commit();
                    return ApiResponse();
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(Farm)));
            }
        }


        #region Delete helpers

        private void RemoveDeviceReferencesOnFarm(Guid farmId)
        {
            var farmDevices = Connection.CloudModel.DevicesLazy.Where(f => f.FarmId == farmId);
            if (!farmDevices.Any())
            {
                return;
            }

            foreach (var device in farmDevices)
            {
                device.Farm = null;
                device.FarmId = null;
            }
            Connection.CloudModel.Devices.UpdateRange(farmDevices);
        }


        private void DeleteFarmHouses(Guid farmId)
        {
            //if any house has reference on this farm, delete this house and check if any flock has reference on this house

            var houses = Connection.CloudModel.Houses.Where(w => w.FarmId == farmId);
            if (!houses.Any())
            {
                return;
            }

            //remove references on selected houses
            foreach (var house in houses)
            {
                RemoveDeviceReferencesOnHouse(house.Id);
                RemoveFlockReferencesOnHouse(house.Id);
            }

            Connection.CloudModel.Houses.RemoveRange(houses);
        }

        private IEnumerable<Cloud.Model.Models.Device> GetFarmDevices(Guid farmId)
        {
            var houseIds = Connection.CloudModel.Houses.Where(w => w.FarmId == farmId).Select(s => s.Id).ToList();
            return Connection.CloudModel.Devices.Where(w => w.HouseId.HasValue && houseIds.Contains(w.HouseId.Value)).ToList();
        }

        #endregion
    }
}
