﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.DTOs.Enums;
using Bat.Api.Map;
using Bird.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bat.Api.Controllers
{
    [CommonValidation("birdId")]
    public partial class BirdController : BaseApiController
    {
        private const string GET_BY_ID = "GetBirdById";
        private readonly BirdModelContainer birdModel;

        public BirdController(IConnectionFactory connectionFactory, BirdModelContainer birdModel)
            : base(connectionFactory)
        {
            this.birdModel = birdModel ?? throw new ArgumentOutOfRangeException(nameof(birdModel));
        }


        /// <summary>
        /// List All Birds
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromQuery]string expand = null)
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            if (!GuidIsValid(companyId))
            {
                return ApiResponse(HttpStatusCode.BadRequest, Properties.Resources.NoAssignedCompany);
            }

            var birds = await Connection.CloudModel.BirdsLazy.ToListAsync();
            var mappedBirds = birds.MapTo(GetMappedPropertiesWithDataMemberAttributes<DTOs.Bird>(expand), false);
            return ApiResponse(mappedBirds);
        }

        /// <summary>
        /// List All Templates of Birds
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("template")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetTemplateAsync([FromQuery]string expand = null)
        {
            var birds = await birdModel.BirdsLazy.ToListAsync();
            var mappedBirds = birds.MapTo(GetMappedPropertiesWithDataMemberAttributes<DTOs.Bird>(expand), true);
            return ApiResponse(mappedBirds);
        }


        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{birdId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid birdId, [FromQuery]string expand = null)
        {
            var companyId = Connection.CurrentUser?.CompanyId;
            var isTemplate = false;
            var bird = GuidIsValid(companyId)
                ? await Connection.CloudModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id == birdId)
                : null;

            if (bird == null)
            {
                isTemplate = true;
                bird = await birdModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id == birdId);
            }

            if (bird == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird)));
            }

            var mappedBird = bird.MapTo(GetMappedPropertiesWithDataMemberAttributes<DTOs.Bird>(expand), isTemplate);
            return ApiResponse(mappedBird);
        }


        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>&#39;You may create a bird.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden - You are not allowed to perform this action</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]BirdCreate body)
        {
            var newBird = body.MapFrom();
            if (!IsValidRelationshipsProperties(ref newBird, body?.Scope, out var statusCode, out var errMessage))
            {
                return ApiResponse(statusCode, errMessage);
            }

            var isTemplate = body.Scope.HasValue && body.Scope == Scope.Global;
            if (isTemplate)
            {
                await CreateBirdTemplateAsync(newBird, body.Product, body.Company);
            }
            else
            {
                await CreateUserBirdAsync(newBird, body.Product, body.Company);
            }


            var result = newBird.MapTo();
            if (isTemplate)
            {
                result.Scope = Scope.Global;
            }
            return CreateResponse(GET_BY_ID, new { birdId = newBird.Id }, result);
        }

        private async Task CreateBirdTemplateAsync(Bird.Model.Models.Bird newBird, string product, string company)
        {
            var birdProduct = GetOrCreateBirdTemplateProduct(product, company);
            newBird.Product = birdProduct;
            await birdModel.Birds.AddAsync(newBird);
            await birdModel.SaveChangesAsync();
        }

        private async Task CreateUserBirdAsync(Bird.Model.Models.Bird newBird, string product, string company)
        {
            var birdProduct = GetOrCreateUserBirdProduct(product, company);
            newBird.Product = birdProduct;
            await Connection.CloudModel.Birds.AddAsync(newBird);
            await Connection.CloudModel.SaveChangesAsync();
        }


        /// <summary>
        /// Check if foreign keys referencing on valid properties.
        /// </summary>
        /// <param name="bird"></param>
        /// <param name="errMessage"></param>
        /// <param name="statusCode"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        private bool IsValidRelationshipsProperties(ref Bird.Model.Models.Bird bird, Scope? scope, out HttpStatusCode statusCode, out string errMessage)
        {
            errMessage = Properties.Resources.PreconditionError;
            statusCode = HttpStatusCode.PreconditionFailed;
            if (bird == null)
            {
                return false;
            }

            if (scope != null && scope.Value == Scope.Global && !Connection.IsInRole(Role.VeitOperator))
            {
                statusCode = HttpStatusCode.Forbidden;
                errMessage = "You are not allowed to create/update bird template";
                return false;
            }
            return true;
        }


        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden - You are not allowed to perform this action</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{birdId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 403)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute][Required]Guid birdId, [FromBody]DTOs.Bird body)
        {
            var updateModel = body.MapFrom(birdId);
            if (!IsValidRelationshipsProperties(ref updateModel, body?.Scope, out var statusCode, out var errMessage))
            {
                return ApiResponse(statusCode, errMessage);
            }

            var isTemplate = body.Scope.HasValue && body.Scope == Scope.Global;
            var (updateResult, existedModel) = await UpdateBirdAsync(birdId, updateModel, body, isTemplate);
            if (!updateResult)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird)));
            }

            var result = existedModel.MapTo();
            if (isTemplate)
            {
                result.Scope = Scope.Global;
            }
            return UpdateResponse(result);
        }


        #region Bird update

        private async Task<(bool, Bird.Model.Models.Bird)> UpdateBirdAsync(Guid birdId, Bird.Model.Models.Bird updateModel, DTOs.Bird source, bool isTemplate)
        {
            var existedModel = await GetBirdAsync(birdId, isTemplate);
            if (existedModel == null)
            {
                return (false, null);
            }

            var originProductId = existedModel.Product.Id;
            var originCompanyId = existedModel.Product.Company.Id;
            updateModel.Product = UpdateProduct(existedModel, source, isTemplate);
            existedModel.Update(updateModel);

            await CommitUpdateAsync(isTemplate, existedModel);
            CleanUpAfterEditBird(originProductId, originCompanyId, isTemplate);
            return (true, existedModel);
        }

        private async Task<Bird.Model.Models.Bird> GetBirdAsync(Guid id, bool isTemplate)
        {
            if (isTemplate)
            {
                return await birdModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id == id);
            }
            return await Connection.CloudModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id == id);
        }

        private async Task CommitUpdateAsync(bool isTemplate, Bird.Model.Models.Bird existedModel)
        {
            if (isTemplate)
            {
                birdModel.Birds.Update(existedModel);
                await birdModel.SaveChangesAsync();
            }
            else
            {
                Connection.CloudModel.Birds.Update(existedModel);
                await Connection.CloudModel.SaveChangesAsync();
            }
        }

        private Bird.Model.Models.Product UpdateProduct(Bird.Model.Models.Bird existedModel, DTOs.Bird body, bool isTemplate)
        {
            if (existedModel.Product.Name != body.Product ||
                existedModel.Product.Company.Name != body.Company)
            {
                if (isTemplate)
                {
                    return GetOrCreateBirdTemplateProduct(body.Product, body.Company);
                }
                return GetOrCreateUserBirdProduct(body.Product, body.Company);
            }

            //product is without any change
            return existedModel.Product;
        }

        private void CleanUpAfterEditBird(Guid originProductId, Guid originCompanyId, bool isTemplate)
        {
            if (isTemplate)
            {
                CleanUpAfterEditBirdTemplate(originProductId, originCompanyId);
            }
            else
            {
                CleanUpAfterEditUserBird(originProductId, originCompanyId);
            }
        }

        private void CleanUpAfterEditBirdTemplate(Guid originProductId, Guid originCompanyId)
        {
            var product = birdModel.Products.FirstOrDefault(f => f.Id == originProductId);
            if (!product.Birds.Any())
            {
                birdModel.Products.Remove(product);
                birdModel.SaveChanges();
            }

            var company = birdModel.CompaniesLazy.FirstOrDefault(f => f.Id == originCompanyId);
            if (!company.Products.Any())
            {
                birdModel.Companies.Remove(company);
                birdModel.SaveChanges();
            }
        }

        private void CleanUpAfterEditUserBird(Guid originProductId, Guid originCompanyId)
        {
            var product = Connection.CloudModel.BirdProductsLazy.FirstOrDefault(f => f.Id == originProductId);
            if (!product.Birds.Any())
            {
                Connection.CloudModel.BirdProducts.Remove(product);
                Connection.CloudModel.SaveChanges();
            }

            var company = Connection.CloudModel.BirdCompaniesLazy.FirstOrDefault(f => f.Id == originCompanyId);
            if (!company.Products.Any())
            {
                Connection.CloudModel.BirdCompanies.Remove(company);
                Connection.CloudModel.SaveChanges();
            }
        }

        #endregion


        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{birdId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute][Required]Guid birdId)
        {
            var scope = Scope.Private;
            var bird = await Connection.CloudModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id.Equals(birdId));

            if (bird == null)
            {
                scope = Scope.Global;
                bird = await birdModel.BirdsLazy.FirstOrDefaultAsync(f => f.Id.Equals(birdId));
            }

            if (bird == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(DTOs.Bird)));
            }

            if (!IsValidRelationshipsProperties(ref bird, scope, out var statusCode, out var errMessage))
            {
                return ApiResponse(statusCode, errMessage);
            }

            if (scope == Scope.Private)
            {
                CleanUpUserBird(bird);
                RemoveFlockReferenceOnBird(birdId);
                Connection.CloudModel.Birds.Remove(bird);
                await Connection.CloudModel.SaveChangesAsync();
            }
            else
            {
                CleanUpBirdTemplate(bird);
                birdModel.Birds.Remove(bird);
                await birdModel.SaveChangesAsync();
            }
            return ApiResponse();
        }

        private void RemoveFlockReferenceOnBird(Guid birdId)
        {
            var flocks = Connection.CloudModel.FlocksLazy.Where(f => f.BirdId == birdId);
            if (!flocks.Any())
            {
                return;
            }

            foreach (var flock in flocks)
            {
                flock.BirdId = null;
            }
            Connection.CloudModel.Flocks.UpdateRange(flocks);
        }

        /// <summary>
        /// Deletes referenced objects if they are not used in other objects.
        /// </summary>
        /// <param name="bird"></param>
        private void CleanUpBirdTemplate(Bird.Model.Models.Bird bird)
        {
            var selectedBirdProducts = birdModel.Birds.Count(b => b.Product.Id == bird.Product.Id);
            if (selectedBirdProducts == 1)
            {
                birdModel.Products.Remove(bird.Product);
            }

            var selectedCompanyProducts = birdModel.Products.Count(p => p.Company.Id == bird.Product.Company.Id);
            if (selectedCompanyProducts == 1)
            {
                birdModel.Companies.Remove(bird.Product.Company);
            }
        }

        /// <summary>
        /// Deletes referenced objects if they are not used in other objects.
        /// </summary>
        /// <param name="bird"></param>
        private void CleanUpUserBird(Bird.Model.Models.Bird bird)
        {
            var selectedBirdProducts = Connection.CloudModel.Birds.Count(b => b.Product.Id == bird.Product.Id);
            if (selectedBirdProducts == 1)
            {
                Connection.CloudModel.BirdProducts.Remove(bird.Product);
            }

            var selectedCompanyProducts = Connection.CloudModel.BirdProducts.Count(p => p.Company.Id == bird.Product.Company.Id);
            if (selectedCompanyProducts == 1)
            {
                Connection.CloudModel.BirdCompanies.Remove(bird.Product.Company);
            }
        }

        #region Private helpers

        private Bird.Model.Models.Product GetOrCreateBirdTemplateProduct(string productName, string companyName)
        {
            var product = birdModel.ProductsLazy.FirstOrDefault(f => f.Name == productName && f.Company.Name == companyName);
            if (product == null)
            {
                var company = GetOrCreateBirdTemplateCompany(companyName);
                product = new Bird.Model.Models.Product
                {
                    Name = productName,
                    Company = company
                };
            }
            return product;
        }

        private Bird.Model.Models.Product GetOrCreateUserBirdProduct(string productName, string companyName)
        {
            var product = Connection.CloudModel.BirdProductsLazy.FirstOrDefault(f => f.Name == productName && f.Company.Name == companyName);
            if (product == null)
            {
                var company = GetOrCreateUserBirdCompany(companyName);
                product = new Bird.Model.Models.Product
                {
                    Name = productName,
                    Company = company
                };
            }
            return product;
        }

        private Bird.Model.Models.Company GetOrCreateBirdTemplateCompany(string name)
        {
            var company = birdModel.CompaniesLazy.FirstOrDefault(f => f.Name == name);
            if (company == null)
            {
                company = new Bird.Model.Models.Company
                {
                    Name = name
                };
                birdModel.Companies.Add(company);
            }
            return company;
        }

        private Bird.Model.Models.Company GetOrCreateUserBirdCompany(string name)
        {
            var company = Connection.CloudModel.BirdCompaniesLazy.FirstOrDefault(f => f.Name == name);
            if (company == null)
            {
                company = new Bird.Model.Models.Company
                {
                    Name = name
                };
                Connection.CloudModel.BirdCompanies.Add(company);
            }
            return company;
        }

        #endregion
    }
}
