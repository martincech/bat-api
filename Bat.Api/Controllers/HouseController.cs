﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bat.Api.Controllers
{
    [CommonValidation("houseId")]
    public class HouseController : BaseApiController
    {
        private const string GET_BY_ID = "GetHouseById";

        public HouseController(IConnectionFactory connectionFactory)
            : base (connectionFactory)
        {
        }


        /// <summary>
        /// List All Houses
        /// </summary>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromQuery]string expand = null)
        {
            var houses = await Connection.CloudModel.HousesLazy.ToListAsync();
            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<House>(expand);
            var mappedHouses = houses.MapTo(expandDictionary);
            if (expandDictionary.ContainsKey(nameof(House.ScalesCount)))
            {
                LoadHouseExtendedProperties(mappedHouses);
            }
            return ApiResponse(mappedHouses);
        }

        private void LoadHouseExtendedProperties(IEnumerable<House> houses)
        {
            foreach (var house in houses)
            {
                LoadHouseExtendedProperties(house);
            }
        }

        private void LoadHouseExtendedProperties(House house)
        {
            var count = Connection.CloudModel.DevicesLazy.Count(w =>
                (w.HouseId == house.Id && IsScale(w.Type)) ||                                                           //directly referenced
                (IsScale(w.Type) && w.Parent != null && IsTerminal(w.Parent.Type) && w.Parent.HouseId == house.Id));    //referenced by parent (terminal device)

            house.ScalesCount = count;
        }

        private static bool IsScale(Cloud.Model.Enums.DeviceType type)
        {
            switch(type)
            {
                case Cloud.Model.Enums.DeviceType.Bat2CableScale:
                case Cloud.Model.Enums.DeviceType.Bat2GsmScale:
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsTerminal(Cloud.Model.Enums.DeviceType type)
        {
            switch (type)
            {
                case Cloud.Model.Enums.DeviceType.BatCollectorTerminal:
                case Cloud.Model.Enums.DeviceType.BatGateway:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">list of expanded properties</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpGet("{houseId}", Name = GET_BY_ID)]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> GetAsync([FromRoute][Required]Guid houseId, [FromQuery]string expand = null)
        {
            var house = await Connection.CloudModel.HousesLazy.FirstOrDefaultAsync(f => f.Id.Equals(houseId));
            if (house == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(House)));
            }

            var expandDictionary = GetMappedPropertiesWithDataMemberAttributes<House>(expand);
            var mappedHouse = house.MapTo(expandDictionary);
            if (expandDictionary.ContainsKey(nameof(House.ScalesCount)))
            {
                LoadHouseExtendedProperties(mappedHouse);
            }
            return ApiResponse(mappedHouse);
        }


        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>&#39;You may create a house.&#39;</remarks>
        /// <param name="body"></param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 201)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync([FromBody]HouseCreate body)
        {
            var newHouse = body.MapFrom();
            if (!IsValidRelationshipsProperties(ref newHouse, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            await Connection.CloudModel.Houses.AddAsync(newHouse);
            await Connection.CloudModel.SaveChangesAsync();
            return CreateResponse(GET_BY_ID, new { houseId = newHouse.Id }, newHouse.MapTo());
        }


        /// <summary>
        /// Check if foreign keys referencing on valid properties.
        /// </summary>
        /// <param name="house"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private bool IsValidRelationshipsProperties(ref Cloud.Model.Models.House house, out string errMessage)
        {
            errMessage = Properties.Resources.PreconditionError;
            if (house == null)
            {
                return false;
            }

            var farmId = house.FarmId;
            if (farmId == Guid.Empty)
            {
                errMessage = "House must have reference on a farm";
                return false;
            }
            house.Farm = Connection.CloudModel.Farms.FirstOrDefault(f => f.Id == farmId);
            if (house.Farm == null)
            {
                errMessage = "Referenced farm is not found";
                return false;
            }
            return true;
        }


        /// <summary>
        /// Update House details
        /// </summary>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPut("{houseId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        [ProducesResponseType(typeof(ApiResponse), 412)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PutAsync([FromRoute][Required]Guid houseId, [FromBody]House body)
        {
            var updateModel = body.MapFrom(houseId);
            if (!IsValidRelationshipsProperties(ref updateModel, out var errMessage))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, errMessage);
            }

            var existedModel = await Connection.CloudModel.HousesLazy.FirstOrDefaultAsync(f => f.Id == houseId);
            if (existedModel == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(House)));
            }

            existedModel.Update(updateModel);
            Connection.CloudModel.Houses.Update(existedModel);
            await Connection.CloudModel.SaveChangesAsync();
            return UpdateResponse(existedModel.MapTo());
        }


        /// <summary>
        /// Delete House
        /// </summary>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">The specified resource was not found</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpDelete("{houseId}")]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 404)]
        public async Task<ActionResult<ApiResponse>> DeleteAsync([FromRoute][Required]Guid houseId)
        {
            var house = await Connection.CloudModel.HousesLazy.FirstOrDefaultAsync(f => f.Id.Equals(houseId));
            if (house == null)
            {
                return ApiResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.CurrentCulture, Properties.Resources.NotFound, nameof(House)));
            }

            using (var transaction = Connection.CloudModel.Database.BeginTransaction())
            {
                var houseDevices = Connection.CloudModel.DevicesLazy.Where(f => f.HouseId == houseId).ToList();

                RemoveDeviceReferencesOnHouse(house.Id);
                RemoveFlockReferencesOnHouse(house.Id);

                Connection.CloudModel.Houses.Remove(house);
                await Connection.CloudModel.SaveChangesAsync();

                if (UpdateStatisticGroupsForDevices(houseDevices))
                {
                    transaction.Commit();
                    return ApiResponse();
                }
                transaction.Rollback();
                return ApiResponse(HttpStatusCode.InternalServerError, string.Format(CultureInfo.CurrentCulture, Properties.Resources.UpdateGroupsFailed, nameof(House)));
            }
        }
    }
}
