﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Attributes;
using Bat.Api.Connection;
using Bat.Api.Controllers.Helpers;
using Bat.Api.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Veit.Diagnostics.AWS;

namespace Bat.Api.Controllers
{
    [CommonValidation]
    public class FeedbackController : BaseApiController
    {
        private const long MAXIMUM_FILE_SIZE_IN_BYTES = 1024 * 1024 * 20;
        private const char DIAGNOSTIC_DATA_SEPARATOR = '_';
        private const string VALID_CONTENT = "zip";


        public FeedbackController(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }


        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <param name="file">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <remarks>&#39;You may send a feedback.&#39;</remarks>
        /// <response code="200">Successful Response</response>
        /// <response code="400">Invalid ID Supplied</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse), 200)]
        [ProducesResponseType(typeof(ApiResponse), 400)]
        [ProducesResponseType(typeof(ApiResponse), 401)]
        [ProducesResponseType(typeof(ApiResponse), 0)]
        public async Task<ActionResult<ApiResponse>> PostAsync(IFormFile file)
        {
            if (file == null)
            {
                return ApiResponse(HttpStatusCode.BadRequest, "Invalid file supplied");
            }
            if (file.Length > MAXIMUM_FILE_SIZE_IN_BYTES)
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Too large file, maximum is: {MAXIMUM_FILE_SIZE_IN_BYTES}, actual: {file.Length}");
            }
            if (!IsContentTypeSupported(file.ContentType))
            {
                return ApiResponse(HttpStatusCode.PreconditionFailed, $"Unsupported content type: {file.ContentType}. Valid is only zip archive");
            }

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                await S3.PutAsync(stream, file.FileName, GetDiagnosticInfo());
                return ApiResponse();
            }
        }


        private static bool IsContentTypeSupported(string contentType)
        {
            return contentType.Contains(VALID_CONTENT, StringComparison.InvariantCultureIgnoreCase);
        }

        private string GetDiagnosticInfo()
        {
            var user = Connection.CurrentUser;
            if (user == null)
            {
                throw new FieldAccessException("No user is logged!");
            }

            var properties = new List<string>
            {
                user.CompanyId.ToString(),
                user.Company?.Name,
                user.Name
            };
            return string.Join(DIAGNOSTIC_DATA_SEPARATOR, properties);
        }
    }
}
