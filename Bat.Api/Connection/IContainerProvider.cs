﻿using System;
using System.Threading.Tasks;
using Bat.Cloud.Model;

namespace Bat.Api.Connection
{
    public interface IContainerProvider
    {
        BatCloudModelContainer GetCompanyContainer(Guid companyId);

        Task CreateCompanyDatabaseAsync(Guid companyId);
        Task DeleteCompanyDatabaseAsync(Guid companyId);
    }
}
