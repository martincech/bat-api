﻿using System;
using System.Linq;
using System.Security.Claims;
using Bat.Api.DTOs.Enums;
using Bat.Api.Map;
using Bat.Cloud.Model;
using NLog;
using Statistic;
using User.Model;
using Veit.Kafka;

namespace Bat.Api.Connection
{
    public sealed class ConnectionFactory : IConnectionFactory, IDisposable
    {
        private readonly ClaimsPrincipal claimsPrincipal;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        public ConnectionFactory(BatCloudModelContainer cloudModel,
                                 UserModelContainer userModel,
                                 ClaimsPrincipal claimsPrincipal,
                                 IStatisticContext statisticContext,
                                 IKafkaSender kafkaSender)
        {
            CloudModel = cloudModel ?? throw new ArgumentNullException(nameof(cloudModel));
            UserModel = userModel ?? throw new ArgumentNullException(nameof(userModel));
            this.claimsPrincipal = claimsPrincipal ?? throw new ArgumentNullException(nameof(claimsPrincipal));
            StatisticContext = statisticContext ?? throw new ArgumentNullException(nameof(statisticContext));
            KafkaSender = kafkaSender ?? throw new ArgumentNullException(nameof(kafkaSender));
        }


        public BatCloudModelContainer CloudModel { get; private set; }
        public UserModelContainer UserModel { get; private set; }
        public IStatisticContext StatisticContext { get; private set; }
        public IKafkaSender KafkaSender { get; private set; }

        public IGroupProducer GroupProducer
        {
            get
            {
                var company = CurrentUser?.CompanyId.ToString() ?? string.Empty;
                return new GroupProducer(KafkaSender, company);
            }
        }

        public DTOs.User CurrentUser
        {
            get
            {
                try
                {
                    var userClaim = claimsPrincipal.Claims.FirstOrDefault(f => f.Type == ClaimTypes.NameIdentifier);
                    var userId = new Guid(userClaim.Value);
                    var user = UserModel.UsersLazy.FirstOrDefault(f => f.Id == userId);
                    return user.MapTo();
                }
                catch (Exception e)
                {
                    logger.Warn(e, "Get current user exception");
                    return null;
                }
            }
        }

        public bool IsInRole(Role role) => claimsPrincipal.IsInRole(role.ToString());


        public void Dispose()
        {
            CloudModel.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
