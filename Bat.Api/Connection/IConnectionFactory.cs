﻿using Bat.Api.DTOs.Enums;
using Bat.Cloud.Model;
using Bird.Model;
using Statistic;
using User.Model;
using Veit.Cognito.User;
using Veit.Kafka;

namespace Bat.Api.Connection
{
    public interface IConnectionFactory
    {
        BatCloudModelContainer CloudModel { get; }
        UserModelContainer UserModel { get; }
        IStatisticContext StatisticContext { get; }
        IGroupProducer GroupProducer { get; }
        IKafkaSender KafkaSender { get; }

        bool IsInRole(Role role);
        DTOs.User CurrentUser { get; }
    }
}
