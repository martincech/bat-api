﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Bat.Api.Extensions;
using Bat.Cloud.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Bat.Api.Connection
{
    public class ContainerProvider : IContainerProvider
    {
        private readonly IConfiguration configuration;

        public ContainerProvider(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }


        /// <summary>
        /// Create model container for Bat Cloud.
        /// Don't forget to dispose container
        /// after finished all operations.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public BatCloudModelContainer GetCompanyContainer(Guid companyId)
        {
            var connectionString = configuration.GetCloudModelConnectionString();
            return new BatCloudModelContainer(string.Format(CultureInfo.CurrentCulture, connectionString, companyId));
        }

        public async Task CreateCompanyDatabaseAsync(Guid companyId)
        {
            using (var newCompany = GetCompanyContainer(companyId))
            {
                await newCompany.Database.MigrateAsync();
            }
        }

        public async Task DeleteCompanyDatabaseAsync(Guid companyId)
        {
            using (var company = GetCompanyContainer(companyId))
            {
                await company.Database.EnsureDeletedAsync();
                await company.SaveChangesAsync();
            }
        }
    }
}
