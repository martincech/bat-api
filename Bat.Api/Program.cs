﻿using System;
using NLog.Web;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NLog;
using System.IO;
using NLog.LayoutRenderers;

namespace Bat.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var configuringFileName = "nlog.config";
            var aspnetEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (string.IsNullOrEmpty(aspnetEnvironment))
            {
                aspnetEnvironment = "Production";
            }

            var environmentSpecificLogFileName = $"nlog.{aspnetEnvironment}.config";
            if (File.Exists(environmentSpecificLogFileName))
            {
                configuringFileName = environmentSpecificLogFileName;
            }
            var logger = NLogBuilder.ConfigureNLog(configuringFileName).GetCurrentClassLogger();
            LayoutRenderer.Register<DemystifiedExceptionLayoutRenderer>("exception");
            try
            {
                logger.Debug($"Starting application with ASPNETCORE_ENVIRONMENT: {aspnetEnvironment}");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                LogManager.Shutdown();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
