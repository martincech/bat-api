﻿using System.Linq;
using Bat.Api.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;

namespace Bat.Api.Attributes
{
    #region ValidationRecord

    public sealed class ValidationRecord
    {
        private const int DEFAULT_ERROR_STATUS_CODE = 412;

        public ValidationRecord()
        {
            StatusCode = DEFAULT_ERROR_STATUS_CODE;
        }

        public static int DefaultErrorStatusCode => DEFAULT_ERROR_STATUS_CODE;

        public int StatusCode { get; set; }
        public string ParameterName { get; set; }
        public string ErrorMessage { get; set; }
    }

    #endregion

    public abstract class ValidateModelStateAttribute : ResultFilterAttribute
    {
        private readonly ValidationRecord[] validations;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected ValidateModelStateAttribute(ValidationRecord[] validations)
        {
            this.validations = validations;
        }


        public static ValidationRecord PredefinedValidationPrecondition()
        {
            return new ValidationRecord
            {
                StatusCode = ValidationRecord.DefaultErrorStatusCode,
                ParameterName = "body",
                ErrorMessage = "Precondition Failed, Required Data Constraints Not Met"
            };
        }

        public static ValidationRecord PredefinedValidationId(string idName)
        {
            return new ValidationRecord
            {
                StatusCode = 400,
                ParameterName = idName,
                ErrorMessage = "Invalid ID Supplied"
            };
        }


        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var invalidParameters = context.ModelState.Where(w => w.Value.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Invalid).Select(s => s.Key);
                var intersectOfErrors = validations.Select(s => s.ParameterName).Intersect(invalidParameters);

                var code = ValidationRecord.DefaultErrorStatusCode;
                var name = "";
                var message = "";

                if (intersectOfErrors.Any())
                {   //custom error handle
                    var errorProperty = intersectOfErrors.FirstOrDefault();
                    var validator = validations.FirstOrDefault(f => f.ParameterName == errorProperty);

                    code = validator.StatusCode;
                    name = errorProperty;
                    message = validator.ErrorMessage;
                }
                else
                {   //set error from model state
                    name = invalidParameters.FirstOrDefault();
                    context.ModelState.TryGetValue(name, out var value);
                    message = string.Join("|", value.Errors.Select(s => s.ErrorMessage));
                }


                logger.Debug($"Invalid parameter: {name}, {message}");
                context.Result = new ObjectResult(new ApiResponse
                {
                    Code = code,
                    Message = message
                })
                {
                    StatusCode = code
                };
            }

            base.OnResultExecuting(context);
        }
    }
}
