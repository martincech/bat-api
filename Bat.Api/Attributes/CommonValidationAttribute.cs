﻿namespace Bat.Api.Attributes
{
    public sealed class CommonValidationAttribute : ValidateModelStateAttribute
    {
        public CommonValidationAttribute(string idParamName)
            : base(new ValidationRecord[]
            {
                PredefinedValidationId(idParamName),
                PredefinedValidationPrecondition()
            })
        {
        }

        public CommonValidationAttribute()
            : base(new ValidationRecord[]
            {
                PredefinedValidationPrecondition()
            })
        {
        }
    }
}
