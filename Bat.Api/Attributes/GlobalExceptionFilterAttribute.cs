﻿using System;
using System.Net;
using Bat.Api.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;

namespace Bat.Api.Attributes
{
    public sealed class GlobalExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly int serverErrorCode = (int)HttpStatusCode.InternalServerError;


        public override void OnException(ExceptionContext context)
        {
            logger.Error(context.Exception, $"Global Exception");
            context.Result = new ObjectResult(new ApiResponse
            {
                Code = serverErrorCode,
                Message = GetMessageFromexception(context.Exception)
            })
            {
                StatusCode = serverErrorCode
            };

            base.OnException(context);
        }

        private string GetMessageFromexception(Exception ex)
        {
            var message = ex.Message;
            if (ex.InnerException != null)
            {
                message += ";" + GetMessageFromexception(ex.InnerException);
            }
            return message;
        }
    }
}
