﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace Bat.Api
{
    public class EditSwaggerResponseBodyMiddleware
    {
        private readonly RequestDelegate _next;

        public EditSwaggerResponseBodyMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path.ToString().Contains($"/{Startup.SwaggerEndpoint}", System.StringComparison.InvariantCulture))
            {
                var originBody = context.Response.Body;
                var newBody = new MemoryStream();
                context.Response.Body = newBody;

                await _next(context);

                newBody.Seek(0, SeekOrigin.Begin);
                using (var read = new StreamReader(newBody))
                {
                    var swaggerJson = read.ReadToEnd();
                    var swaggerDefinition = JObject.Parse(swaggerJson);
                    swaggerDefinition["host"] = context.Request.Host.Value;
                    var result = swaggerDefinition.ToString();
                    context.Response.Body = originBody;
                    context.Response.ContentLength = result.Length;
                    await context.Response.WriteAsync(result);
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}
