﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bat.Api.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Bat.Api.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created.
        /// </summary>
        internal static string Created {
            get {
                return ResourceManager.GetString("Created", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t create new user.
        /// </summary>
        internal static string CreateUserFailed {
            get {
                return ResourceManager.GetString("CreateUserFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t delete user.
        /// </summary>
        internal static string DeleteUserFailed {
            get {
                return ResourceManager.GetString("DeleteUserFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Influx DB failed.
        /// </summary>
        internal static string InfluxCreateFailed {
            get {
                return ResourceManager.GetString("InfluxCreateFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drop Influx DB failed.
        /// </summary>
        internal static string InfluxDropFailed {
            get {
                return ResourceManager.GetString("InfluxDropFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid ID Supplied.
        /// </summary>
        internal static string InvalidId {
            get {
                return ResourceManager.GetString("InvalidId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are not allowed to perform this action.
        /// </summary>
        internal static string LowPermissions {
            get {
                return ResourceManager.GetString("LowPermissions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User hasn&apos;t assigned any company.
        /// </summary>
        internal static string NoAssignedCompany {
            get {
                return ResourceManager.GetString("NoAssignedCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} not found.
        /// </summary>
        internal static string NotFound {
            get {
                return ResourceManager.GetString("NotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Precondition Failed, Required Data Constraints Not Met.
        /// </summary>
        internal static string PreconditionError {
            get {
                return ResourceManager.GetString("PreconditionError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t reset user account.
        /// </summary>
        internal static string ResetAccountFailed {
            get {
                return ResourceManager.GetString("ResetAccountFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to success.
        /// </summary>
        internal static string Success {
            get {
                return ResourceManager.GetString("Success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updated.
        /// </summary>
        internal static string Updated {
            get {
                return ResourceManager.GetString("Updated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update {0} groups failed.
        /// </summary>
        internal static string UpdateGroupsFailed {
            get {
                return ResourceManager.GetString("UpdateGroupsFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t update user.
        /// </summary>
        internal static string UpdateUserFailed {
            get {
                return ResourceManager.GetString("UpdateUserFailed", resourceCulture);
            }
        }
    }
}
