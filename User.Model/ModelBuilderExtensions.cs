﻿using System;
using Microsoft.EntityFrameworkCore;
using User.Model.Enums;
using User.Model.Models;

namespace User.Model
{
    public static class ModelBuilderExtensions
    {
        /// <summary>
        /// Insert initial data.
        /// </summary>
        /// <param name="modelBuilder"></param>
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var veitOperatorRoleId = new Guid("522e0fb7-bea3-4b04-a0b3-60fb67f32dc3");
            var companyAdminRoleId = new Guid("d466e4dc-5dd2-42b3-bec4-f463285ceffd");

            //Due to related entities created data are anonymous types!
            modelBuilder.Entity<Role>().HasData(new
            {
                Id = veitOperatorRoleId,
                Name = DbRole.VeitOperator.ToString()
            },
            new
            {
                Id = companyAdminRoleId,
                Name = DbRole.CompanyAdmin.ToString()
            });
        }
    }
}
