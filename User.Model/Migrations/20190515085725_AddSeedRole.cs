﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace User.Model.Migrations
{
    public partial class AddSeedRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("d466e4dc-5dd2-42b3-bec4-f463285ceffd"), "CompanyAdmin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("d466e4dc-5dd2-42b3-bec4-f463285ceffd"));
        }
    }
}
