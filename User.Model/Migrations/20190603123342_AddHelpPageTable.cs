﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace User.Model.Migrations
{
    public partial class AddHelpPageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HelpPages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Slug = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    Actions = table.Column<string>(nullable: true),
                    Disabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HelpPages", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HelpPages_Slug",
                table: "HelpPages",
                column: "Slug",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HelpPages");
        }
    }
}
