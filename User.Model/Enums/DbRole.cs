﻿namespace User.Model.Enums
{
    public enum DbRole
    {
        Undefined = 0,
        VeitOperator = 1,
        CompanyAdmin = 2
    }
}
