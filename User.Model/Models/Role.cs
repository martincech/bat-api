﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace User.Model.Models
{
    public partial class Role
    {
        public Role()
        {
            Id = Guid.NewGuid();
            UserRoles = new List<UserRole>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }


        public virtual ICollection<UserRole> UserRoles { get; }

        [NotMapped]
        public virtual IEnumerable<User> Users => UserRoles.Select(s => s.User);
    }
}
