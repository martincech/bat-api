﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace User.Model.Models
{
    public partial class Company
    {
        public Company()
        {
            Id = Guid.NewGuid();
            Users = new List<User>();
        }


        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Vat { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string ContactName { get; set; }
        public string Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<User> Users { get; }
    }
}
