﻿using System;
using System.ComponentModel.DataAnnotations;

namespace User.Model.Models
{
    public class HelpPage
    {
        public HelpPage()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Slug { get; set; }
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        public string Actions { get; set; }
        public bool Disabled { get; set; }
    }
}
