﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace User.Model.Models
{
#pragma warning disable CA1724 // Type names should not match namespaces
    public partial class User
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        public User()
        {
            Id = Guid.NewGuid();
            UserRoles = new List<UserRole>();
        }

        public Guid Id { get; set; }

        public Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }


        public virtual ICollection<UserRole> UserRoles { get; }

        [NotMapped]
        public virtual IEnumerable<Role> Roles => UserRoles.Select(s => s.Role);
    }
}
