﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using User.Model.Enums;
using User.Model.Models;

namespace User.Model
{
    public class UserModelContainer : DbContext, IDesignTimeDbContextFactory<UserModelContainer>
    {
        #region Constructors

        public UserModelContainer()
        {
        }

        public UserModelContainer(DbContextOptions<UserModelContainer> options)
            : base(options)
        {
        }

        #endregion

        public IQueryable<Models.User> UsersLazy => Users
            .Include(i => i.Company)
            .Include(i => i.UserRoles)
                .ThenInclude(x => x.Role);



        public virtual DbSet<Models.User> Users { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<HelpPage> HelpPages { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //define many-to-many relationships between User and Role table
            modelBuilder.Entity<UserRole>()
                .HasKey(ur => new { ur.UserId, ur.RoleId });

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.UserRoles)
                .HasForeignKey(ur => ur.UserId);

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId);

            modelBuilder.Entity<HelpPage>()
              .HasIndex(u => u.Slug)
              .IsUnique();

            modelBuilder.Seed();
        }

        public UserModelContainer CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<UserModelContainer>();
            builder.UseSqlServer("data source=localhost;initial catalog=VeitBatCloudUsers;integrated security=False;user id=sa;password=Passw0rd!;multipleactiveresultsets=True;application name=Web");
            return new UserModelContainer(builder.Options);
        }
    }
}
