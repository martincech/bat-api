﻿namespace Bat.Cloud.Model.Enums
{
    public enum DeviceType
    {
        Bat2GsmScale = 0,
        Bat2CableScale = 1,
        TemperatureSensor = 2,
        HumiditySensor = 3,
        Co2Sensor = 4,
        BatCollectorTerminal = 5,
        BatGateway = 6
    }
}
