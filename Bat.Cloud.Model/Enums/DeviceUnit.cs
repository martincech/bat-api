﻿namespace Bat.Cloud.Model.Enums
{
    public enum DeviceUnit
    {
        G = 0,
        Kg = 1,
        Lb = 2,
    }
}
