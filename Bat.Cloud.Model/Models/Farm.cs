﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bat.Cloud.Model.Models
{
    public partial class Farm
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }


        public string ContactName { get; set; }
        public string Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
