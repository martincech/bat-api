﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bat.Cloud.Model.Models
{
    public partial class Flock
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public int InitialAge { get; set; }
        public int InitialWeight { get; set; }
        public int? TargetAge { get; set; }
        public int? TargetWeight { get; set; }

        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        public Guid? BirdId { get; set; }

        public Guid? HouseId { get; set; }
        public virtual House House { get; set; }
        public Guid? DeviceId { get; set; }
        public virtual Device Device { get; set; }
    }
}
