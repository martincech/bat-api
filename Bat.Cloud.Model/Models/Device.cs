﻿using System;
using Bat.Cloud.Model.Enums;

namespace Bat.Cloud.Model.Models
{
    public partial class Device
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public DeviceType Type { get; set; }
        public DeviceUnit Unit { get; set; }
        public string Description { get; set; }
        public string PhoneNumber { get; set; }

        public Guid? FarmId { get; set; }
        public virtual Farm Farm { get; set; }
        public Guid? HouseId { get; set; }
        public virtual House House { get; set; }
        public Guid? ParentId { get; set; }
        public virtual Device Parent { get; set; }
    }
}
