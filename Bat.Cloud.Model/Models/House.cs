﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bat.Cloud.Model.Models
{
    public partial class House
    {
        public Guid Id { get; set; }


        [Required]
        public string Name { get; set; }


        public Guid FarmId { get; set; }
        public virtual Farm Farm { get; set; }
    }
}
