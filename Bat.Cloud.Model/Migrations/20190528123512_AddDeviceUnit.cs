﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bat.Cloud.Model.Migrations
{
    public partial class AddDeviceUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Unit",
                table: "Devices",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Unit",
                table: "Devices");
        }
    }
}
