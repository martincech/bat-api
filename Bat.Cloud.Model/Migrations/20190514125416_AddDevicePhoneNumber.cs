﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bat.Cloud.Model.Migrations
{
    public partial class AddDevicePhoneNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Devices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Devices");
        }
    }
}
