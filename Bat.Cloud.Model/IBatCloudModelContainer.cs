﻿using System;
using Bat.Cloud.Model.Models;
using Microsoft.EntityFrameworkCore;

namespace Bat.Cloud.Model
{
    public interface IBatCloudModelContainer : IDisposable
    {
        DbSet<Device> Devices { get; set; }
        DbSet<Farm> Farms { get; set; }
        DbSet<Flock> Flocks { get; set; }
        DbSet<House> Houses { get; set; }
    }
}
