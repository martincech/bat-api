﻿using System.Linq;
using Bat.Cloud.Model.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Bat.Cloud.Model
{
    public class BatCloudModelContainer : DbContext, IDesignTimeDbContextFactory<BatCloudModelContainer>, IBatCloudModelContainer
    {
        private readonly string connectionString;

        #region Constructors

        public BatCloudModelContainer()
        {
        }

        public BatCloudModelContainer(string connectionString)
           : base()
        {
            this.connectionString = connectionString;
        }

        public BatCloudModelContainer(DbContextOptions<BatCloudModelContainer> options)
            : base(options)
        {
        }

        #endregion

        public BatCloudModelContainer CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<BatCloudModelContainer>();
            builder.UseSqlServer("data source=localhost,1433;initial catalog=VeitBatCloudData;User Id=sa;Password=Passw0rd!;multipleactiveresultsets=True;application name=Web");

            return new BatCloudModelContainer(builder.Options);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }


        public IQueryable<Device> DevicesLazy => Devices
            .Include(i => i.Farm)
            .Include(i => i.Parent)
            .Include(i => i.House)
                .ThenInclude(t => t.Farm);
        public IQueryable<Flock> FlocksLazy => Flocks
            .Include(i => i.House)
                .ThenInclude(t => t.Farm)
            .Include(i => i.Device)
                .ThenInclude(t => t.House)
            .Include(i => i.Device.Farm);
        public IQueryable<House> HousesLazy => Houses
            .Include(i => i.Farm);


        public IQueryable<Bird.Model.Models.Company> BirdCompaniesLazy => BirdCompanies
            .Include(i => i.Products)
                .ThenInclude(tt => tt.Birds)
                    .ThenInclude(t => t.CurvePoints);
        public IQueryable<Bird.Model.Models.Product> BirdProductsLazy => BirdProducts
            .Include(i => i.Birds)
                .ThenInclude(t => t.CurvePoints)
            .Include(i => i.Company);
        public IQueryable<Bird.Model.Models.Bird> BirdsLazy => Birds
            .Include(i => i.CurvePoints)
            .Include(i => i.Product)
                .ThenInclude(tt => tt.Company);



        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<Farm> Farms { get; set; }
        public virtual DbSet<Flock> Flocks { get; set; }
        public virtual DbSet<House> Houses { get; set; }

        public virtual DbSet<Bird.Model.Models.Bird> Birds { get; set; }
        public virtual DbSet<Bird.Model.Models.Company> BirdCompanies { get; set; }
        public virtual DbSet<Bird.Model.Models.Product> BirdProducts { get; set; }
    }
}
