#!/bin/bash -xe
for f in *.in; do cat $f | envsubst '${VERSION},${ENVIRONMENT},${INFLUX},${INFLUX_METRICS_DB},${KAFKA},${DB},${DB_USER},${PRIME_OPERATOR_ID},${DB_PASSWORD},${COGNITO_DOMAIN},${COGNITO_REGION},${COGNITO_POOL_ID},${IAM_ID},${IAM_SECRET},${SENTRY_DSN}' > `basename $f .in`; done

cp nlog.${ENVIRONMENT}.config nlog.config
exec dotnet Bat.Api.dll

