﻿using System;
using System.Collections.Generic;
using System.IO;
using Bat.Cloud.Model;
using Bat.Migration.OldCompanyDatabase;
using Bat.Migration.OldUserDatabase;
using InfluxData.Net.InfluxDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MigrationV07.Model;
using Statistic;
using User.Model;

namespace Bat.Migration
{
    public static class Configuration
    {
        static readonly IConfigurationRoot config = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false).Build();

        public static string GetConfiguration()
        {
            return File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"));
        }

        public static List<string> Companies()
        {
            return config.GetSection(nameof(Companies)).Get<List<string>>();
        }

        public static OldUserModelContainer OldUserDatabase()
        {
            var options = ConfigureContext<OldUserModelContainer>(config.GetConnectionString(nameof(OldUserDatabase)));
            return new OldUserModelContainer(options);
        }

        public static UserModelContainer NewUserDatabase()
        {
            var options = ConfigureContext<UserModelContainer>(config.GetConnectionString(nameof(NewUserDatabase)));
            return new UserModelContainer(options);
        }

        public static OldBatCloudModelContainer OldCompanyDatabase(string database)
        {
            var options = ConfigureContext<OldBatCloudModelContainer>(FormatString(config.GetConnectionString(nameof(OldCompanyDatabase)), database));
            return new OldBatCloudModelContainer(options);
        }

        public static BatCloudModelContainer NewCompanyDatabase(string database)
        {
            var options = ConfigureContext<BatCloudModelContainer>(FormatString(config.GetConnectionString(nameof(NewCompanyDatabase)), database));
            return new BatCloudModelContainer(options);
        }

        public static MigrationV07ModelContainer MigrationDatabase()
        {
            var options = ConfigureContext<MigrationV07ModelContainer>(config.GetConnectionString(nameof(MigrationDatabase)));
            return new MigrationV07ModelContainer(options);
        }

        public static InfluxContext InfluxContext()
        {
            return new InfluxContext(new Uri(InfluxDb()));
        }

        public static InfluxDbClient InfluxClient()
        {
            return new InfluxDbClient(InfluxDb(), "user", "pass", InfluxData.Net.Common.Enums.InfluxDbVersion.Latest);
        }

        static string InfluxDb()
        {
            return config[nameof(InfluxDb)];
        }

        static DbContextOptions<T> ConfigureContext<T>(string connectionString) where T : DbContext
        {
            var b = new DbContextOptionsBuilder<T>();
            b.UseSqlServer(connectionString);
            return b.Options;
        }

        static string FormatString(string connectionString, string databaseName)
        {
            return string.Format(connectionString, databaseName);
        }
    }
}
