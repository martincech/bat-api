﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bat.Cloud.Model;
using Bat.Cloud.Model.Models;
using Bat.Migration.Extensions;
using Bat.Migration.OldCompanyDatabase;
using Bat.Migration.OldCompanyDatabase.Map;
using InfluxData.Net.InfluxDb;
using Microsoft.EntityFrameworkCore;
using MigrationV07.Model;

namespace Bat.Migration
{
    class MigrateCompanyDB
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        readonly string sourceDbName;
        readonly string targetDbName;

        readonly BatCloudModelContainer target;
        readonly OldBatCloudModelContainer source;
        readonly MigrationV07ModelContainer migrationDb;
        readonly InfluxDbClient influxDb;
        readonly string companyName;

        readonly Dictionary<int, Guid> farmMap;
        readonly Dictionary<int, Guid> houseMap;
        readonly Dictionary<int, KeyValuePair<Guid, string>> terminalMap;
        readonly Dictionary<int, KeyValuePair<Guid, string>> deviceMap;

        Farm defaultFarm;
        Farm DefaultFarm => defaultFarm ?? (defaultFarm = CreateDefaultFarm());

        public MigrateCompanyDB(
            OldBatCloudModelContainer source,
            BatCloudModelContainer target,
            MigrationV07ModelContainer migrationDb,
            InfluxDbClient influxDb,
            string companyName
            )
        {
            this.source = source;
            this.target = target;
            this.migrationDb = migrationDb;
            this.influxDb = influxDb;
            this.companyName = companyName;

            sourceDbName = source.Database.GetDbConnection().Database;
            targetDbName = target.Database.GetDbConnection().Database;

            farmMap = new Dictionary<int, Guid>();
            houseMap = new Dictionary<int, Guid>();
            terminalMap = new Dictionary<int, KeyValuePair<Guid, string>>();
            deviceMap = new Dictionary<int, KeyValuePair<Guid, string>>();
        }

        public bool Run()
        {
            if (string.Equals(sourceDbName, targetDbName, StringComparison.OrdinalIgnoreCase))
            {
                logger.Fatal($"Source and target company database({sourceDbName}) cannot be same!");
                return false;
            }

            if (!source.Exist())
            {
                logger.Fatal($"Source company database '{sourceDbName}' do not exist!");
                return false;
            }

            if (target.Exist())
            {
                logger.Warn($"Target company database '{targetDbName}' already exist!. Deleting previous version of target database.");
                target.Database.EnsureDeleted();
            }

            var migrateInflux = influxDb.Exist(sourceDbName);
            if (!migrateInflux)
            {
                logger.Error($"Source influx database({sourceDbName}) do not exist. Skipping influx database migration.");
            }

            logger.Info($"Migrate company data from '{sourceDbName}' to '{targetDbName}'.");
            target.Database.Migrate();

            MigrateFarms();
            MigrateHouses();
            MigrateTerminals();
            MigrateDevices();
            MigrateBirds();
            MigrateEvents(migrateInflux);
            MigrationMapping();

            return true;
        }

        void MigrationMapping()
        {
            migrationDb.Database.Migrate();
            var guid = Guid.Parse(targetDbName);
            var migrationCompany = migrationDb.CompaniesLazy.FirstOrDefault(f => f.Id == guid);
            if (migrationCompany == null)
            {
                migrationCompany = new MigrationV07.Model.Models.Company { Id = guid };
                migrationDb.Companies.Add(migrationCompany);
            }
            else
            {
                migrationDb.Devices.RemoveRange(migrationCompany.Devices);
            }

            foreach (var device in deviceMap.Union(terminalMap))
            {
                migrationCompany.Devices.Add(new MigrationV07.Model.Models.Device { Id = device.Value.Key, OldUid = device.Value.Value });
            }

            migrationDb.SaveChanges();
        }

        void MigrateFarms()
        {
            foreach (var item in source.Farms)
            {
                var farm = new Farm
                {
                    Id = Guid.NewGuid(),
                    Name = item.Name,
                    Address = item.Location,
                };
                target.Farms.Add(farm);
                farmMap.Add(item.Id, farm.Id);
            }
            target.SaveChanges();
            logger.Info($"Migrated {farmMap.Count} farms.");
        }

        void MigrateHouses()
        {
            foreach (var item in source.Houses)
            {
                var house = item.Map(GetFarmId(item.FarmId) ?? DefaultFarm.Id);
                target.Houses.Add(house);
                houseMap.Add(item.Id, house.Id);
            }
            target.SaveChanges();
            logger.Info($"Migrated {houseMap.Count} houses.");
        }

        void MigrateTerminals()
        {
            var devices = source.Devices.ToList();
            var houses = source.Houses.ToList();

            foreach (var item in source.Terminals)
            {
                var terminal = item.Map(houses, devices, farmMap);
                target.Devices.Add(terminal);
                terminalMap.Add(item.Id, new KeyValuePair<Guid, string>(terminal.Id, item.Uid));
            }
            target.SaveChanges();
            logger.Info($"Migrated {terminalMap.Count} terminals.");
        }

        void MigrateDevices()
        {
            foreach (var item in source.Devices)
            {
                var type = item.Type.Map();
                if (type == null)
                {
                    logger.Trace($"Device (id:{item.Id}|uid:{item.Uid}) will not be migrated. Unsuported device type {item.Type}.");
                    continue;
                }

                var device = item.Map(type);
                device.ParentId = GetTerminalId(item.TerminalId)?.Key;
                device.HouseId = GetHouseId(item.HouseId);

                target.Devices.Add(device);
                deviceMap.Add(item.Id, new KeyValuePair<Guid, string>(device.Id, item.Uid));
            }
            target.SaveChanges();
            logger.Info($"Migrated {deviceMap.Count} devices.");
        }

        void MigrateBirds()
        {
            var company = new Bird.Model.Models.Company { Id = Guid.NewGuid(), Name = companyName };
            var product = new Bird.Model.Models.Product { Id = Guid.NewGuid(), Company = company, Name = "(default)" };

            foreach (var item in source.Curves)
            {
                var bird = item.Map(source.CurvePoins.Where(w => w.CurveId == item.Id).ToList());
                if (bird != null)
                {
                    bird.Product = product;
                    target.Add(bird);
                }
            }
            target.SaveChanges();
            logger.Info($"Migrated {target.Birds.Count()} birds.");
        }

        void MigrateEvents(bool migrateInflux)
        {
            var devices = source.Devices.ToList();
            foreach (var item in source.Events)
            {
                var flock = item.Map();
                var eventDevices = source.EventDevice.Where(w => w.EventId == item.Id && deviceMap.ContainsKey(w.DeviceId));
                var edIds = eventDevices.Select(s => s.DeviceId);
                var devicesInEvent = devices.Where(w => edIds.Contains(w.Id));
                if (eventDevices.Count() == 1)
                {
                    flock.DeviceId = deviceMap[eventDevices.First().DeviceId].Key;
                }
                else if (eventDevices.Count() > 1)
                {
                    var houseId = devicesInEvent.GroupBy(gr => gr.HouseId)
                         .OrderByDescending(g => g.Count())
                         .Select(g => g.Key)
                         .First();

                    flock.HouseId = GetHouseId(houseId);
                }
                target.Flocks.Add(flock);
                if (migrateInflux)
                {
                    MigrateInfluxData(item.Id.ToString(), flock.Id.ToString());
                    if (item.To > DateTime.Now)
                    {
                        foreach (var d in devicesInEvent)
                        {
                            MigrateInfluxData(d.Uid, deviceMap[d.Id].Key.ToString(), "device", "device");
                        }
                    }
                }
            }
            target.SaveChanges();
            logger.Info($"Migrated {target.Flocks.Count()} flocks.");
        }

        void MigrateInfluxData(string fromId, string toId, string fromTable = "event", string toTable = "flock")
        {
            var from = $"\"{sourceDbName}\".\"1440_m\".\"{fromTable}_{fromId}_weight_1440m\"";
            var to = $"\"{targetDbName}\".\"1440_m\".\"{toTable}_{toId}_weight_1440m\"";

            logger.Trace($"Migrate influx data from {from} to {to}.");

            influxDb.Client.QueryAsync($"SELECT * INTO {to} FROM {from} GROUP BY *").GetAwaiter().GetResult();
        }

        Guid? GetHouseId(int? oldId) => GetId(houseMap, oldId);
        Guid? GetFarmId(int? oldId) => GetId(farmMap, oldId);
        KeyValuePair<Guid, string>? GetTerminalId(int? oldId) => GetId(terminalMap, oldId);

        static T? GetId<T>(Dictionary<int, T> map, int? oldId) where T : struct
        {
            if (oldId.HasValue && map != null && map.ContainsKey(oldId.Value)) return map[oldId.Value];
            return null;
        }

        Farm CreateDefaultFarm()
        {
            var farm = new Farm { Id = Guid.NewGuid(), Name = "(default)" };
            target.Farms.Add(farm);
            target.SaveChanges();
            return farm;
        }

        public static bool Start(string sourceDatabase, Guid targetDatabase, string companyName)
        {
            try
            {
                using (var srcDb = Configuration.OldCompanyDatabase(sourceDatabase))
                using (var trgDb = Configuration.NewCompanyDatabase(targetDatabase.ToString()))
                using (var mgrDb = Configuration.MigrationDatabase())
                {
                    CreateTargetInfluxDatabaseAsync(targetDatabase).GetAwaiter().GetResult();
                    var migration = new MigrateCompanyDB(srcDb, trgDb, mgrDb, Configuration.InfluxClient(), companyName);
                    return migration.Run();
                }
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to migrate company datebase {sourceDatabase} to {targetDatabase.ToString()}!");
                return false;
            }
        }

        static async Task CreateTargetInfluxDatabaseAsync(Guid targetDatabase)
        {
            using (var influxContext = Configuration.InfluxContext())
            {
                await influxContext.DropCompanyContextAsync(targetDatabase.ToString());
                await influxContext.CreateCompanyContextAsync(targetDatabase.ToString());
            }
        }
    }
}
