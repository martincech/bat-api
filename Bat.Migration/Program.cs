﻿namespace Bat.Migration
{
    static class Program
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main()
        {
            Logger.Init();
            logger.Info("Bat cloud Migration Tool 1.0");
            logger.Info($"Configuration:\n{Configuration.GetConfiguration()}\n");
            MigrateDatabases.Start();
            logger.Info("Migration finished. Press <Enter> to exit.");
            System.Console.ReadLine();
        }
    }
}
