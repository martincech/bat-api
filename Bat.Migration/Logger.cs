﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace Bat.Migration
{
    public static class Logger
    {
        public static void Init()
        {
            var config = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget("target1")
            {
                Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}"
            };
            config.AddTarget(consoleTarget);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, consoleTarget);
            LogManager.Configuration = config;
        }
    }
}
