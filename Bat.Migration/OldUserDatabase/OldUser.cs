﻿namespace Bat.Migration.OldUserDatabase
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Users")]
    public class OldUser
    {
        public Guid Id { get; set; }
        public Guid? CompanyId { get; set; }
    }
}
