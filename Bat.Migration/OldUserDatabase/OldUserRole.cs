﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldUserDatabase
{
    [Table("UserRole")]
    public class OldUserRole
    {
        [Column("Users_Id")]
        public Guid UserId { get; set; }

        [Column("Roles_Id")]
        public Guid RoleId { get; set; }
    }
}
