﻿namespace Bat.Migration.OldUserDatabase
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Companies")]
    public class OldCompany
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Database { get; set; }

        public string PersonName { get; set; }

        public string PersonEmail { get; set; }

        public string PersonPhone { get; set; }

        public int? Vat { get; set; }

        public string Address { get; set; }

        public string Postal { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
