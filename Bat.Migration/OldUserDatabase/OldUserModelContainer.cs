﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Bat.Migration.OldUserDatabase
{
    public class OldUserModelContainer : DbContext
    {
        public OldUserModelContainer(DbContextOptions<OldUserModelContainer> options)
            : base(options)
        {
        }

        public virtual DbSet<OldCompany> Companies { get; set; }
        public virtual DbSet<OldUser> Users { get; set; }
        public virtual DbSet<OldRole> Roles { get; set; }
        public virtual DbSet<OldUserRole> UserRole { get; set; }

        public List<Guid> GetAdminUserIds()
        {
            var adminRole = Roles.FirstOrDefault(f => f.Name == "Veit");
            return UserRole.Where(w => w.RoleId == adminRole.Id).Select(s => s.UserId).ToList();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OldUserRole>()
             .HasKey(bc => new { bc.UserId, bc.RoleId });
        }
    }
}
