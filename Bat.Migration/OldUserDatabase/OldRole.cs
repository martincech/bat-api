﻿namespace Bat.Migration.OldUserDatabase
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Roles")]
    public class OldRole
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
