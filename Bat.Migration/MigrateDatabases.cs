﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Migration.Extensions;
using Bat.Migration.OldUserDatabase;
using Microsoft.EntityFrameworkCore;
using User.Model;
using User.Model.Enums;
using User.Model.Models;

namespace Bat.Migration
{
    public class MigrateDatabases
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        readonly UserModelContainer target;
        readonly OldUserModelContainer source;

        List<Guid> admins;
        Role adminRole;

        MigrateDatabases(OldUserModelContainer source, UserModelContainer target)
        {
            this.source = source;
            this.target = target;
        }

        public bool Run(List<string> companiesToMigrate)
        {
            if (!source.Exist())
            {
                logger.Fatal($"Old user database '{source.Database.GetDbConnection().Database}' do not exist!");
                return false;
            }

            logger.Debug($"Ensure that user database '{source.Database.GetDbConnection().Database}' exist!\n");
            target.Database.Migrate();

            admins = source.GetAdminUserIds();
            adminRole = target.Roles.FirstOrDefault(f => f.Name == DbRole.VeitOperator.ToString());

            foreach (var company in source.Companies)
            {
                if (companiesToMigrate != null
                    && companiesToMigrate.Any()
                    && !companiesToMigrate.Contains(company.Id.ToString().ToUpperInvariant()))
                {
                    logger.Warn($"Skipping migration of company {company.Name}(id:{company.Id}).\n");
                    continue;
                }
                MigrateCompany(company);
            }
            return true;
        }

        void MigrateCompany(OldCompany company)
        {
            logger.Info($"Migrate company {company.Name}(id:{company.Id})");
            using (var transaction = target.Database.BeginTransaction())
            {
                try
                {
                    CopyCompanyRecord(company);
                    CopyCompanyUsers(company.Id);

                    if (MigrateCompanyDB.Start(company.Database, company.Id, company.Name))
                    {
                        transaction.Commit();
                        logger.Info($"Migration of company {company.Name}(id:{company.Id}) was succesfful.\n");
                        return;
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e, $"Error occured during users migration from company {company.Name}(id:{company.Id}).");
                }
                logger.Error($"Migration of company {company.Name}(id:{company.Id}) failed.\n");
                transaction.Rollback();
            }
        }

        void CopyCompanyRecord(OldCompany cp)
        {
            var company = target.Companies.FirstOrDefault(f => f.Id == cp.Id);
            var create = company == null;
            if (create)
            {
                company = new Company
                {
                    Id = cp.Id,
                };
            }
            company.Address = cp.Address;
            company.City = cp.City;
            company.ContactName = cp.PersonName;
            company.Country = cp.Country;
            company.Email = cp.PersonEmail;
            company.Name = cp.Name;
            company.Phone = cp.PersonPhone;
            company.Vat = cp.Vat?.ToString();
            company.Zip = cp.Postal;

            target.AddOrUpdate(company, create);
            target.SaveChanges();
        }

        void CopyCompanyUsers(Guid companyId)
        {
            var users = source.Users.Where(w => w.CompanyId == companyId).ToList();

            foreach (var user in users)
            {
                CopyCompanyUser(user, admins);
            }
            target.SaveChanges();
            logger.Info($"Migrated {users.Count} users.");
        }

        void CopyCompanyUser(OldUser us, List<Guid> admins)
        {
            var user = target.UsersLazy.FirstOrDefault(f => f.Id == us.Id);
            var create = user == null;
            if (create)
            {
                user = new User.Model.Models.User
                {
                    Id = us.Id
                };
            }

            user.CompanyId = us.CompanyId;
            if (admins.Contains(user.Id))
            {
                AddAdminRole(user);
            }
            target.AddOrUpdate(user, create);
        }

        void AddAdminRole(User.Model.Models.User user)
        {
            if (!user.Roles.Any() && adminRole != null)
            {
                user.UserRoles.Add(new UserRole
                {
                    RoleId = adminRole.Id,
                    UserId = user.Id,
                });
            }
        }

        public static bool Start()
        {

            using (var oldContext = Configuration.OldUserDatabase())
            using (var newContext = Configuration.NewUserDatabase())
            {
                var migration = new MigrateDatabases(oldContext, newContext);
                return migration.Run(Configuration.Companies());
            }
        }
    }
}
