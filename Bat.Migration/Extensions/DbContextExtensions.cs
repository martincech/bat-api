﻿using System.Linq;
using InfluxData.Net.InfluxDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Bat.Migration.Extensions
{
    public static class DbContextExtensions
    {
        public static bool Exist(this DbContext dbContext)
        {
            return dbContext.Database.GetService<IRelationalDatabaseCreator>().Exists();
        }

        public static EntityEntry AddOrUpdate<T>(this DbContext dbContext, T item, bool create) where T : class
        {
            return create ? dbContext.Add(item) : dbContext.Update(item);
        }

        public static bool Exist(this InfluxDbClient dbClient, string database)
        {
            var databases = dbClient.Database.GetDatabasesAsync().GetAwaiter().GetResult();
            return databases.Any(d => d.Name == database);
        }
    }
}
