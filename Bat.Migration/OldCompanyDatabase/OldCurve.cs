﻿using System.ComponentModel.DataAnnotations.Schema;
using Bat.Migration.OldCompanyDatabase.Enums;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("Curves")]
    public class OldCurve
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OldCurveType Type { get; set; }
    }
}
