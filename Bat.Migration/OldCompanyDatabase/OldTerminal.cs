﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("Terminals")]
    public class OldTerminal
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Uid { get; set; }

        public string Name { get; set; }
    }
}
