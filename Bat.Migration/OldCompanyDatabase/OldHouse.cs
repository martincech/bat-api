﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("Houses")]
    public class OldHouse
    {
        public int Id { get; set; }

        public int? FarmId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Location { get; set; }
    }
}
