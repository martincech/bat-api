﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("Events")]
    public class OldEvent
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public int StartAge { get; set; }

        public int? CurveId { get; set; }

        public int? CurveFemaleId { get; set; }

        public double InitialWeight { get; set; }

        public double InitialWeightFemale { get; set; }

        public int? BirdId { get; set; }

    }
}
