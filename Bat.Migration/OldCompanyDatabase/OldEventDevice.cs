﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("EventDevice")]
    public class OldEventDevice
    {
        [Column("Events_Id")]
        public int EventId { get; set; }

        [Column("Devices_Id")]
        public int DeviceId { get; set; }
    }
}
