﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("Devices")]
    public class OldDevice
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Uid { get; set; }

        public string Name { get; set; }

        public OldDeviceType Type { get; set; }

        public int? HouseId { get; set; }

        public int? TerminalId { get; set; }
    }
}
