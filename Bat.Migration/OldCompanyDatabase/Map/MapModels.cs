﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Cloud.Model.Enums;
using Bat.Cloud.Model.Models;
using Bat.Migration.OldCompanyDatabase;

namespace Bat.Migration.OldCompanyDatabase.Map
{
    public static class MapModels
    {
        public static Flock Map(this OldEvent item)
        {
            return new Flock
            {
                Id = Guid.NewGuid(),
                InitialAge = item.StartAge,
                InitialWeight = Convert.ToInt32(item.InitialWeight),
                Name = item.Name,
                StartDate = item.From,
                TargetAge = (int)Math.Ceiling((item.To - item.From).TotalDays),
                EndDate = item.To < DateTime.Now ? item.To : (DateTimeOffset?)null,
            };
        }

        public static Device Map(this OldTerminal item, List<OldHouse> houses, List<OldDevice> devices, Dictionary<int, Guid> farmMap)
        {
            var terminal = new Device
            {
                Id = Guid.NewGuid(),
                Name = item.Name,
                Type = DeviceType.BatCollectorTerminal
            };

            var houseIds = devices
                 .Where(w => w.HouseId.HasValue && w.TerminalId.HasValue && w.TerminalId == item.Id)
                 .Select(s => s.HouseId);

            var farmId = houses
                .Where(w => w.FarmId.HasValue && houseIds.Contains(w.Id))
                .GroupBy(g => g.FarmId)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault();

            if (farmId.HasValue && farmMap.ContainsKey(farmId.Value))
            {
                terminal.FarmId = farmMap[farmId.Value];
            }

            return terminal;
        }

        public static Device Map(this OldDevice item, DeviceType? type)
        {
            return new Device
            {
                Id = Guid.NewGuid(),
                Name = item.Name,
                Type = type.Value,
            };
        }

        public static House Map(this OldHouse item, Guid farmId)
        {
            return new House
            {
                Id = Guid.NewGuid(),
                Name = item.Name,
                FarmId = farmId,
            };
        }

        public static Bird.Model.Models.Bird Map(this OldCurve item, List<OldCurvePoint> curvePoints)
        {
            if (item == null || item.Type.Map() == null || curvePoints == null || !curvePoints.Any()) return null;
            var bird = new Bird.Model.Models.Bird
            {
                Id = Guid.NewGuid(),
                Sex = item.Type.Map().Value,
                DateType = Bird.Model.Enums.DateType.Day,
                Name = item.Name,
                Duration = curvePoints.Max(m => m.Day)
            };
            foreach (var curvePoint in curvePoints.Map())
            {
                bird.CurvePoints.Add(curvePoint);
            }
            return bird;
        }

        public static List<Bird.Model.Models.CurvePoint> Map(this List<OldCurvePoint> curvePoints)
        {
            return curvePoints?.Where(w => w.Value.HasValue)
                .Select(s => new Bird.Model.Models.CurvePoint
                {
                    Day = s.Day,
                    Weight = (int)s.Value.Value,
                }).ToList();
        }
    }
}
