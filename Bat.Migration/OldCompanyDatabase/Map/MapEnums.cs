﻿using Bat.Cloud.Model.Enums;
using Bat.Migration.OldCompanyDatabase.Enums;
using Bird.Model.Enums;

namespace Bat.Migration.OldCompanyDatabase.Map
{
    static class MapEnums
    {
        public static DeviceType? Map(this OldDeviceType deviceType)
        {
            switch (deviceType)
            {
                case OldDeviceType.Bat3:
                case OldDeviceType.Bat2Cable:
                    return DeviceType.Bat2CableScale;
                default:
                    return null;
            }
        }

        public static Sex? Map(this OldCurveType curveType)
        {
            switch (curveType)
            {
                case OldCurveType.BirdFemale:
                    return Sex.Female;
                case OldCurveType.BirdMale:
                    return Sex.Male;
                case OldCurveType.BirdUndefined:
                    return Sex.Irrelevant;
                default:
                    return null;
            }
        }
    }
}
