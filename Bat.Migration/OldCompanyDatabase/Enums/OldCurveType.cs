﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat.Migration.OldCompanyDatabase.Enums
{
    public enum OldCurveType
    {
        BirdFemale = 0,
        BirdMale = 1,
        BirdUndefined = 2,
        Humidity = 3,
        Temperature = 4,
        Detection = 5,
        Co2 = 6
    }
}
