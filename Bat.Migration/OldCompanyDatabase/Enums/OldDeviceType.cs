﻿namespace Bat.Migration.OldCompanyDatabase
{
    public enum OldDeviceType
    {
        None = 0,
        SensorPack = 1,
        Bat2Gsm = 2,
        ExternalSensorPack = 3,
        Bat2Cable = 4,
        ExternalTemperature = 5,
        ExternalHumidity = 6,
        ExternalCo2 = 7,
        ExternalWeight = 8,
        Bat3 = 9
    }
}
