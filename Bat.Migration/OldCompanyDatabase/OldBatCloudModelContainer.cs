﻿using Microsoft.EntityFrameworkCore;

namespace Bat.Migration.OldCompanyDatabase
{
    public class OldBatCloudModelContainer : DbContext
    {
        public virtual DbSet<OldDevice> Devices { get; set; }
        public virtual DbSet<OldEvent> Events { get; set; }
        public virtual DbSet<OldEventDevice> EventDevice { get; set; }
        public virtual DbSet<OldFarm> Farms { get; set; }
        public virtual DbSet<OldHouse> Houses { get; set; }
        public virtual DbSet<OldTerminal> Terminals { get; set; }
        public virtual DbSet<OldCurve> Curves { get; set; }
        public virtual DbSet<OldCurvePoint> CurvePoins { get; set; }

        public OldBatCloudModelContainer(DbContextOptions<OldBatCloudModelContainer> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OldEventDevice>()
               .HasKey(bc => new { bc.EventId, bc.DeviceId });
        }
    }
}
