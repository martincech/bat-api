﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Bat.Migration.OldCompanyDatabase
{
    [Table("CurvePoints")]
    public class OldCurvePoint
    {
        public int Id { get; set; }
        public int CurveId { get; set; }
        public int Day { get; set; }
        public double? Value { get; set; }
    }
}
