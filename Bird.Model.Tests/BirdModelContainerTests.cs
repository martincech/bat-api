﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Cloud.Model.Tests;
using Bird.Model.Models;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Bird.Model.Tests
{
    public sealed class BirdModelContainerTests : IDisposable
    {
        private readonly BirdModelContainer container;

        public BirdModelContainerTests()
        {
            var options = new DbContextOptionsBuilder<BirdModelContainer>()
                .UseInMemoryDatabase(databaseName: "BirdModelContainerTests_InMemoryDB")
                .Options;

            container = new BirdModelContainer(options);
        }

        public async void Dispose()
        {
            container.Birds.RemoveRange(container.Birds);
            container.Products.RemoveRange(container.Products);
            container.Companies.RemoveRange(container.Companies);

            await container.SaveChangesAsync();
            container.Dispose();
            GC.SuppressFinalize(this);
        }



        [Fact]
        public void Companies_LazyLoading()
        {
            // Arrange
            var bird = GetDemoBirdWithReferences();
            container.Companies.Add(bird.Product.Company);
            container.SaveChanges();

            // Act
            var company = container.CompaniesLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(company);
            Assert.True(IsCompanySame(bird.Product.Company, company));
        }



        [Fact]
        public void Products_LazyLoading()
        {
            // Arrange
            var bird = GetDemoBirdWithReferences();
            container.Products.Add(bird.Product);
            container.SaveChanges();

            // Act
            var product = container.ProductsLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(product);
            Assert.True(IsProductSame(bird.Product, product));
        }

        [Fact]
        public void Birds_LazyLoading()
        {
            // Arrange
            var expectedBird = GetDemoBirdWithReferences();
            container.Birds.Add(expectedBird);
            container.SaveChanges();

            // Act
            var bird = container.BirdsLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(bird);
            Assert.True(IsBirdSame(expectedBird, bird));
        }


        #region Compare methods

        private static bool IsCompanySame(Company c1, Company c2)
        {
            if (!TestHelper.PreCheckForNull(c1, c2))
            {
                return false;
            }

            return c1.Name == c2.Name &&
                   IsProductSame(c1.Products, c2.Products);
        }

        private static bool IsProductSame(ICollection<Product> p1, ICollection<Product> p2)
        {
            if (!TestHelper.PreCheckCollectionForNull(p1, p2))
            {
                return false;
            }

            p1 = p1.OrderBy(o => o.Id).ToList();
            p2 = p2.OrderBy(o => o.Id).ToList();
            for (var i = 0; i < p1.Count; i++)
            {
                if (!IsProductSame(p1.ElementAt(i), p2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsProductSame(Product p1, Product p2)
        {
            if (!TestHelper.PreCheckForNull(p1, p2))
            {
                return false;
            }

            return p1.Name == p2.Name &&
                   IsBirdSame(p1.Birds, p2.Birds);
        }

        private static bool IsBirdSame(ICollection<Models.Bird> b1, ICollection<Models.Bird> b2)
        {
            if (!TestHelper.PreCheckCollectionForNull(b1, b2))
            {
                return false;
            }

            b1 = b1.OrderBy(o => o.Id).ToList();
            b2 = b2.OrderBy(o => o.Id).ToList();
            for (var i = 0; i < b1.Count; i++)
            {
                if (!IsBirdSame(b1.ElementAt(i), b2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsBirdSame(Models.Bird b1, Models.Bird b2)
        {
            if (!TestHelper.PreCheckForNull(b1, b2))
            {
                return false;
            }

            return b1.Name == b2.Name &&
                   b1.Sex == b2.Sex &&
                   b1.Duration == b2.Duration &&
                   IsCurveSame(b1.CurvePoints, b2.CurvePoints);
        }

        private static bool IsCurveSame(ICollection<CurvePoint> c1, ICollection<CurvePoint> c2)
        {
            if (!TestHelper.PreCheckCollectionForNull(c1, c2))
            {
                return false;
            }

            c1 = c1.OrderBy(o => o.Day).ToList();
            c2 = c2.OrderBy(o => o.Day).ToList();
            for (var i = 0; i < c1.Count; i++)
            {
                if (!IsCurvePointSame(c1.ElementAt(i), c2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsCurvePointSame(CurvePoint p1, CurvePoint p2)
        {
            if (!TestHelper.PreCheckForNull(p1, p2))
            {
                return false;
            }
            return p1.Day == p2.Day && p1.Weight == p2.Weight;
        }

        #endregion

        #region Demo data

        private static Models.Bird GetDemoBirdWithReferences()
        {
            var bird = GetDemobird();
            var product = GetDemoProduct();
            product.Company = GetDemoCompany();
            bird.Product = product;
            return bird;
        }

        private static Company GetDemoCompany()
        {
            return new Company
            {
                Name = "__Test Bird Company"
            };
        }

        private static Product GetDemoProduct()
        {
            return new Product
            {
                Name = "_test Product"
            };
        }

        private static Models.Bird GetDemobird()
        {
            var bird = new Models.Bird
            {
                Name = "_ test bird",
                Sex = Enums.Sex.Female,
                Duration = 456
            };
            bird.CurvePoints.Add(new CurvePoint { Day = 1, Weight = 10 });
            bird.CurvePoints.Add(new CurvePoint { Day = 2, Weight = 20 });
            bird.CurvePoints.Add(new CurvePoint { Day = 5, Weight = 30 });
            bird.CurvePoints.Add(new CurvePoint { Day = 11, Weight = 40 });
            bird.CurvePoints.Add(new CurvePoint { Day = 18, Weight = 1450 });
            return bird;
        }

        #endregion
    }
}
