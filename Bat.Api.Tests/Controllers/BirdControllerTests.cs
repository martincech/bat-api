﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public partial class BirdControllerTests : ControllerTests
    {
        private readonly BirdController controller;
        private const string FULL_EXPAND = "all";//"sex,duration,dateType,scope,curvePoints";

        public BirdControllerTests()
            : base(nameof(DTOs.Bird))
        {
            controller = new BirdController(Connection, BirdModel);
        }

        #region GET

        [Fact]
        public async void GetAllBirds_NoData_WhenNoCompanyAssignedAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetAsync();

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.BadRequest, responseCode);
        }

        [Fact]
        public async void GetAllBirds_NoDataAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Bird>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetAllBirds_ReturnsDataAsync()
        {
            // Arrange
            var companyId = Guid.NewGuid();
            FakeUser(companyId);

            var testData = GetTestBirds();
            Model.Birds.AddRange(testData);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(FULL_EXPAND);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Bird>>(response);
            Assert.Equal(testData.Count, data.Count());
            Assert.True(IsBirdSame(testData, data));
        }

        [Fact]
        public async void GetBirdById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void GetBirdById_ReturnsDataAsync()
        {
            // Arrange
            var expectedBird = GetTestBirds().First();
            var companyId = Guid.NewGuid();
            FakeUser(companyId);
            Model.Birds.Add(expectedBird);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedBird.Id, FULL_EXPAND);

            // Assert
            var data = GetResponseData<DTOs.Bird>(response);
            Assert.True(IsBirdSame(expectedBird, data));
        }

        [Fact]
        public async void GetAllBirdsTemplates_NoDataAsync()
        {
            // Arrange
            // no data

            // Act
            var response = await controller.GetTemplateAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Bird>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetAllBirdsTemplates_ReturnsDataAsync()
        {
            // Arrange
            var testData = GetTestBirds();
            BirdModel.Birds.AddRange(testData);
            BirdModel.SaveChanges();
            var expected = testData.MapTo();
            foreach (var item in expected)
            {
                item.Scope = DTOs.Enums.Scope.Global;
            }

            // Act
            var response = await controller.GetTemplateAsync(FULL_EXPAND);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Bird>>(response);
            Assert.Equal(expected.OrderBy(i => i.Id), data.OrderBy(i => i.Id));
        }


        [Fact]
        public async void GetBirdTemplateById_ReturnsDataAsync()
        {
            // Arrange
            var template = GetTestBirds()[0];
            var userBird = GetTestBirds()[1];
            var expectedTemplate = template.MapTo();
            expectedTemplate.Scope = DTOs.Enums.Scope.Global;

            Model.Birds.Add(userBird);
            Model.SaveChanges();

            BirdModel.Birds.Add(template);
            BirdModel.SaveChanges();

            // Act
            var response = await controller.GetAsync(template.Id, FULL_EXPAND);

            // Assert
            var data = GetResponseData<DTOs.Bird>(response);
            Assert.Equal(expectedTemplate, data);
        }


        [Fact]
        public async void GetBirdTemplateById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        #endregion

        #region POST

        [Theory]
        [MemberData(nameof(GetInvalidDemoBirds))]
        public async void Post_NotValidBirdAsync(DTOs.BirdCreate testBird)
        {
            // Arrange
            //data in argument

            // Act
            var response = await controller.PostAsync(testBird);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.True(IsStatusCode4xx(responseCode));
        }

        public static IEnumerable<object[]> GetInvalidDemoBirds
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new DTOs.Bird { Name = "some extra bird name", Scope = DTOs.Enums.Scope.Global } },  //missing reference on a company (not administrator user cannot creates bird template)
                };
            }
        }

        private static bool IsStatusCode4xx(int code)
        {
            return code >= 400 && code <= 499;
        }

        [Fact]
        public async void Post_CreateBirdAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);

            var modelBird = GetTestBirds().First();
            Model.Birds.Add(modelBird);
            Model.SaveChanges();
            var testBird = modelBird.MapTo();

            // Act
            var response = await controller.PostAsync(testBird);

            // Assert
            var data = GetResponseData<DTOs.Bird>(response);
            testBird.Id = data.Id;
            Assert.True(testBird.Equals(data));
        }


        [Fact]
        public async void Post_CreateBirdTemplateAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            var modelBird = GetTestBirds().First();
            BirdModel.Birds.Add(modelBird);
            BirdModel.SaveChanges();
            var testBird = modelBird.MapTo();

            // Act
            var response = await controller.PostAsync(testBird);

            // Assert
            var data = GetResponseData<DTOs.Bird>(response);
            testBird.Id = data.Id;
            Assert.True(testBird.Equals(data));
        }

        #endregion

        #region PUT

        [Fact]
        public async void Put_BirdNotFoundAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            var bird = GetTestBirds().First();
            BirdModel.Birds.Add(bird);
            BirdModel.SaveChanges();

            // Act
            var response = await controller.PutAsync(new Guid(), bird.MapTo());

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoBirds))]
        public async void Put_NotValidBirdAsync(DTOs.Bird testBird)
        {
            // Arrange
            //No data is needed

            // Act
            var response = await controller.PutAsync(new Guid(), testBird);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.True(IsStatusCode4xx(responseCode));
        }


        [Fact]
        public async void Put_BirdTemplateAsStandardUserAsync()
        {
            // Arrange
            var originBird = GetTestBirds().First();
            BirdModel.Birds.Add(originBird);
            BirdModel.SaveChanges();
            var updateBird = originBird.MapTo();
            UpdateBirdProperties(ref updateBird);
            updateBird.Scope = DTOs.Enums.Scope.Global;

            // Act
            var response = await controller.PutAsync(updateBird.Id, updateBird);

            // Act
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }


        /// <summary>
        /// Insert test bird data to store.
        /// </summary>
        /// <returns></returns>
        private DTOs.Bird InsertBirdInStore()
        {
            var originBird = GetTestBirds().First();
            Model.Birds.Add(originBird);
            Model.SaveChanges();

            return originBird.MapTo();
        }

        /// <summary>
        /// Make a few changes in bird.
        /// </summary>
        /// <param name="updateBird"></param>
        private static void UpdateBirdProperties(ref DTOs.Bird updateBird)
        {
            updateBird.Duration += 7;
            var points = updateBird.CurvePoints.Take(1);
            updateBird.CurvePoints = points;
        }


        [Fact]
        public async void Put_BirdTemplateAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var originBird = GetTestBirds().First();
            BirdModel.Birds.Add(originBird);
            BirdModel.SaveChanges();

            var updateBird = originBird.MapTo();
            UpdateBirdProperties(ref updateBird);
            updateBird.Scope = DTOs.Enums.Scope.Global;

            // Act
            var response = await controller.PutAsync(updateBird.Id, updateBird);

            // Act
            CheckBirdResponse(response, updateBird, true);
        }

        private void CheckBirdResponse(ActionResult<DTOs.ApiResponse> response, DTOs.Bird expectedBird, bool checkTemplate)
        {
            var bird = GetResponseData<DTOs.Bird>(response);
            Assert.True(bird.Equals(expectedBird));
            if (checkTemplate)
            {
                Assert.Single(BirdModel.Products);
                Assert.Single(BirdModel.Companies);
            }
            else
            {   //check user bird
                Assert.Single(Model.BirdProducts);
                Assert.Single(Model.BirdCompanies);
            }
        }

        [Fact]
        public async void Put_BirdModifiedAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var updateBird = InsertBirdInStore();
            UpdateBirdProperties(ref updateBird);

            // Act
            var response = await controller.PutAsync(updateBird.Id, updateBird);

            // Assert
            CheckBirdResponse(response, updateBird, false);
        }

        [Fact]
        public async void Put_BirdModifiedNewProductAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var originBird = GetTestBirds().First();
            Model.Birds.Add(originBird);
            Model.SaveChanges();

            var updateBird = originBird.MapTo();
            updateBird.Product = "800-2018";

            // Act
            var response = await controller.PutAsync(originBird.Id, updateBird);

            // Assert
            CheckBirdResponse(response, updateBird, false);
        }

        [Fact]
        public async void Put_BirdModifiedNewBirdCompanyAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var originBird = GetTestBirds().First();
            Model.Birds.Add(originBird);
            Model.SaveChanges();

            var updateBird = originBird.MapTo();
            updateBird.Company = "Ross";
            updateBird.Product = "new 300";

            // Act
            var response = await controller.PutAsync(originBird.Id, updateBird);

            // Assert
            CheckBirdResponse(response, updateBird, false);
        }

        #endregion

        #region DELETE

        [Fact]
        public async void Delete_BirdNotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);  //use same id for fake company as bird - no problem in this test

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var resultCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, resultCode);
        }


        [Fact]
        public async void Delete_BirdTemplateAsCommonUserAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            var originBird = GetTestBirds().First();
            BirdModel.Birds.Add(originBird);
            BirdModel.SaveChanges();

            // Act
            var response = await controller.DeleteAsync(originBird.Id);

            // Assert
            var resultCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, resultCode);
        }


        [Fact]
        public async void Delete_BirdTemplateAsAdminAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            var originBird = GetTestBirds().First();
            BirdModel.Birds.Add(originBird);
            BirdModel.SaveChanges();

            // Act
            var response = await controller.DeleteAsync(originBird.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(BirdModel.Birds);
        }


        [Fact]
        public async void Delete_BirdDeletedAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);

            var originBird = GetTestBirds().First();
            Model.Birds.Add(originBird);
            Model.SaveChanges();

            // Act
            var response = await controller.DeleteAsync(originBird.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(BirdModel.Birds);
        }

        #endregion
    }
}
