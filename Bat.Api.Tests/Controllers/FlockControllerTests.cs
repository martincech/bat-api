﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.Map;
using Bat.Cloud.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class FlockControllerTests : ControllerTests
    {
        private readonly FlockController controller;
        private const string FULL_EXPAND = "initialAge,initialWeight,targetWeight,targetAge,endDate,bird,house,device";

        public FlockControllerTests()
            : base(nameof(Flock))
        {
            controller = new FlockController(Connection, BirdModel);
        }



        [Fact]
        public async void GetAllFlocks_ReturnsDataAsync()
        {
            // Arrange
            var testData = GetTestFlocks();
            Model.AddRange(testData);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(FULL_EXPAND);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Flock>>(response);
            Assert.Equal(testData.Count, data.Count());
            Assert.True(IsFlockSame(testData, data));
        }


        [Fact]
        public async void GetAllFlocks_NoDataAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Flock>>(response);
            Assert.Empty(data);
        }

        [Fact]
        public async void GetFlockById_ReturnsDataAsync()
        {
            // Arrange
            var expectedFlock = GetTestFlocks().First();
            Model.Add(expectedFlock);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedFlock.Id, FULL_EXPAND);

            // Assert
            var data = GetResponseData<DTOs.Flock>(response);
            Assert.True(IsFlockSame(expectedFlock, data));
        }

        [Fact]
        public async void GetFlockById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoFlocks))]
        public async void Post_NotValidFlockAsync(DTOs.FlockCreate testFlock)
        {
            // Arrange
            //data in argument

            // Act
            var response = await controller.PostAsync(testFlock);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        public static IEnumerable<object[]> GetInvalidDemoFlocks
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new DTOs.Flock { Name = "some extra flock name", TargetAge = 456 } },  //missing reference on house or device
                    new object[] { new DTOs.Flock { Name = "ff", HouseId = Guid.Empty, TargetWeight = 1811 } },    //invalid reference (empty GUID)
                    new object[] { new DTOs.Flock { Name = "ff2", HouseId = Guid.NewGuid() } }    //invalid reference (TargetAge and TargetWeight are null)
                };
            }
        }



        [Fact]
        public async void Post_CreateFlockAsync()
        {
            // Arrange
            var modelFlock = GetTestFlocks().First();
            Model.Add(modelFlock);
            Model.SaveChanges();
            var testFlock = modelFlock.MapTo();

            // Act
            var response = await controller.PostAsync(testFlock);

            // Assert
            var data = GetResponseData<DTOs.Flock>(response);
            Assert.Equal(testFlock.Name, data.Name);
        }


        [Fact]
        public async void Put_FlockNotFoundAsync()
        {
            // Arrange
            var flock = GetTestFlocks().First();
            Model.Houses.Add(flock.House);
            Model.SaveChanges();

            // Act
            var response = await controller.PutAsync(new Guid(), flock.MapTo());

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoFlocks))]
        public async void Put_NotValidFlockAsync(DTOs.Flock testFlock)
        {
            // Arrange
            //No data is needed

            // Act
            var response = await controller.PutAsync(new Guid(), testFlock);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_FlockModifiedAsync()
        {
            // Arrange
            var originFlock = GetTestFlocks().First();
            await Model.Flocks.AddAsync(originFlock);
            await Model.SaveChangesAsync();

            var updateFlock = originFlock.MapTo();
            updateFlock.Name = "UPDATED FLOCK NAME";

            // Act
            var response = await controller.PutAsync(originFlock.Id, updateFlock);

            // Assert
            var data = GetResponseData<DTOs.Flock>(response);
            Assert.True(data.Equals(updateFlock));
        }

        [Fact]
        public async void Delete_FlockNotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Delete_FlockDeletedAsync()
        {
            // Arrange
            var originFlock = GetTestFlocks().First();
            await Model.Flocks.AddAsync(originFlock);
            await Model.SaveChangesAsync();

            // Act
            var response = await controller.DeleteAsync(originFlock.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(Model.Flocks);
        }
    }
}
