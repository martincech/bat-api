﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.Map;
using Bat.Cloud.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class FarmControllerTests : ControllerTests
    {
        private readonly FarmController controller;
        private const string FULL_EXPAND = "address,country,contactName,phone,email";

        public FarmControllerTests()
            : base(nameof(Farm))
        {
            controller = new FarmController(Connection);
        }


        [Fact]
        public async void GetAllFarms_ReturnsDataAsync()
        {
            // Arrange
            var testData = GetTestFarms();
            Model.AddRange(testData);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(FULL_EXPAND);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Farm>>(response);
            Assert.Equal(testData.Count, data.Count());
            Assert.True(IsFarmSame(testData, data));
        }

        [Fact]
        public async void GetAllFarms_NoDataAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetAsync(FULL_EXPAND);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Farm>>(response);
            Assert.Empty(data);
        }

        [Fact]
        public async void GetFarmById_ReturnsDataAsync()
        {
            // Arrange
            var expectedFarm = GetTestFarms().First();
            Model.Add(expectedFarm);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedFarm.Id, FULL_EXPAND);

            // Assert
            var data = GetResponseData<DTOs.Farm>(response);
            Assert.True(IsFarmSame(expectedFarm, data));
        }

        [Fact]
        public async void GetFarmById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Post_NotValidFarmAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.PostAsync(null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Post_CreateFarmAsync()
        {
            // Arrange
            var testFarm = new DTOs.FarmCreate
            {
                Name = "My super test farm"
            };

            // Act
            var response = await controller.PostAsync(testFarm);

            // Assert
            var data = GetResponseData<DTOs.Farm>(response);
            Assert.Equal(testFarm.Name, data.Name);
        }

        [Fact]
        public async void Put_FarmNotFoundAsync()
        {
            // Arrange
            var farm = GetTestFarms().First();

            // Act
            var response = await controller.PutAsync(new Guid(), farm.MapTo());

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Put_NotValidFarmAsync()
        {
            // Arrange
            //No data is needed

            // Act
            var response = await controller.PutAsync(new Guid(), null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_FarmModifiedAsync()
        {
            // Arrange
            var originFarm = GetTestFarms().First();
            await Model.Farms.AddAsync(originFarm);
            await Model.SaveChangesAsync();

            var updateFarm = originFarm.MapTo();
            updateFarm.Name = "UPDATED FARM";

            // Act
            var response = await controller.PutAsync(originFarm.Id, updateFarm);

            // Assert
            var data = GetResponseData<DTOs.Farm>(response);
            Assert.True(data.Equals(updateFarm));
        }

        [Fact]
        public async void Delete_FarmNotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Delete_FarmDeletedAsync()
        {
            // Arrange
            var originFarm = GetTestFarms().First();
            await Model.Farms.AddAsync(originFarm);
            await Model.SaveChangesAsync();

            // Act
            var response = await controller.DeleteAsync(originFarm.Id);

            // Assert
            AssertOkResponse(response);
        }
    }
}
