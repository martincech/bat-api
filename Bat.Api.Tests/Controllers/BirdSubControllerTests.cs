﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Map;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public partial class BirdControllerTests : ControllerTests
    {
        #region Bird companies

        [Fact]
        public async void GetBirdCompanies_NoData_WhenNoCompanyAssignedAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetBirdCompaniesAsync();

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.BadRequest, responseCode);
        }

        [Fact]
        public async void GetBirdCompanies_NoDataAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetBirdCompaniesAsync();

            // Assert
            var data = GetResponseData<IEnumerable<string>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetBirdCompanies_ReturnsDataAsync()
        {
            // Arrange
            var companyId = Guid.NewGuid();
            FakeUser(companyId);
            var companies = CreateStructuredBirdCompaniesData();

            // Act
            var response = await controller.GetBirdCompaniesAsync();

            // Assert
            var data = GetResponseData<IEnumerable<string>>(response);
            Assert.Equal(companies.OrderBy(o => o), data.OrderBy(o => o));
        }

        private IEnumerable<string> CreateStructuredBirdCompaniesData()
        {
            var privateBirdsCompanies = new List<string>
            {
                "Cobb",
                "Ross"
            };
            var company1 = new Bird.Model.Models.Company
            {
                Id = Guid.NewGuid(),
                Name = privateBirdsCompanies[0]
            };
            var company2 = new Bird.Model.Models.Company
            {
                Id = Guid.NewGuid(),
                Name = privateBirdsCompanies[1]
            };
            var companyDummy = new Bird.Model.Models.Company
            {
                Id = Guid.NewGuid(),
                Name = "Dummy"
            };
            var birdsUserData = new List<Bird.Model.Models.Bird>();
            var birdsTemplateData = new List<Bird.Model.Models.Bird>();

            //bird Cobb - private
            var b1 = GetTestBird();
            b1.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "500",
                Company = company1
            };
            birdsUserData.Add(b1);

            //bird Cobb - global
            var b2 = GetTestBird();
            b2.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "200",
                Company = new Bird.Model.Models.Company
                {
                    Id = Guid.NewGuid(),
                    Name = privateBirdsCompanies[0]
                }
            };
            birdsTemplateData.Add(b2);


            //bird Ross - private
            var b3 = GetTestBird();
            b3.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "300",
                Company = company2
            };
            birdsUserData.Add(b3);

            //bird Ross - private
            var b4 = GetTestBird();
            b4.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "200",
                Company = company2
            };
            birdsUserData.Add(b4);

            //bird Dummy - global
            var b5 = GetTestBird();
            b5.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "XXX",
                Company = companyDummy
            };
            birdsTemplateData.Add(b5);


            BirdModel.Birds.AddRange(birdsTemplateData);
            BirdModel.SaveChanges();
            Model.Birds.AddRange(birdsUserData);
            Model.SaveChanges();
            return privateBirdsCompanies;
        }

        #endregion

        #region Company products

        [Fact]
        public async void GetBirdProductsByCompanyId_NoDataAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);

            // Act
            var response = await controller.GetBirdCompanyProductsAsync("NotExistCompany");

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Theory]
        [MemberData(nameof(GetInvalidDemoBirdCompanies))]
        public async void GetBirdProductsByCompanyId_NotFound_WhenNoCompanyAssignedAsync(string companyName)
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetBirdCompanyProductsAsync(companyName);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.True(IsStatusCode4xx(responseCode));
        }

        public static IEnumerable<object[]> GetInvalidDemoBirdCompanies
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { "" },
                    new object[] { "TestCompany" }  //not exist company
                };
            }
        }


        [Fact]
        public async void GetBirdProductsByCompanyId_ReturnsDataAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);
            const string birdCompanyName = "Bird Company QSC";
            var expectedProducts = CreateStructuredBirdProductsData(birdCompanyName);

            // Act
            var response = await controller.GetBirdCompanyProductsAsync(birdCompanyName);

            // Assert
            var data = GetResponseData<IEnumerable<string>>(response);
            Assert.Equal(expectedProducts.OrderBy(o => o), data.OrderBy(o => o));
        }


        private IEnumerable<string> CreateStructuredBirdProductsData(string birdCompanyName)
        {
            var products = new List<string>
            {
                "a a a",
                "b b b"
            };
            var company = GetBirdCompany(birdCompanyName);

            var birdsUserData = new List<Bird.Model.Models.Bird>();
            var birdsTemplateData = new List<Bird.Model.Models.Bird>();

            //private bird (product)
            var b1 = GetTestBird();
            b1.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = products[0],
                Company = company
            };
            birdsUserData.Add(b1);

            //bird-global
            var b2 = GetTestBird();
            b2.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "444",
                Company = GetBirdCompany(birdCompanyName)
            };
            birdsTemplateData.Add(b2);


            //bird - private
            var b3 = GetTestBird();
            b3.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = products[1],
                Company = company
            };
            birdsUserData.Add(b3);

            BirdModel.Birds.AddRange(birdsTemplateData);
            BirdModel.SaveChanges();
            Model.Birds.AddRange(birdsUserData);
            Model.SaveChanges();
            return products;
        }

        private static Bird.Model.Models.Company GetBirdCompany(string name)
        {
            return new Bird.Model.Models.Company
            {
                Id = Guid.NewGuid(),
                Name = name
            };
        }

        #endregion

        #region Product Birds

        [Fact]
        public async void GetBirdsByProductId_NoDataAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);

            // Act
            var response = await controller.GetBirdsByProductAsync("NotExistCompany");

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoBirdCompanies))]
        public async void GetBirdsByProductId_NotFound_WhenNoCompanyAssignedAsync(string companyName)
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetBirdsByProductAsync(companyName);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.True(IsStatusCode4xx(responseCode));
        }


        [Fact]
        public async void GetBirdsByProductId_ReturnsDataAsync()
        {
            // Arrange
            var id = Guid.NewGuid();
            FakeUser(id);
            const string birdProductName = "Bird PRODUCT";
            var expectedProducts = CreateStructuredBirdsData(birdProductName);

            // Act
            var response = await controller.GetBirdsByProductAsync(birdProductName);

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Bird>>(response);
            Assert.Equal(expectedProducts.OrderBy(o => o.Id), data.OrderBy(o => o.Id));
        }

        private IEnumerable<DTOs.Bird> CreateStructuredBirdsData(string birdProductName)
        {
            var product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = birdProductName,
                Company = new Bird.Model.Models.Company
                {
                    Id = Guid.NewGuid(),
                    Name = "company COMP"
                }
            };
            var birdsUserData = new List<Bird.Model.Models.Bird>();
            var birdsTemplateData = new List<Bird.Model.Models.Bird>();

            //private bird
            var b1 = GetTestBird();
            b1.Name = "layers";
            b1.Product = product;
            birdsUserData.Add(b1);

            //bird-global
            var b2 = GetTestBird();
            b2.Product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = birdProductName,
                Company = new Bird.Model.Models.Company
                {
                    Id = Guid.NewGuid(),
                    Name = "company COMP"
                }
            };
            birdsTemplateData.Add(b2);

            //bird - private
            var b3 = GetTestBird();
            b3.Name = "parents";
            b3.Product = product;
            birdsUserData.Add(b3);

            BirdModel.Birds.AddRange(birdsTemplateData);
            BirdModel.SaveChanges();
            Model.Birds.AddRange(birdsUserData);
            Model.SaveChanges();
            return birdsUserData.MapTo(null, false);    //map in minimal state (without expand properties)
        }

        #endregion

        #region Private helpers

        private static Bird.Model.Models.Bird GetTestBird()
        {
            var bird = new Bird.Model.Models.Bird
            {
                Id = Guid.NewGuid(),
                Name = "broilers",
                Product = null,
                Sex = Bird.Model.Enums.Sex.Female,
                Duration = 37
            };

            bird.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 1, Weight = 35 });
            bird.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 7, Weight = 241 });
            bird.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 14, Weight = 571 });
            bird.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 21, Weight = 1047 });
            return bird;
        }

        #endregion
    }
}
