﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.DTOs;
using Bat.Api.DTOs.Enums;
using Bat.Api.Map;
using Moq;
using Veit.Kafka.Topics;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class DeviceControllerTests : ControllerTests
    {
        private readonly DeviceController controller;
        private const string FULL_EXPAND = "description,farm,house,parent";
        private readonly Dictionary<string, string> fullExpandDict;

        public DeviceControllerTests()
            : base(nameof(Device))
        {
            controller = new DeviceController(Connection);
            fullExpandDict = FULL_EXPAND.Split(",")
                .Select(s => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s))
                .ToDictionary(k => k, v => v);
        }


        [Fact]
        public async void GetAllDevices_NoDataAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<Device>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetAllDevices_ReturnsDataAsync()
        {
            // Arrange
            var testData = GetTestDevices();
            Model.Devices.AddRange(testData);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(FULL_EXPAND);
            // Assert
            var data = GetResponseData<IEnumerable<Device>>(response);
            Assert.Equal(testData.Count, data.Count());
            Assert.True(IsDeviceSame(testData.MapTo(fullExpandDict), data));
        }


        [Fact]
        public async void GetDeviceById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void GetDeviceById_ReturnsDataAsync()
        {
            // Arrange
            var expectedDevice = GetTestDevices().First();
            Model.Add(expectedDevice);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedDevice.Id, FULL_EXPAND);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(IsDeviceSame(expectedDevice.MapTo(fullExpandDict), data));
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoCreateDevices))]
        public async void Post_NotValidDeviceAsync(DeviceCreate testDevice)
        {
            // Arrange
            //data in argument

            // Act
            var response = await controller.PostAsync(testDevice);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        public static IEnumerable<object[]> GetInvalidDemoDevices
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new Device { Name = "Invalid test device" } }    //invalid type (default value 0 is undefined)
                };
            }
        }

        public static IEnumerable<object[]> GetInvalidDemoCreateDevices
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new DeviceCreate { Name = "Invalid device" } }
                };
            }
        }


        [Fact]
        public async void Post_CreateDeviceAsync()
        {
            // Arrange
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            Model.SaveChanges();
            var expectedDevice = new DeviceCreate
            {
                Name = "New device #A",
                Type = DeviceType.Bat2CableScale,
                FarmId = testFarm.Id
            };

            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(expectedDevice.Equals(data));
            MoqKafka.Verify(v => v.Send(It.IsAny<List<DeviceMap>>()), Times.Never);
        }


        [Fact]
        public async void Post_CreateGsmDeviceAsync()
        {
            // Arrange
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            Model.SaveChanges();
            var expectedDevice = new DeviceCreate
            {
                Name = "New device #A",
                Type = DeviceType.Bat2GsmScale,
                FarmId = testFarm.Id,
                PhoneNumber = "+123456789",
                Unit = DeviceUnit.Kg,
            };

            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(expectedDevice.Equals(data));
            MoqKafka.Verify(v => v.Send(It.Is<List<DeviceMap>>(e => e.First().Uid == expectedDevice.PhoneNumber)), Times.Once);
        }


        [Fact]
        public async void Post_CreateDeviceWithCustomIdAsync()
        {
            // Arrange
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            Model.SaveChanges();
            var expectedDevice = new DeviceCreate
            {
                Id = Guid.NewGuid(),
                Name = "New device with predefined ID",
                Type = DeviceType.BatCollectorTerminal,
                FarmId = testFarm.Id
            };

            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(expectedDevice.Equals(data));
            Assert.Equal(expectedDevice.Id.Value, data.Id);
        }

        [Fact]
        public async void Post_CreateDeviceWithCustomId_AlreadyExistAsync()
        {
            // Arrange
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            var id = Guid.NewGuid();
            var testDevice = new DeviceCreate
            {
                Id = id,
                Name = "Duplicity device",
                Type = DeviceType.BatCollectorTerminal,
                FarmId = testFarm.Id
            };
            Model.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = id,
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal
            });
            Model.SaveChanges();

            // Act
            var response = await controller.PostAsync(testDevice);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Post_CreateGsmDeviceWith_PhoneNumberAlreadyExistAsync()
        {
            // Arrange
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            const string phoneNumber = "+123456789";
            var testDevice = new DeviceCreate
            {
                Name = "GSM Device",
                Type = DeviceType.Bat2GsmScale,
                FarmId = testFarm.Id,
                PhoneNumber = phoneNumber,
                Unit = DeviceUnit.Lb,
            };
            Model.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2GsmScale,
                PhoneNumber = phoneNumber,
            });
            Model.SaveChanges();

            // Act
            var response = await controller.PostAsync(testDevice);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
            MoqKafka.Verify(v => v.Send(It.IsAny<List<DeviceMap>>()), Times.Never);
        }


        [Fact]
        public async void Post_ScaleTakePlacementFromParent_NoPlacementAsync()
        {
            // Arrange
            var (parent, expectedDevice) = GetDemoDeviceWithParent();
            Model.Devices.Add(parent);
            Model.SaveChanges();

            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(expectedDevice.Equals(data));
            CheckDevicePlacementWithHisParent(parent.MapTo(), data);
        }

        [Fact]
        public async void Post_ScaleTakePlacementFromParent_FarmReferenceAsync()
        {
            // Arrange
            var (parent, expectedDevice) = GetDemoDeviceWithParent();
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            parent.FarmId = testFarm.Id;
            Model.Devices.Add(parent);
            Model.SaveChanges();

            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            CheckDevicePlacementWithHisParent(parent.MapTo(), data);
        }

        [Fact]
        public async void Post_ScaleTakePlacementFromParent_HouseReferenceAsync()
        {
            // Arrange
            var (parent, expectedDevice) = GetDemoDeviceWithParent();
            var testHouse = GetTestHouses().First();
            Model.Houses.Add(testHouse);
            parent.HouseId = testHouse.Id;
            Model.Devices.Add(parent);
            Model.SaveChanges();
            parent = Model.DevicesLazy.FirstOrDefault(f => f.Id == parent.Id);
            // Act
            var response = await controller.PostAsync(expectedDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            CheckDevicePlacementWithHisParent(parent.MapTo(), data);
        }

        private static (Cloud.Model.Models.Device parent, DeviceCreate device) GetDemoDeviceWithParent()
        {
            var parent = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Name = "parent 1",
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal,
                ParentId = null
            };

            var expectedDevice = new DeviceCreate
            {
                Name = "Scale A",
                Type = DeviceType.Bat2CableScale,
                ParentId = parent.Id,
            };
            return (parent, expectedDevice);
        }

        private static void CheckDevicePlacementWithHisParent(Device parent, Device data)
        {
            Assert.Equal(parent.Id, data.ParentId);
            Assert.Equal(parent.FarmId, data.FarmId);
            Assert.Equal(parent.HouseId, data.HouseId);
        }


        [Fact]
        public async void Put_ParentPlacementUpdatesAllChildren_HouseReferenceAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testHouse = GetTestHouses().First();
            Model.Houses.Add(testHouse);
            parent.HouseId = testHouse.Id;
            var parentDto = parent.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();

            // Act
            var response = await controller.PutAsync(parent.Id, parentDto);

            // Assert
            AssertOkResponse(response);
            CheckParentChildrenReferences(parent);
        }

        [Fact]
        public async void Put_ParentPlacementUpdatesAllChildren_FarmReferenceAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            parent.FarmId = testFarm.Id;
            var parentDto = parent.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();

            // Act
            var response = await controller.PutAsync(parent.Id, parentDto);

            // Assert
            AssertOkResponse(response);
            CheckParentChildrenReferences(parent);
        }

        /// <summary>
        /// Children scales have any references, some has ref on farm, other on house,
        /// but when you update terminal all scales takes his references.
        /// </summary>
        [Fact]
        public async void Put_ParentPlacementUpdatesAllChildren_ChildrenHasAnyReferenceAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            parent.FarmId = testFarm.Id;
            var parentDto = parent.MapTo();

            //set to scales some references
            var testFarm2 = GetTestFarm();
            Model.Farms.Add(testFarm2);
            devices.ElementAt(0).FarmId = testFarm2.Id;

            var testHouse = GetTestHouses().First();
            Model.Houses.Add(testHouse);
            devices.ElementAt(1).HouseId = testHouse.Id;

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();

            // Act
            var response = await controller.PutAsync(parent.Id, parentDto);

            // Assert
            AssertOkResponse(response);
            CheckParentChildrenReferences(parent);
        }

        private void CheckParentChildrenReferences(Cloud.Model.Models.Device parent)
        {
            var modelDevices = Model.DevicesLazy.Where(w => w.ParentId == parent.Id);
            foreach (var device in modelDevices)
            {
                Assert.Equal(parent.FarmId, device.FarmId);
                Assert.Equal(parent.HouseId, device.HouseId);
            }
        }

        private static (Cloud.Model.Models.Device parent, IEnumerable<Cloud.Model.Models.Device> scales) GetDemoDevicesWithParent()
        {
            var parentDevice = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Name = "Terminal - parent",
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal
            };
            var devices = new List<Cloud.Model.Models.Device>
            {
                new Cloud.Model.Models.Device
                {
                    Name = "scale A",
                    Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                    ParentId = parentDevice.Id
                },
                new Cloud.Model.Models.Device
                {
                    Name = "scale B",
                    Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                    ParentId = parentDevice.Id
                },
                new Cloud.Model.Models.Device
                {
                    Name = "scale C",
                    Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                    ParentId = parentDevice.Id
                },
            };
            return (parentDevice, devices);
        }


        [Fact]
        public async void Put_ScalePlacementDifferentFromParent_DeleteAllReferencesAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            parent.FarmId = testFarm.Id;

            //set to scales some references
            var expectedScale = devices.FirstOrDefault();
            expectedScale.FarmId = null;    //forbidden operation (scale must have the same reference as parent)
            expectedScale.HouseId = null;
            var expectedScaleDto = expectedScale.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();
            expectedScaleDto.Id = expectedScale.Id;

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_ScalePlacementDifferentFromParent_DifferentFarmAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testFarm = GetTestFarm();
            Model.Farms.Add(testFarm);
            parent.FarmId = testFarm.Id;

            //set to scales some references
            var expectedScale = devices.FirstOrDefault();
            var testFarm2 = GetTestFarm();
            Model.Farms.Add(testFarm2);
            expectedScale.FarmId = testFarm2.Id;    //forbidden
            expectedScale.HouseId = null;
            var expectedScaleDto = expectedScale.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();
            expectedScaleDto.Id = expectedScale.Id;

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }


        [Fact]
        public async void Put_ScalePlacementDifferentFromParent_HouseFromDifferentFarmAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testHouse = GetTestHouses().First();
            Model.Houses.Add(testHouse);
            parent.HouseId = testHouse.Id;

            //set to scales some references
            var expectedScale = devices.FirstOrDefault();
            var testHouse2 = GetTestHouses().First();
            Model.Houses.Add(testHouse2);
            expectedScale.HouseId = testHouse2.Id;  //forbidden
            var expectedScaleDto = expectedScale.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();
            expectedScaleDto.Id = expectedScale.Id;

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_ScaleHouseFromSameFarmAsParentAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var testHouse = GetTestHouses().First();
            Model.Houses.Add(testHouse);
            parent.FarmId = testHouse.FarmId;

            var expectedScale = devices.FirstOrDefault();
            expectedScale.FarmId = null;
            expectedScale.HouseId = testHouse.Id;  //valid - house from the same farm as it has parent
            var expectedScaleDto = expectedScale.MapTo();

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();
            expectedScaleDto.Id = expectedScale.Id;

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.Equal(expectedScale.HouseId, data.HouseId);
        }

        /// <summary>
        /// If BAT2 cable scale has assigned a parent, you won't remove the reference
        /// on this parent. Parent is permanent link.
        /// </summary>
        [Fact]
        public async void Put_Bat2CableScale_RemoveParentAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();

            var expectedScale = devices.FirstOrDefault();
            var expectedScaleDto = expectedScale.MapTo();
            expectedScaleDto.ParentId = null; //forbidden - cannot remove parent

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }


        [Fact]
        public async void Put_Bat2CableScale_ChangeParentAsync()
        {
            // Arrange
            var (parent, devices) = GetDemoDevicesWithParent();
            var parent2 = devices.ElementAt(1);
            parent2.Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal;
            parent2.Name = "Terminal 2";
            parent2.Parent = null;
            parent2.ParentId = null;

            Model.Devices.Add(parent);
            Model.Devices.AddRange(devices);
            Model.SaveChanges();

            var expectedScale = devices.FirstOrDefault();
            var expectedScaleDto = expectedScale.MapTo();
            expectedScaleDto.ParentId = parent2.Id; //forbidden - cannot change parent

            // Act
            var response = await controller.PutAsync(expectedScaleDto.Id, expectedScaleDto);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }


        [Fact]
        public async void Put_DeviceNotFoundAsync()
        {
            // Arrange
            var device = GetTestDevices().First();

            // Act
            var response = await controller.PutAsync(device.Id, device.MapTo());

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoDevices))]
        public async void Put_NotValidDeviceAsync(Device testDevice)
        {
            // Arrange
            //data in argument

            // Act
            var response = await controller.PutAsync(new Guid(), testDevice);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Put_DeviceModifiedAsync()
        {
            // Arrange
            var originDevice = GetTestDevices().First();
            await Model.Devices.AddAsync(originDevice);
            await Model.SaveChangesAsync();
            var parent = Model.Devices.FirstOrDefault(f => f.Id == originDevice.ParentId);
            var updateDevice = originDevice.MapTo(null, parent);
            updateDevice.Name = "UPDATED Device 6663231";

            // Act
            var response = await controller.PutAsync(originDevice.Id, updateDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(data.Equals(updateDevice));
            MoqKafka.Verify(v => v.Send(It.IsAny<List<DeviceMap>>()), Times.Never);
        }

        [Fact]
        public async void Put_GsmDevice_PhoneNumberModifiedAsync()
        {
            // Arrange
            var originDevice = GetTestDevices().First();
            originDevice.Type = Cloud.Model.Enums.DeviceType.Bat2GsmScale;
            originDevice.PhoneNumber = "+123456789";
            await Model.Devices.AddAsync(originDevice);
            await Model.SaveChangesAsync();
            var parent = Model.Devices.FirstOrDefault(f => f.Id == originDevice.ParentId);
            var updateDevice = originDevice.MapTo(null, parent);
            updateDevice.Name = "UPDATED Device 6663231";
            originDevice.PhoneNumber = "+1234567899";

            // Act
            var response = await controller.PutAsync(originDevice.Id, updateDevice);

            // Assert
            var data = GetResponseData<Device>(response);
            Assert.True(data.Equals(updateDevice));
            MoqKafka.Verify(v => v.Send(It.IsAny<List<DeviceMap>>()), Times.Exactly(2));
        }


        [Fact]
        public async void Delete_DeviceNotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void Delete_DeviceDeletedAsync()
        {
            // Arrange
            var originDevice = GetTestDevices().First();
            await Model.Devices.AddAsync(originDevice);
            await Model.SaveChangesAsync();

            // Act
            var response = await controller.DeleteAsync(originDevice.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(Model.Devices);
            MoqKafka.Verify(v => v.Send(It.IsAny<List<DeviceMap>>()), Times.Never);
        }

        [Fact]
        public async void Delete_GsmDeviceDeletedAsync()
        {
            // Arrange
            var originDevice = GetTestDevices().First();
            originDevice.Type = Cloud.Model.Enums.DeviceType.Bat2GsmScale;
            originDevice.PhoneNumber = "+123456789";
            await Model.Devices.AddAsync(originDevice);
            await Model.SaveChangesAsync();

            // Act
            var response = await controller.DeleteAsync(originDevice.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(Model.Devices);
            MoqKafka.Verify(v => v.Send(It.Is<List<DeviceMap>>(i => i.First().Uid == originDevice.PhoneNumber && i.First().Id == null)), Times.Once);
        }
    }
}
