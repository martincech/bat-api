﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Moq;
using User.Model.Enums;
using User.Model.Models;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class UserControllerTests : ControllerTests
    {
        private readonly UserController controller;
        private readonly Role veitOperatorRole;
        private readonly Role companyAdminRole;

        public UserControllerTests()
            : base(nameof(User))
        {
            controller = new UserController(Connection, MoqUserContext.Object, MoqContainerProvider);

            //create default roles
            veitOperatorRole = new Role
            {
                Id = Guid.NewGuid(),
                Name = DbRole.VeitOperator.ToString()
            };
            UserModel.Roles.Add(veitOperatorRole);
            companyAdminRole = new Role
            {
                Id = Guid.NewGuid(),
                Name = DbRole.CompanyAdmin.ToString()
            };
            UserModel.Roles.Add(companyAdminRole);
            UserModel.SaveChanges();
        }


        [Fact]
        public async void Get_AllUsersAsync()
        {
            // Arrange
            var users = GetDemoUsers();
            InsertDemoUsersToDb(users);
            var user = users.First(f => f.Role != DTOs.Enums.Role.VeitOperator);
            FakeUserIdentity(user.Id);
            var expectedData = users.Where(w => w.Role != DTOs.Enums.Role.VeitOperator && w.CompanyId == user.CompanyId);

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.User>>(response);
            Assert.True(AssertCollection(expectedData, data));
        }


        [Fact]
        public async void Get_AllUsers_NoDataAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.User>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void Get_AllUsers_ValidDataAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var expectedData = GetDemoUsers();
            InsertDemoUsersToDb(expectedData);

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.User>>(response);
            Assert.True(AssertCollection(expectedData, data));
        }


        [Fact]
        public async void Get_UserByIdAsync()
        {
            // Arrange
            var users = GetDemoUsers();
            InsertDemoUsersToDb(users);
            var expectedUser = users.First(f => f.Role != DTOs.Enums.Role.VeitOperator);
            FakeUser(expectedUser.CompanyId.Value);

            // Act
            var response = await controller.GetAsync(expectedUser.Id);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.True(expectedUser.Equals(data));
        }

        [Fact]
        public async void Get_UserById_NotFoundAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Get_UserById_AsPrivilegedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var expectedData = GetDemoUsers();
            InsertDemoUsersToDb(expectedData);
            var expectedUser = expectedData.First();

            // Act
            var response = await controller.GetAsync(expectedUser.Id);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.True(expectedUser.Equals(data));
        }


        [Fact]
        public async void Post_CreateUserAsync()
        {
            // Arrange
            var expectedData = GetDemoUserForCreate();

            // Act
            var response = await controller.PostAsync(expectedData);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.Equal(expectedData.Name, data.Name);
        }


        [Fact]
        public async void Post_CreateUser_NotAllowedUserRoleAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var expectedData = GetDemoUserForCreate();
            //Try create super administrator
            expectedData.Role = DTOs.Enums.Role.VeitOperator;

            // Act
            var response = await controller.PostAsync(expectedData);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void Post_CreateCompanyAdmin_NotAllowedUserRoleAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var expectedData = GetDemoUserForCreate();
            //Try create super administrator
            expectedData.Role = DTOs.Enums.Role.CompanyAdmin;

            // Act
            var response = await controller.PostAsync(expectedData);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }


        [Fact]
        public async void Post_NotValidUserAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            // Act
            var response = await controller.PostAsync(null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Post_CreateUser_AsPrivilegedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var testUser = GetDemoUserForCreate();

            // Act
            var response = await controller.PostAsync(testUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.Equal(testUser.Name, data.Name);
        }

        [Fact]
        public async void Post_CreateCompanyAdmin_AsPrivilegedAsync()
        {
            // Arrange
            var companyId = new Guid();
            FakeUserAsComapnyAdmin(companyId);
            var testUser = GetDemoUserForCreate();
            testUser.Role = DTOs.Enums.Role.CompanyAdmin;

            // Act
            var response = await controller.PostAsync(testUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.Equal(testUser.Name, data.Name);
            Assert.Equal(testUser.CompanyId, data.CompanyId);
        }


        [Fact]
        public async void Put_UserModifiedAsync()
        {
            var users = GetDemoUsers();
            InsertDemoUsersToDb(users);

            var updateUser = users.FirstOrDefault();
            var dbUser = updateUser.MapFrom(new List<Role>());
            var originUser = dbUser.MapTo();

            FakeUser(updateUser.CompanyId.Value);

            updateUser.Name = "UPDATED user name";
            FakeCognitoUser(new Veit.Cognito.User.Model.CognitoUser { Id = dbUser.Id, Name = "" });

            // Act
            var response = await controller.PutAsync(originUser.Id, updateUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.True(data.Equals(updateUser));
        }

        [Fact]
        public async void Put_CompanyAdminUserNotModified_Async()
        {
            var users = GetDemoUsers();
            InsertDemoUsersToDb(users);

            var updateUser = users.FirstOrDefault();
            var dbUser = updateUser.MapFrom(new List<Role>());
            var originUser = dbUser.MapTo();

            FakeUser(updateUser.CompanyId.Value);

            updateUser.Name = "UPDATED user name";
            updateUser.Role = DTOs.Enums.Role.CompanyAdmin;
            FakeCognitoUser(new Veit.Cognito.User.Model.CognitoUser { Id = dbUser.Id, Name = "" });

            // Act
            var response = await controller.PutAsync(originUser.Id, updateUser);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void Put_NotValidUserAsync()
        {
            // Arrange
            //No data is needed
            FakeUserAsAdministrator();

            // Act
            var response = await controller.PutAsync(new Guid(), null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }


        [Fact]
        public async void Put_UserNotFoundAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var user = GetDemoUsers().First();
            user.Name = "my test user A";

            // Act
            var response = await controller.PutAsync(new Guid(), user);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void Put_UserModified_AsPrivilegedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var originUser = GetDemoUsers().First();
            var role = new Role
            {
                Name = DbRole.VeitOperator.ToString()
            };
            UserModel.Roles.Add(role);
            var dbUser = originUser.MapFrom(new List<Role> { role });
            UserModel.Users.Add(dbUser);
            UserModel.SaveChanges();

            var updateUser = dbUser.MapTo();
            updateUser.Name = "UPDATED user name";
            FakeCognitoUser(new Veit.Cognito.User.Model.CognitoUser { Id = dbUser.Id, Name = "" });

            // Act
            var response = await controller.PutAsync(originUser.Id, updateUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.True(data.Equals(updateUser));
        }

        [Fact]
        public async void Put_DoNotModifyUserFromDifferentCompanyAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());

            var users = GetDemoUsers();
            InsertDemoUsersToDb(users);

            var updateUser = users.FirstOrDefault();
            updateUser.Name = "testx";
            // Act
            var response = await controller.PutAsync(updateUser.Id, updateUser);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void Put_UserNameNotSetAsync()
        {
            var demoUser = GetDemoUsers().First();
            demoUser.Name = "user 01";
            var role = new Role
            {
                Name = DbRole.VeitOperator.ToString()
            };
            UserModel.Users.Add(demoUser.MapFrom(new List<Role> { role }));
            UserModel.SaveChanges();
            demoUser.Name = null;

            // Act
            var response = await controller.PutAsync(demoUser.Id, demoUser);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_CommonUserModifiedYourselfNameAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var dbUser = UserModel.Users.FirstOrDefault();

            var updateUser = dbUser.MapTo();
            updateUser.Name = "UPDATED user name";
            FakeCognitoUser(new Veit.Cognito.User.Model.CognitoUser { Id = dbUser.Id, Name = "" });

            // Act
            var response = await controller.PutAsync(dbUser.Id, updateUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.True(data.Equals(updateUser));
        }

        [Fact]
        public async void Put_CommonUserModifiedYourselfCompanyAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var dbUser = UserModel.Users.FirstOrDefault();

            var updateUser = dbUser.MapTo();
            updateUser.Name = "name";
            updateUser.CompanyId = Guid.NewGuid();
            FakeCognitoUser(new Veit.Cognito.User.Model.CognitoUser { Id = dbUser.Id, Name = "" });

            // Act
            var response = await controller.PutAsync(dbUser.Id, updateUser);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        private void FakeCognitoUser(Veit.Cognito.User.Model.CognitoUser user)
        {
            MoqUserContext.Setup(x => x.GetCognitoUserAsync(It.IsAny<Guid>())).ReturnsAsync(user);
        }

        [Fact]
        public async void Delete_UserAsUserAsync()
        {
            // Arrange
            var data = GetDemoUsers();
            var expectedData = data.First();
            InsertDemoUsersToDb(data);
            FakeUser(expectedData.CompanyId.Value);

            // Act
            var response = await controller.DeleteAsync(expectedData.Id);

            // Assert
            AssertOkResponse(response);
        }

        [Fact]
        public async void Delete_UserHimselfAsync()
        {
            // Arrange
            FakeUser(Guid.NewGuid());
            var expectedData = Connection.UserModel.Users.First();

            // Act
            var response = await controller.DeleteAsync(expectedData.Id);

            // Assert
            AssertOkResponse(response);
        }

        [Fact]
        public async void Delete_UserNotFoundAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void Delete_User_AsPrivilegedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var demoData = GetDemoUsers();
            InsertDemoUsersToDb(demoData);
            var admin = demoData.FirstOrDefault(f => f.Role == DTOs.Enums.Role.VeitOperator);

            // Act
            var response = await controller.DeleteAsync(admin.Id);

            // Assert
            // privileged user cannot be deleted
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void Delete_CompanyAdminUserAsync()
        {
            // Arrange
            var demoData = GetDemoUsers();
            InsertDemoUsersToDb(demoData);
            var admin = demoData.FirstOrDefault(f => f.Role == DTOs.Enums.Role.CompanyAdmin);
            FakeUser(admin.CompanyId.Value);

            // Act
            var response = await controller.DeleteAsync(admin.Id);

            // Assert
            // company admin cannot be deleted by user
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void Delete_CompanyAdminUser_AsCompanyAdminAsync()
        {
            // Arrange
            var demoData = GetDemoUsers();
            InsertDemoUsersToDb(demoData);
            var admin = demoData.FirstOrDefault(f => f.Role == DTOs.Enums.Role.CompanyAdmin);
            FakeUserAsComapnyAdmin(admin.CompanyId.Value);

            // Act
            var response = await controller.DeleteAsync(admin.Id);

            // Assert
            AssertOkResponse(response);
        }

        [Fact]
        public async void Delete_CompanyAdminUser_AsPrivilegedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var demoData = GetDemoUsers();
            InsertDemoUsersToDb(demoData);
            var admin = demoData.FirstOrDefault(f => f.Role == DTOs.Enums.Role.CompanyAdmin);

            // Act
            var response = await controller.DeleteAsync(admin.Id);

            // Assert
            AssertOkResponse(response);
        }


        [Fact]
        public async void Delete_UserDeletedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var demoData = GetDemoUsers();
            var commonUser = demoData.FirstOrDefault(f => f.Role == DTOs.Enums.Role.Undefined);
            InsertDemoUsersToDb(new List<DTOs.User> { commonUser });

            // Act
            var response = await controller.DeleteAsync(commonUser.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(UserModel.Users);
        }


        [Fact]
        public async void Post_RegisterAlreadyExistedUserAsync()
        {
            // Arrange
            var testUser = GetDemoRegisterUser();
            MoqUserContext.Setup(x => x.ExistUserAsync(It.Is<string>(g => g == testUser.Email))).ReturnsAsync(true);

            // Act
            var response = await controller.PostRegistrationAsync(testUser);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Post_RegisterUserOkAsync()
        {
            // Arrange
            var testUser = GetDemoRegisterUser();
            MoqUserContext.Setup(x => x.ExistUserAsync(It.IsAny<string>())).ReturnsAsync(false);
            MoqUserContext.Setup(x => x.ResetUserPasswordAsync(It.IsAny<Guid>())).ReturnsAsync(true);

            // Act
            var response = await controller.PostRegistrationAsync(testUser);

            // Assert
            var data = GetResponseData<DTOs.User>(response);
            Assert.Equal(testUser.Email, data.Email);
            Assert.Equal(testUser.Name, data.Name);
            Assert.Equal(testUser.Company, data.Company.Name);

            Assert.NotNull(UserModel.Companies.FirstOrDefault(f => f.Name == testUser.Company));
            Assert.NotNull(UserModel.Users.FirstOrDefault(f => f.Id == data.Id));
        }



        #region Private helpers

        private static UserRegister GetDemoRegisterUser()
        {
            return new UserRegister
            {
                Email = "user@user.cz",
                Name = "userByUser",
                Company = "user company test"
            };
        }

        private static IEnumerable<DTOs.User> GetDemoUsers()
        {
            var companyId = Guid.NewGuid();
            return new List<DTOs.User>
            {
                new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.Undefined,
                    CompanyId = companyId,
                },
                new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.Undefined,
                    CompanyId = companyId,
                },
                new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.Undefined,
                    CompanyId = Guid.NewGuid(),
                },
                new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.VeitOperator,
                    CompanyId = companyId,
                },
                 new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.VeitOperator,
                    CompanyId = companyId,
                },
                new DTOs.User
                {
                    Id = Guid.NewGuid(),
                    Role = DTOs.Enums.Role.CompanyAdmin,
                    CompanyId = companyId,
                },
            };
        }

        private static DTOs.UserCreate GetDemoUserForCreate()
        {
            return new DTOs.UserCreate
            {
                Email = "abc@zzz.com",
                Name = "user01",
                Role = DTOs.Enums.Role.Undefined
            };
        }

        private void InsertDemoUsersToDb(IEnumerable<DTOs.User> users)
        {
            var models = users.MapFrom(UserModel.Roles);
            UserModel.Users.AddRange(models);
            UserModel.SaveChanges();
        }

        private static bool AssertCollection(IEnumerable<DTOs.User> col1, IEnumerable<DTOs.User> col2)
        {
            if (col1 == null && col2 == null)
            {
                return true;
            }
            if ((col1 == null && col2 != null) ||
                (col1 != null && col2 == null) ||
                col1.Count() != col2.Count())
            {
                return false;
            }

            var c1 = col1.OrderBy(o => o.Id);
            var c2 = col2.OrderBy(o => o.Id);
            for (var i = 0; i < c1.Count(); i++)
            {
                if (!c1.ElementAt(i).Equals(c2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
