﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bat.Api.Controllers.Migration.v07;
using MigrationV07.Model;
using MigrationV07.Model.Models;
using Xunit;

namespace Bat.Api.Tests.Controllers.Migration.v07
{
    public class MigrationControllerTests : ControllerTests
    {
        private const string TestOldTerminalId = "abcd-1234-5678-90ef";
        private const string TestOldDeviceId = "192.168.1.250:1001-1-T-" + TestOldTerminalId;
        private readonly MigrationController controller;
        private readonly MigrationV07ModelContainer migrationContainer;
        private readonly Guid TestCompanyId = Guid.NewGuid();

        public MigrationControllerTests()
            : base(nameof(MigrationV07))
        {
            var options = CreateInMemoryDbContext<MigrationV07ModelContainer>("MigrationV07ModelContainer_InMemoryDB");
            migrationContainer = new MigrationV07ModelContainer(options);
            controller = new MigrationController(Connection, migrationContainer);

            FakeUser(TestCompanyId);
        }


        [Fact]
        public async void GetDevice_NotExistWholeMigrationAsync()
        {
            await NotExistDeviceTestAsync();
        }

        [Fact]
        public async void GetDevice_NotExistAsync()
        {
            InitMigrationCompany();
            await NotExistDeviceTestAsync();
        }

        private async Task NotExistDeviceTestAsync()
        {
            // Arrange
            const string id = TestOldDeviceId;

            // Act
            var response = await controller.GetDeviceIdAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void GetDevice_RequestOnTerminalAsync()
        {
            // Arrange
            InitMigrationCompany();
            const string oldTerminalId = "abcd-1234-5678-90ef";

            // Act
            var response = await controller.GetDeviceIdAsync(oldTerminalId);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.BadRequest, responseCode);
        }

        [Fact]
        public async void GetDevice_ReturnsDataAsync()
        {
            // Arrange
            InitMigrationCompany();
            var newId = Guid.NewGuid();
            const string oldId = TestOldDeviceId;
            InsertIdMapping(newId, oldId);

            // Act
            var response = await controller.GetDeviceIdAsync(oldId);

            // Assert
            var id = GetResponseData<Guid>(response);
            Assert.Equal(newId, id);
        }


        [Fact]
        public async void UpdateTerminal_MappingNotExistAsync()
        {
            // Arrange
            InitMigrationCompany();
            var body = new Terminal
            {
                Id = Guid.NewGuid(),
                OldId = TestOldTerminalId
            };

            // Act
            var response = await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void UpdateTerminal_MappingNotChangedAsync()
        {
            // Arrange
            InitMigrationCompany();
            var newId = Guid.NewGuid();
            const string oldId = TestOldTerminalId;
            var body = new Terminal
            {
                Id = newId,
                OldId = oldId
            };
            InsertIdMapping(newId, oldId);

            // Act
            var response = await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }


        [Fact]
        public async void UpdateTerminal_MigrationNotExistAsync()
        {
            // Arrange
            const string oldId = TestOldTerminalId;
            var body = new Terminal
            {
                Id = Guid.NewGuid(),
                OldId = oldId
            };

            // Act
            var response = await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void UpdateTerminal_TerminalNotExistAsync()
        {
            // Arrange
            InitMigrationCompany();
            const string oldId = TestOldTerminalId;
            var body = new Terminal
            {
                Id = Guid.NewGuid(),
                OldId = oldId
            };
            InsertIdMapping(Guid.NewGuid(), oldId);

            // Act
            var response = await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void UpdateTerminal_SuccessAsync()
        {
            // Arrange
            InitMigrationCompany();
            const string oldId = TestOldTerminalId;
            var body = new Terminal
            {
                Id = Guid.NewGuid(),
                OldId = oldId
            };
            var newIdBeforeUpdate = Guid.NewGuid();
            InsertIdMapping(newIdBeforeUpdate, oldId);
            Connection.CloudModel.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = newIdBeforeUpdate,
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal
            });
            Connection.CloudModel.SaveChanges();

            // Act
            await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            Assert.Single(Connection.CloudModel.Devices);
            var device = Connection.CloudModel.Devices.FirstOrDefault();
            Assert.NotNull(device);
            Assert.Equal(body.Id, device.Id);
        }

        [Fact]
        public async void UpdateTerminal_UpdateAllReferencesAsync()
        {
            // Arrange
            InitMigrationCompany();
            const string oldId = TestOldTerminalId;
            var body = new Terminal
            {
                Id = Guid.NewGuid(),
                OldId = oldId
            };
            var newIdBeforeUpdate = Guid.NewGuid();
            InsertIdMapping(newIdBeforeUpdate, oldId);
            InsertDemoData(newIdBeforeUpdate);

            // Act
            await controller.PutTerminalAsync(TestOldTerminalId, body);

            // Assert
            CheckDemodata(body.Id);
        }


        #region Private helpers

        private void InsertDemoData(Guid id)
        {
            Connection.CloudModel.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = id,
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal
            });
            Connection.CloudModel.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                Name = "1",
                ParentId = id
            });
            Connection.CloudModel.Devices.Add(new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                Name = "2",
                ParentId = id
            });
            Connection.CloudModel.Flocks.Add(new Cloud.Model.Models.Flock
            {
                Name = "demo flock",
                StartDate = DateTime.Now,
                DeviceId = id
            });
            Connection.CloudModel.SaveChanges();
        }

        private void CheckDemodata(Guid newId)
        {
            //check terminal
            var terminals = Connection.CloudModel.Devices.Where(w => w.Type == Cloud.Model.Enums.DeviceType.BatCollectorTerminal);
            Assert.Single(terminals);
            Assert.Equal(newId, terminals.First().Id);

            //check terminal's devices
            var devices = Connection.CloudModel.Devices.Where(w => w.Type != Cloud.Model.Enums.DeviceType.BatCollectorTerminal);
            foreach (var device in devices)
            {
                Assert.Equal(newId, device.ParentId);
            }

            //check terminal's flocks
            foreach (var flock in Connection.CloudModel.Flocks)
            {
                Assert.Equal(newId, flock.DeviceId);
            }
        }

        private void InsertIdMapping(Guid newId, string oldId)
        {
            var idMap = new Device
            {
                Id = newId,
                OldUid = oldId
            };

            var company = migrationContainer.Companies.FirstOrDefault(f => f.Id == TestCompanyId);
            company.Devices.Add(idMap);
            migrationContainer.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            migrationContainer.Devices.RemoveRange(migrationContainer.Devices);
            migrationContainer.Companies.RemoveRange(migrationContainer.Companies);
            migrationContainer.SaveChanges();
            migrationContainer.Dispose();
        }

        private void InitMigrationCompany()
        {
            migrationContainer.Companies.Add(new Company { Id = TestCompanyId });
            migrationContainer.SaveChanges();
        }

        #endregion
    }
}
