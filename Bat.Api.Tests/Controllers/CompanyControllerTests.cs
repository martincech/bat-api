﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.Map;
using User.Model.Enums;
using User.Model.Models;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class CompanyControllerTests : ControllerTests
    {
        private readonly CompanyController controller;

        public CompanyControllerTests()
            : base(nameof(Company))
        {
            controller = new CompanyController(Connection, MoqContainerProvider);
        }


        [Fact]
        public async void Get_CurrentCompanyAsync()
        {
            // Arrange
            var company = CreateDemoCompanyForCommonUser();
            FakeUser(company.Id);

            // Act
            var response = await controller.GetCurrentAsync();

            // Assert
            var data = GetResponseData<DTOs.Company>(response);
            Assert.True(company.MapTo().Equals(data));
        }

        [Fact]
        public async void Put_NotValidCurrentCompanyAsync()
        {
            // Arrange
            //No data is needed

            // Act
            var response = await controller.PutCurrentAsync(null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_CurrentCompanyModifiedAsync()
        {
            // Arrange
            var company = CreateDemoCompanyForCommonUser();
            var expected = company.MapTo();
            expected.Name = "Updated company name";
            FakeUser(company.Id);

            // Act
            var response = await controller.PutCurrentAsync(expected);

            // Assert
            var data = GetResponseData<DTOs.Company>(response);
            Assert.True(expected.Equals(data));
        }


        #region Administrator methods tests

        [Fact]
        public async void Get_AllCompanies_InsufficientAuthorizationAsync()
        {
            // Arrange

            // Act
            var response = await controller.GetAsync();

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }


        [Fact]
        public async void Get_AllCompanies_ValidDataAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            demoCompany.Id = demoCompanyId;
            CreateDemoCompany(demoCompany, demoCompanyId);

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Company>>(response);
            Assert.Single(data);
            Assert.True(demoCompany.Equals(data.FirstOrDefault()));
        }

        [Fact]
        public async void Get_AllCompanies_NoDataAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<DTOs.Company>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetCompanyById_ForeignCompanyAsStandardUserAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void GetCompanyById_ForeignCompanyAsVeitOperatorAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void GetCompanyById_ReturnsDataAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            demoCompany.Id = demoCompanyId;
            CreateDemoCompany(demoCompany, demoCompanyId);

            // Act
            var response = await controller.GetAsync(demoCompany.Id);

            // Assert
            var data = GetResponseData<DTOs.Company>(response);
            Assert.True(demoCompany.Equals(data));
        }


        [Fact]
        public async void Post_CreateCompany_InsufficientAuthorizationAsync()
        {
            // Arrange

            // Act
            var response = await controller.PostAsync(null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }

        [Fact]
        public async void Post_NotValidCompanyAsync()
        {
            // Arrange
            FakeUserAsAdministrator();

            // Act
            var response = await controller.PostAsync(null);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Post_CreateCompanyAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var testCompany = new DTOs.Company
            {
                Name = "My super test COMPANY"
            };

            // Act
            var response = await controller.PostAsync(testCompany);

            // Assert
            var data = GetResponseData<DTOs.Company>(response);
            testCompany.Id = data.Id;
            Assert.True(testCompany.Equals(data));
        }


        [Fact]
        public async void Delete_InsufficientAuthorizationAsync()
        {
            // Arrange
            var id = new Guid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.Forbidden, responseCode);
        }


        [Fact]
        public async void Delete_CompanyNotFoundAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void Delete_CompanyDeletedAsync()
        {
            // Arrange
            FakeUserAsAdministrator();
            var companyId = demoCompanyId;
            CreateDemoCompany(demoCompany, companyId);

            var commonUsers = new List<User.Model.Models.User>
            {
                new User.Model.Models.User { CompanyId = companyId },
                new User.Model.Models.User { CompanyId = companyId },
            };
            var sa1 = new User.Model.Models.User { Id = Guid.NewGuid(), CompanyId = companyId };
            var sa2 = new User.Model.Models.User { Id = Guid.NewGuid(), CompanyId = companyId };
            var role = new Role { Name = DbRole.VeitOperator.ToString() };
            UserModel.Roles.Add(role);
            var sa1Role = new UserRole { RoleId = role.Id, UserId = sa1.Id };
            var sa2Role = new UserRole { RoleId = role.Id, UserId = sa2.Id };
            sa1.UserRoles.Add(sa1Role);
            sa2.UserRoles.Add(sa2Role);

            var superUsers = new List<User.Model.Models.User> { sa1, sa2 };
            InsertUsersToDb(commonUsers.Concat(superUsers));


            // Act
            var response = await controller.DeleteAsync(companyId);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(UserModel.Companies);

            //common users were delete with their company, only super administrators stayed
            Assert.Equal(superUsers.Count(), UserModel.Users.Count());
            var dbUsers = UserModel.Users;
            foreach (var u in dbUsers)
            {
                var saRole = u.Roles.FirstOrDefault(f => f.Name == DbRole.VeitOperator.ToString());
                Assert.NotNull(saRole);
            }
        }

        #region Private helpers

        private readonly Guid demoCompanyId = new Guid(11, 22, 33, new byte[] { 5, 5, 5, 5, 5, 5, 5, 5 });

        private readonly DTOs.Company demoCompany = new DTOs.Company
        {
            Name = "123 company",
            Country = "ABC"
        };

        private void CreateDemoCompany(DTOs.Company company, Guid companyId)
        {
            var model = company.MapFrom();
            model.Id = companyId;
            UserModel.Companies.Add(model);
            UserModel.SaveChanges();

            var container = MoqContainerProvider.GetCompanyContainer(companyId);
        }

        private void InsertUsersToDb(IEnumerable<User.Model.Models.User> users)
        {
            UserModel.Users.AddRange(users);
            UserModel.SaveChanges();
        }

        #endregion

        private Company CreateDemoCompanyForCommonUser()
        {
            var company = new Company
            {
                Name = "Test company name XXSAD",
                City = "Brno",
                Country = "Czech Republic",
            };
            UserModel.Companies.Add(company);
            UserModel.SaveChanges();
            return company;
        }

        #endregion
    }
}
