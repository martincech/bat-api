﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Bat.Api.Connection;
using Bat.Api.Map;
using Bat.Cloud.Model;
using Bird.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Moq;
using Statistic;
using Statistic.Models;
using Statistic.Models.Enums;
using User.Model;
using User.Model.Enums;
using Veit.Cognito.User;
using Veit.Kafka;
using Veit.Kafka.Topics;
using Xunit;
using Device = Bat.Cloud.Model.Models.Device;
using Farm = Bat.Cloud.Model.Models.Farm;
using Flock = Bat.Cloud.Model.Models.Flock;
using House = Bat.Cloud.Model.Models.House;

namespace Bat.Api.Tests.Controllers
{
    public abstract class ControllerTests : IDisposable
    {
        private readonly Mock<ClaimsPrincipal> moqPrincipal;
        private readonly MockStatisticContext moqStatistic;


        protected ControllerTests(string name)
        {
            var options = CreateInMemoryDbContext<BatCloudModelContainer>(name + "ControllerTests_InMemoryDB");
            var userOptions = CreateInMemoryDbContext<UserModelContainer>(name + "ControllerUserTests_InMemoryDB");
            var birdOptions = CreateInMemoryDbContext<BirdModelContainer>(name + "ControllerBirdTests_InMemoryDB");

            Model = new BatCloudModelContainer(options);
            UserModel = new UserModelContainer(userOptions);
            BirdModel = new BirdModelContainer(birdOptions);
            moqPrincipal = new Mock<ClaimsPrincipal>();
            MoqContainerProvider = new MockContainerProvider();
            moqStatistic = new MockStatisticContext();
            MoqKafka = new Mock<IKafkaSender>();
            MoqKafka.Setup(x => x.Send(It.IsAny<List<DeviceGroup>>())).Returns(true);
            MoqKafka.Setup(x => x.Send(It.IsAny<List<DeviceMap>>())).Returns(true);

            MoqUserContext = new Mock<IUserContext>();
            MoqUserContext.Setup(x => x.DeleteUserAsync(It.IsAny<Guid>())).ReturnsAsync(true);
            MoqUserContext.Setup(x => x.CreateUserAsync(It.IsAny<Veit.Cognito.User.Model.CognitoUser>(), It.IsAny<bool>())).ReturnsAsync(Guid.NewGuid());
            MoqUserContext.Setup(x => x.UpdateUserAsync(It.IsAny<Veit.Cognito.User.Model.CognitoUser>())).ReturnsAsync(true);

            Connection = new ConnectionFactory(Model,
                                               UserModel,
                                               moqPrincipal.Object,
                                               moqStatistic,
                                               MoqKafka.Object);
        }

        protected ConnectionFactory Connection { get; private set; }
        protected BatCloudModelContainer Model { get; private set; }
        protected UserModelContainer UserModel { get; private set; }
        protected BirdModelContainer BirdModel { get; private set; }
        protected MockContainerProvider MoqContainerProvider { get; private set; }
        protected Mock<IUserContext> MoqUserContext { get; private set; }
        protected Mock<IKafkaSender> MoqKafka { get; private set; }

        protected string CommonWholeExpandParameter { get; } = "all";

        protected static DbContextOptions<T> CreateInMemoryDbContext<T>(string name)
            where T : DbContext
        {
            return new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase(databaseName: name)
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .EnableSensitiveDataLogging()
                .Options;
        }

        #region Helper methods for check API responses

        protected static void AssertOkResponse(ActionResult<DTOs.ApiResponse> response)
        {
            var result = Assert.IsType<OkObjectResult>(response.Result);
            var resultModel = Assert.IsAssignableFrom<DTOs.ApiResponse>(result.Value);
            Assert.Equal((int)HttpStatusCode.OK, resultModel.Code);
        }

        protected static int GetResponseCode(ActionResult<DTOs.ApiResponse> response)
        {
            var result = Assert.IsType<ObjectResult>(response.Result);
            var resultModel = Assert.IsAssignableFrom<DTOs.ApiResponse>(result.Value);
            return resultModel.Code;
        }

        protected static T GetResponseData<T>(ActionResult<DTOs.ApiResponse> response)
        {
            ObjectResult result = null;
            var expectedCode = (int)HttpStatusCode.OK;

            if (response.Result is OkObjectResult)
            {
                result = Assert.IsType<OkObjectResult>(response.Result);
            }
            else if (response.Result is CreatedAtRouteResult)
            {
                expectedCode = (int)HttpStatusCode.Created;
                result = Assert.IsType<CreatedAtRouteResult>(response.Result);
            }
            else
            {
                throw new ArgumentException($"Unexpected response result: {nameof(response)}");
            }

            var resultModel = Assert.IsAssignableFrom<DTOs.ApiResponse>(result.Value);
            Assert.Equal(expectedCode, resultModel.Code);
            return Assert.IsAssignableFrom<T>(resultModel.Data);
        }

        #endregion

        protected void FakeUserRole(DbRole role)
        {
            moqPrincipal.Setup(e => e.IsInRole(role.ToString())).Returns(true);
        }

        protected void FakeUserAsAdministrator()
        {
            FakeUserRole(DbRole.VeitOperator);
        }

        protected void FakeUserAsComapnyAdmin(Guid companyId)
        {
            FakeUserRole(DbRole.CompanyAdmin);
            FakeUser(companyId);
        }

        protected void FakeUser(Guid companyId)
        {
            var user = new User.Model.Models.User
            {
                CompanyId = companyId
            };
            UserModel.Users.Add(user);
            UserModel.SaveChanges();

            FakeUserIdentity(user.Id);
        }

        protected void FakeUserIdentity(Guid userId)
        {
            var fakeClaim = new Claim(ClaimTypes.NameIdentifier, userId.ToString());
            moqPrincipal.Setup(e => e.Claims).Returns(new List<Claim> { fakeClaim });
        }

        protected void FakeStatistics(GroupData data)
        {
            moqStatistic.Data = data;
        }

        protected virtual void Dispose(bool disposing)
        {
            UserModel.Roles.RemoveRange(UserModel.Roles);
            UserModel.Users.RemoveRange(UserModel.Users);
            UserModel.Companies.RemoveRange(UserModel.Companies);
            UserModel.SaveChanges();
            UserModel.Dispose();


            Model.Flocks.RemoveRange(Model.Flocks);
            Model.Houses.RemoveRange(Model.Houses);
            Model.Farms.RemoveRange(Model.Farms);
            Model.Devices.RemoveRange(Model.Devices);
            Model.Birds.RemoveRange(Model.Birds);
            Model.BirdProducts.RemoveRange(Model.BirdProducts);
            Model.BirdCompanies.RemoveRange(Model.BirdCompanies);
            Model.SaveChanges();
            Model.Dispose();

            BirdModel.Birds.RemoveRange(BirdModel.Birds);
            BirdModel.Products.RemoveRange(BirdModel.Products);
            BirdModel.Companies.RemoveRange(BirdModel.Companies);
            BirdModel.SaveChanges();
            BirdModel.Dispose();


            MoqContainerProvider.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        #region Demo data

        protected static Farm GetTestFarm()
        {
            return new Farm
            {
                Id = Guid.NewGuid(),
                Name = "Test farm for houses tests",
                Country = "CZ"
            };
        }

        protected static List<House> GetTestHouses()
        {
            var farm = GetTestFarm();
            return new List<House>
            {
                new House
                {
                    Id = Guid.NewGuid(),
                    Name = "HOUSE HH01",
                    FarmId = farm.Id,
                    Farm = farm
                },
                new House
                {
                    Id = Guid.NewGuid(),
                    Name = "HOUSE HH02",
                    FarmId = farm.Id,
                    Farm = farm
                }
            };
        }

        protected static bool IsHouseSame(IEnumerable<House> houses, IEnumerable<DTOs.House> objs)
        {
            if (houses == null && objs == null)
            {
                return true;
            }
            if ((houses == null && objs != null) ||
                (houses != null && objs == null) ||
                houses.Count() != objs.Count())
            {
                return false;
            }

            for (var i = 0; i < houses.Count(); i++)
            {
                if (!IsHouseSame(houses.ElementAt(i), objs.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        protected static bool IsHouseSame(House house, DTOs.House other)
        {
            return other.Equals(house.MapTo());
        }

        protected static List<Farm> GetTestFarms()
        {
            return new List<Farm>
            {
                new Farm
                {
                    Id = new Guid(1, 2, 3, new byte[] { 1, 2, 3, 4, 5, 6, 7, 8}),
                    Name = "Farm 01",
                    Address = "Avenue 1503",
                    Country = "Banana Republic",
                },
                new Farm
                {
                    Id = new Guid(11, 22, 33, new byte[] { 8, 7, 6, 5, 4, 3, 2, 1}),
                    Name = "Farm 45678",
                    Address = "Street ABC",
                    Country = "Banana Republic",
                },
            };
        }

        protected static bool IsFarmSame(IEnumerable<Farm> farms, IEnumerable<DTOs.Farm> others)
        {
            if (farms == null && others == null)
            {
                return true;
            }
            if ((farms == null && others != null) ||
                (farms != null && others == null) ||
                farms.Count() != others.Count())
            {
                return false;
            }

            for (var i = 0; i < farms.Count(); i++)
            {
                if (!IsFarmSame(farms.ElementAt(i), others.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        protected static bool IsFarmSame(Farm farm, DTOs.Farm other)
        {
            return other.Equals(farm.MapTo());
        }

        protected static List<Device> GetTestDevices()
        {
            var houses = GetTestHouses();
            var parentDevice = new Device
            {
                Name = "TERMINAL #local 001",
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal,
                Description = "The first terminal",
                House = houses[0]
            };
            return new List<Device>
            {
                parentDevice,
                new Device
                {
                    Name = "DEVICE #---1",
                    Type = Cloud.Model.Enums.DeviceType.HumiditySensor,
                    Description = "description -1",
                    Farm = houses[0].Farm,
                },
                new Device
                {
                    Name = "DEVICE #---2",
                    Type = Cloud.Model.Enums.DeviceType.Bat2GsmScale,
                    Description = "description -2",
                    PhoneNumber = "+420123456789",
                    Parent = parentDevice
                },
            };
        }

        protected static bool IsDeviceSame(IEnumerable<DTOs.Device> devices, IEnumerable<DTOs.Device> objs)
        {
            if (devices == null && objs == null)
            {
                return true;
            }
            if ((devices == null && objs != null) ||
                (devices != null && objs == null) ||
                devices.Count() != objs.Count())
            {
                return false;
            }

            devices = devices.OrderBy(o => o.Id);
            objs = objs.OrderBy(o => o.Id);
            for (var i = 0; i < devices.Count(); i++)
            {
                if (!IsDeviceSame(devices.ElementAt(i), objs.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        protected static bool IsDeviceSame(DTOs.Device device, DTOs.Device other)
        {
            return other.Equals(device);
        }

        protected static List<Flock> GetTestFlocks()
        {
            var houses = GetTestHouses();
            return new List<Flock>
            {
                new Flock
                {
                    Id = new Guid(1, 2, 3, new byte[] { 1, 2, 3, 4, 5, 6, 7, 8}),
                    Name = "Flock AAA 850",
                    InitialAge = 2,
                    InitialWeight = 35,
                    TargetAge = 35,
                    StartDate = new DateTime(2018, 10, 31),
                    HouseId = houses[0].Id,
                    House = houses[0]
                },
                new Flock
                {
                    Id = new Guid(3, 2, 1, new byte[] { 10, 20, 30, 40, 50, 60, 70, 80}),
                    Name = "Flock BBB 222",
                    InitialAge = 0,
                    InitialWeight = 74,
                    TargetWeight = 1437,
                    StartDate = new DateTime(2018, 11, 1),
                    HouseId = houses[1].Id,
                    House = houses[1]
                }
            };
        }

        protected static bool IsFlockSame(IEnumerable<Flock> flocks, IEnumerable<DTOs.Flock> objs)
        {
            if (flocks == null && objs == null)
            {
                return true;
            }
            if ((flocks == null && objs != null) ||
                (flocks != null && objs == null) ||
                flocks.Count() != objs.Count())
            {
                return false;
            }

            for (var i = 0; i < flocks.Count(); i++)
            {
                if (!IsFlockSame(flocks.ElementAt(i), objs.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        protected static bool IsFlockSame(Flock flock, DTOs.Flock other)
        {
            return other.Equals(flock.MapTo());
        }


        protected static List<Bird.Model.Models.Bird> GetTestBirds()
        {
            var product = new Bird.Model.Models.Product
            {
                Id = Guid.NewGuid(),
                Name = "500",
                Company = new Bird.Model.Models.Company
                {
                    Id = Guid.NewGuid(),
                    Name = "Cobb"
                }
            };

            var bird1 = new Bird.Model.Models.Bird
            {
                Id = Guid.NewGuid(),
                Name = "broilers",
                Product = product,
                Sex = Bird.Model.Enums.Sex.Female,
                Duration = 37,
                DateType = Bird.Model.Enums.DateType.Day
            };
            bird1.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 1, Weight = 35 });
            bird1.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 7, Weight = 241 });
            bird1.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 14, Weight = 571 });
            bird1.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 21, Weight = 1047 });

            var bird2 = new Bird.Model.Models.Bird
            {
                Id = Guid.NewGuid(),
                Name = "layers",
                Product = product,
                Sex = Bird.Model.Enums.Sex.Female,
                Duration = 300,
                DateType = Bird.Model.Enums.DateType.Week
            };
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 1, Weight = 33 });
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 10, Weight = 145 });
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 20, Weight = 320 });
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 30, Weight = 600 });
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 40, Weight = 800 });
            bird2.CurvePoints.Add(new Bird.Model.Models.CurvePoint { Day = 50, Weight = 1100 });

            return new List<Bird.Model.Models.Bird> { bird1, bird2 };
        }

        protected static bool IsBirdSame(IEnumerable<Bird.Model.Models.Bird> birds, IEnumerable<DTOs.Bird> objs)
        {
            if (birds == null && objs == null)
            {
                return true;
            }
            if ((birds == null && objs != null) ||
                (birds != null && objs == null) ||
                birds.Count() != objs.Count())
            {
                return false;
            }

            birds = birds.OrderBy(o => o.Id);
            objs = objs.OrderBy(o => o.Id);

            for (var i = 0; i < birds.Count(); i++)
            {
                if (!IsBirdSame(birds.ElementAt(i), objs.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        protected static bool IsBirdSame(Bird.Model.Models.Bird bird, DTOs.Bird other)
        {
            return other.Equals(bird.MapTo());
        }

        #endregion
    }

    #region Mock objects

    public sealed class MockContainerProvider : IContainerProvider, IDisposable
    {
        private readonly Dictionary<Guid, BatCloudModelContainer> containers = new Dictionary<Guid, BatCloudModelContainer>();

        public BatCloudModelContainer GetCompanyContainer(Guid companyId)
        {
            if (containers.TryGetValue(companyId, out var existed))
            {
                return existed;
            }

            var options = new DbContextOptionsBuilder<BatCloudModelContainer>()
               .UseInMemoryDatabase(databaseName: companyId + "ControllerCompanyTests_InMemoryDB")
               .Options;
            var newContainer = new BatCloudModelContainer(options);
            containers.Add(companyId, newContainer);
            return newContainer;
        }


        public void Dispose()
        {
            foreach (var container in containers)
            {
                container.Value.Database.EnsureDeleted();
                container.Value.Dispose();
            }

            GC.SuppressFinalize(this);
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task CreateCompanyDatabaseAsync(Guid companyId)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var newCompany = GetCompanyContainer(companyId);
        }

        public async Task DeleteCompanyDatabaseAsync(Guid companyId)
        {
            if (containers.TryGetValue(companyId, out var existed))
            {
                await existed.Database.EnsureDeletedAsync();
                await existed.SaveChangesAsync();
                existed.Dispose();
                containers.Remove(companyId);
            }
        }
    }

    internal class MockStatisticContext : IStatisticContext
    {
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<bool> CreateCompanyContextAsync(string companyId)
        {
            return true;
        }

        public async Task<bool> DropCompanyContextAsync(string companyId)
        {
            return true;
        }

        public async Task<bool> ExistCompanyContextAsync(string companyId)
        {
            return true;
        }

        public GroupData Data { get; set; }

        public async Task<GroupData> ReadAllWeightStatisticsAsync(string companyId, Guid id, StatisticGroup group, AggregationInterval interval, DateTimeOffset? from, DateTimeOffset? to)
        {
            return Data;
        }

        public async Task<IEnumerable<GroupData>> ReadLastWeightStatisticsAsync(string companyId, IEnumerable<Guid> id, StatisticGroup group, AggregationInterval interval)
        {
            return new List<GroupData> { Data };
        }

#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
    }

    #endregion
}
