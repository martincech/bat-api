﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Statistic.Models;
using Statistic.Models.Enums;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class StatisticControllerTests : ControllerTests
    {
        private readonly StatisticController controller;


        public StatisticControllerTests()
            : base(nameof(DailyStatistic))
        {
            controller = new StatisticController(Connection);
        }


        [Fact]
        public async void GetAllStatisticByFlock_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAllStatisticByFlockIdAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }

        [Fact]
        public async void GetAllStatisticByFlock_ReturnsDataAsync()
        {
            // Arrange
            var demoFlock = GetTestFlocks().First();
            Model.Add(demoFlock);
            Model.SaveChanges();

            var groupData = GetTestGroupData();
            FakeStatistics(groupData);
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetAllStatisticByFlockIdAsync(demoFlock.Id);

            // Assert
            var data = GetResponseData<IEnumerable<DailyStatistic>>(response);
            Assert.True(IsStatisticSame(groupData.MapTo(), data));
        }


        [Fact]
        public async void GetLastStatisticByFlock_ReturnsDataAsync()
        {
            // Arrange
            var demoFlocks = GetTestFlocks();
            Model.Flocks.AddRange(demoFlocks);
            Model.SaveChanges();

            var groupData = GetTestGroupData();
            FakeStatistics(groupData);
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetLastStatisticsByFlockIdsAsync(demoFlocks.Select(s => s.Id));

            // Assert
            var data = GetResponseData<IEnumerable<DailyStatistic>>(response);
            Assert.True(IsStatisticSame(groupData.MapTo(), data));
        }


        [Fact]
        public async void GetAllStatisticByDevice_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAllStatisticByDeviceIdAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void GetAllStatisticByDevice_ReturnsDataAsync()
        {
            // Arrange
            var demoFlock = GetTestFlocks().First();
            var demoDevice = GetTestDevices().First();
            demoFlock.Device = demoDevice;
            Model.Devices.Add(demoDevice);
            Model.Flocks.Add(demoFlock);
            Model.SaveChanges();

            var groupData = GetTestGroupData();
            FakeStatistics(groupData);
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetAllStatisticByDeviceIdAsync(demoDevice.Id);

            // Assert
            var data = GetResponseData<IEnumerable<DailyStatistic>>(response);
            Assert.True(IsStatisticSame(groupData.MapTo(), data));
        }

        [Fact]
        public async void GetLastStatisticByDevice_ReturnsDataAsync()
        {
            // Arrange
            var demoHouse = GetTestHouses().First();
            var demoDevices = GetTestDevices();
            foreach (var dev in demoDevices)
            {
                dev.House = demoHouse;
            }
            var demoFlock = GetTestFlocks().First();
            demoFlock.StartDate = DateTime.Now.AddDays(-2);
            demoFlock.House = demoHouse;

            Model.Houses.Add(demoHouse);
            Model.Flocks.Add(demoFlock);
            Model.Devices.AddRange(demoDevices);
            Model.SaveChanges();

            var groupData = GetTestGroupData();
            groupData.Id = demoDevices.Last().Id.ToString();
            FakeStatistics(groupData);
            FakeUser(Guid.NewGuid());

            // Act
            var response = await controller.GetLastStatisticsByDeviceIdsAsync(demoDevices.Select(s => s.Id));

            // Assert
            var data = GetResponseData<IEnumerable<DailyStatistic>>(response);
            Assert.True(IsStatisticSame(groupData.MapTo(), data));
        }


        #region Private helpers

        private static bool IsStatisticSame(IEnumerable<DailyStatistic> expected, IEnumerable<DailyStatistic> actual)
        {
            if ((expected == null && actual != null) ||
                (expected != null && actual == null))
            {
                return false;
            }

            var l1 = expected.OrderBy(o => o.DateTime);
            var l2 = actual.OrderBy(o => o.DateTime);

            if (l1.Count() != l2.Count())
            {
                return false;
            }

            for (var i = 0; i < l1.Count(); i++)
            {
                var element1 = l1.ElementAt(i);
                var element2 = l2.ElementAt(i);

                if (!element1.Equals(element2))
                {
                    return false;
                }
            }
            return true;
        }

        private static GroupData GetTestGroupData()
        {
            var date1 = DateTime.Now.AddDays(-1).Date.AddSeconds(-1);
            var date2 = date1.AddDays(1);

            return new GroupData
            {
                Id = Guid.NewGuid().ToString(),
                Group = StatisticGroup.Device,
                TimeSize = AggregationInterval.Day,
                Values = new List<Stat>
                    {
                        new Stat
                        {
                            Day = 1,
                            TimeStamp = date1,
                            Average = 34,
                            Min = 31,
                            Max = 39,
                            Count = 250,
                            Cv = 0.8,
                            Sigma = 50,
                            Uniformity = 97,
                            Gain = 0,
                            Sex = Veit.Bat.Common.Sex.Female
                        },
                        new Stat
                        {
                            Day = 1,
                            TimeStamp = date1,
                            Average = 35,
                            Min = 33,
                            Max = 40,
                            Count = 220,
                            Cv = 0.75,
                            Sigma = 50,
                            Uniformity = 98.7,
                            Gain = 0,
                            Sex = Veit.Bat.Common.Sex.Male
                        },
                        new Stat
                        {
                            Day = 3,
                            TimeStamp = date2,
                            Average = 45,
                            Min = 36,
                            Max = 48,
                            Count = 311,
                            Cv = 0.9,
                            Sigma = 60,
                            Uniformity = 100,
                            Gain = 33,
                            Sex = Veit.Bat.Common.Sex.Female
                        }
                    }
            };
        }


        #endregion
    }
}
