﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat.Api.Controllers;
using Bat.Api.DTOs;
using Bat.Api.Map;
using Xunit;

namespace Bat.Api.Tests.Controllers
{
    public class HouseControllerTests : ControllerTests
    {
        private readonly HouseController controller;

        public HouseControllerTests()
            : base(nameof(House))
        {
            controller = new HouseController(Connection);
        }



        [Fact]
        public async void GetAllHouses_NoDataAsync()
        {
            // Arrange
            //No data

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<House>>(response);
            Assert.Empty(data);
        }


        [Fact]
        public async void GetAllHouses_ReturnsDataAsync()
        {
            // Arrange
            var testData = GetTestHouses();
            Model.Houses.AddRange(testData);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync();

            // Assert
            var data = GetResponseData<IEnumerable<House>>(response);
            Assert.Equal(testData.Count, data.Count());
            Assert.True(IsHouseSame(testData, data));
        }


        [Fact]
        public async void GetHouseById_NotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.GetAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void GetHouseById_ReturnsDataAsync()
        {
            // Arrange
            var expectedHouse = GetTestHouses().First();
            Model.Add(expectedHouse);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedHouse.Id);

            // Assert
            var data = GetResponseData<House>(response);
            Assert.True(IsHouseSame(expectedHouse, data));
        }

        [Fact]
        public async void GetHouseById_ReturnsAllExpandDataAsync()
        {
            // Arrange
            var expectedHouse = GetTestHouses().First();
            Model.Houses.Add(expectedHouse);
            var expectedScalesCount = CreateHouseScales(expectedHouse);
            Model.SaveChanges();

            // Act
            var response = await controller.GetAsync(expectedHouse.Id, CommonWholeExpandParameter);

            // Assert
            var data = GetResponseData<House>(response);
            Assert.True(IsHouseSame(expectedHouse, data));
            Assert.Equal(expectedScalesCount, data.ScalesCount);
        }

        /// <summary>
        /// Creates 4 devices - 1 terminal and
        /// 3 scales - one of them is reference to house directly,
        /// others are referenced by terminal to the house.
        /// </summary>
        /// <param name="house"></param>
        /// <returns></returns>
        private int CreateHouseScales(Cloud.Model.Models.House house)
        {
            var terminal = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.BatCollectorTerminal,
                House = house
            };
            var scale1 = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                Parent = terminal
            };
            var scale2 = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                Parent = terminal
            };
            var scale3 = new Cloud.Model.Models.Device
            {
                Id = Guid.NewGuid(),
                Type = Cloud.Model.Enums.DeviceType.Bat2CableScale,
                House = house
            };

            Model.Devices.Add(terminal);
            Model.Devices.Add(scale1);
            Model.Devices.Add(scale2);
            Model.Devices.Add(scale3);
            return 3;
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoHouses))]
        public async void Post_NotValidHouseAsync(HouseCreate testHouse)
        {
            // Arrange
            //data in argument

            // Act
            var response = await controller.PostAsync(testHouse);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        public static IEnumerable<object[]> GetInvalidDemoHouses
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new House { Name = "Invalid test house" } }    //missing reference (required) on farm
                };
            }
        }


        [Fact]
        public async void Post_CreateHouseAsync()
        {
            // Arrange
            var farm = GetTestFarm();
            Model.Farms.Add(farm);
            Model.SaveChanges();

            var testHouse = new HouseCreate
            {
                Name = "My super test house",
                FarmId = farm.Id
            };

            // Act
            var response = await controller.PostAsync(testHouse);

            // Assert
            var data = GetResponseData<House>(response);
            Assert.Equal(testHouse.Name, data.Name);
        }


        [Fact]
        public async void Put_HouseNotFoundAsync()
        {
            // Arrange
            var farm = GetTestFarm();
            var house = GetTestHouses().First();
            house.Farm = farm;
            house.FarmId = farm.Id;
            Model.Farms.Add(farm);
            Model.SaveChanges();

            // Act
            var response = await controller.PutAsync(house.Id, house.MapTo());

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoHouses))]
        public async void Put_NotValidHouseAsync(House testHouse)
        {
            // Arrange
            //No data is needed

            // Act
            var response = await controller.PutAsync(new Guid(), testHouse);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.PreconditionFailed, responseCode);
        }

        [Fact]
        public async void Put_HouseModifiedAsync()
        {
            // Arrange
            var originHouse = GetTestHouses().First();
            await Model.Houses.AddAsync(originHouse);
            await Model.SaveChangesAsync();

            var updateHouse = originHouse.MapTo();
            updateHouse.Name = "UPDATED HHH";

            // Act
            var response = await controller.PutAsync(originHouse.Id, updateHouse);

            // Assert
            var data = GetResponseData<House>(response);
            Assert.True(data.Equals(updateHouse));
        }


        [Fact]
        public async void Delete_HouseNotFoundAsync()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var response = await controller.DeleteAsync(id);

            // Assert
            var responseCode = GetResponseCode(response);
            Assert.Equal((int)HttpStatusCode.NotFound, responseCode);
        }


        [Fact]
        public async void Delete_HouseDeletedAsync()
        {
            // Arrange
            var originHouse = GetTestHouses().First();
            await Model.Houses.AddAsync(originHouse);
            await Model.SaveChangesAsync();

            // Act
            var response = await controller.DeleteAsync(originHouse.Id);

            // Assert
            AssertOkResponse(response);
            Assert.Empty(Model.Houses);
        }
    }
}
