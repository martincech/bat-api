﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfluxData.Net.InfluxDb.Models.Responses;
using Statistic.Models;
using Statistic.Models.Enums;
using Xunit;

#pragma warning disable CA1308 // Normalize strings to uppercase

namespace Statistic.Tests
{
    public class InfluxParserTests
    {
        private readonly InfluxParser parser;
        private static readonly string validSerieName = $"device_{Guid.NewGuid()}_weight_1440m";

        public InfluxParserTests()
        {
            parser = new InfluxParser();
        }


        [Theory]
        [MemberData(nameof(GetInvalidDemoData))]
        public void Parse_InvalidData(List<Serie> invalidData)
        {
            // Arrange
            //data in argument

            // Act
#pragma warning disable IDE0039 // Use local function
            Action act = () => parser.ParseWeightStatistics(invalidData);
#pragma warning restore IDE0039 // Use local function

            // Assert
            Assert.Throws<ArgumentException>(act);
        }

        public static IEnumerable<object[]> GetInvalidDemoData
        {
            get
            {
                return new[]
                {
                    // unknown - unsupported statistic group value
                    new object[] { new List<Serie> { new Serie { Name = $"unknown_{Guid.NewGuid()}_weight_1440m" } } },
                    // invalid ID (not GUID)
                    new object[] { new List<Serie> { new Serie { Name = $"device_nonGuiId_weight_1440m" } } },
                    //not weight
                    new object[] { new List<Serie> { new Serie { Name = $"device_{Guid.NewGuid()}_humidity_1440m" } } },
                    //unsupported time size
                    new object[] { new List<Serie> { new Serie { Name = $"device_{Guid.NewGuid()}_weight_11m" } } },
                    //missing columns
                    new object[] { new List<Serie> { new Serie { Columns = null, Name = validSerieName } } },
                    //missing any columns
                    new object[] { new List<Serie> { new Serie { Columns = new List<string> { "day", "time" }, Name = validSerieName } } },
                    //no values
                    new object[] { new List<Serie> { new Serie { Values = null, Columns = SerieColumns, Name = validSerieName } } },
                    //values are not complete
                    new object[] { new List<Serie> { new Serie { Values = new List<IList<object>> { new List<object> { "g" } }, Columns = SerieColumns, Name = validSerieName } } },
                    //invalid sex
                    new object[] { new List<Serie> { new Serie { Values = new List<IList<object>> { new List<object> { 10, DateTime.Now, 950, 900, 1000, 2154, 10, 11, 99.5, 8, "unknown" } }, Columns = SerieColumns, Name = validSerieName } } }
                };
            }
        }


        [Theory]
        [MemberData(nameof(GetEmptyDemoData))]
        public void Parse_EmptyData(IEnumerable<Serie> responseData)
        {
            // Arrange
            //data in argument

            // Act
            var groupData = parser.ParseWeightStatistics(responseData);

            // Assert
            Assert.Empty(groupData);
        }

        public static IEnumerable<object[]> GetEmptyDemoData
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new List<Serie>() }
                };
            }
        }


        [Theory]
        [MemberData(nameof(GetValidDemoData))]
        public void Parse_ValidData_AllProperties(IEnumerable<GroupData> expectedData)
        {
            // Arrange
            var responseData = GetDemoWeightData(expectedData);

            // Act
            var groupData = parser.ParseWeightStatistics(responseData);

            // Assert
            CheckResponses(expectedData, groupData);
        }


        public static IEnumerable<object[]> GetValidDemoData
        {
            get
            {
                var dataWithoutMinMax = GetDemoGroupData();
                foreach (var group in dataWithoutMinMax)
                {
                    foreach (var stat in group.Values)
                    {
                        stat.Min = null;
                        stat.Max = null;
                    }
                }

                return new[]
                {
                    new object[] { GetDemoGroupData() },    //full data
                    new object[] { dataWithoutMinMax }      //some properties are null
                };
            }
        }


        #region Private helpers

        private static void CheckResponses(IEnumerable<GroupData> data1, IEnumerable<GroupData> data2)
        {
            var l1 = data1.OrderBy(o => o.Id);
            var l2 = data2.OrderBy(o => o.Id);

            Assert.Equal(data1.Count(), data2.Count());
            for (var i = 0; i < l1.Count(); i++)
            {
                var group1 = l1.ElementAt(i);
                var group2 = l2.ElementAt(i);

                group1.Values = group1.Values.OrderBy(o => o.Day).ThenBy(t => t.Sex);
                group2.Values = group2.Values.OrderBy(o => o.Day).ThenBy(t => t.Sex);
                Assert.Equal(group1.Values.Count(), group2.Values.Count());
                for (var j = 0; j < group1.Values.Count(); j++)
                {
                    var a = group1.Values.ElementAt(j);
                    var b = group2.Values.ElementAt(j);

                    Assert.Equal(a.Day, b.Day);
                    Assert.Equal(a.TimeStamp, b.TimeStamp);
                    Assert.Equal(a.Average, b.Average);
                    Assert.Equal(a.Min, b.Min);
                    Assert.Equal(a.Max, b.Max);
                    Assert.Equal(a.Count, b.Count);
                    Assert.Equal(a.Cv, b.Cv);
                    Assert.Equal(a.Sigma, b.Sigma);
                    Assert.Equal(a.Uniformity, b.Uniformity);
                    Assert.Equal(a.Gain, b.Gain);
                    Assert.Equal(a.Sex, b.Sex);
                }

                Assert.Equal(group1.Id, group2.Id);
                Assert.Equal(group1.Group, group2.Group);
                Assert.Equal(group1.TimeSize, group2.TimeSize);
            }
        }

        private static IEnumerable<Serie> GetDemoWeightData(IEnumerable<GroupData> groups)
        {
            var series = new List<Serie>();
            foreach (var group in groups)
            {
                series.AddRange(ConvertGroup(group));
            }
            return series;
        }

        private static IEnumerable<Serie> ConvertGroup(GroupData data)
        {
            var series = new List<Serie>();
            var groups = data.Values.GroupBy(g => g.Sex);
            foreach (var group in groups)
            {
                var serie = new Serie
                {
                    Columns = SerieColumns,
                    Name = $"{data.Group.ToString().ToLowerInvariant()}_{data.Id}_weight_{data.TimeSize.MapToMinutes()}m",
                    Tags = new Dictionary<string, string> { { "sex", group.Key.ToString().ToLowerInvariant() } },
                    Values = MapStat(group)
                };
                series.Add(serie);
            }
            return series;
        }

        private static IList<string> SerieColumns
        {
            get { return AggregationTypeExtensions.AggregationLowerNames().ToList(); }
        }

        private static IList<IList<object>> MapStat(IEnumerable<Stat> stats)
        {
            var values = new List<IList<object>>();
            foreach (var stat in stats)
            {
                values.Add(new List<object>
                {
                    stat.TimeStamp,
                    stat.Day,
                    stat.Average,
                    stat.Count,
                    stat.Min,
                    stat.Max,
                    stat.Sigma,
                    stat.Cv,
                    stat.Uniformity,
                    stat.Gain,
                    stat.Sex.ToString().ToLowerInvariant()
                });
            }
            return values;
        }


        private static IEnumerable<GroupData> GetDemoGroupData()
        {
            return new List<GroupData>
            {
                new GroupData
                {
                    Id = Guid.NewGuid().ToString(),
                    Group = StatisticGroup.Device,
                    TimeSize = AggregationInterval.Day,
                    Values = new List<Stat>
                    {
                        new Stat
                        {
                            Day = 1,
                            TimeStamp = new DateTime(2019, 1, 10, 23, 59, 59),
                            Average = 34,
                            Min = 31,
                            Max = 39,
                            Count = 250,
                            Cv = 0.8,
                            Sigma = 50,
                            Uniformity = 97,
                            Gain = 0,
                            Sex = Veit.Bat.Common.Sex.Female
                        },
                        new Stat
                        {
                            Day = 1,
                            TimeStamp = new DateTime(2019, 1, 10, 23, 59, 59),
                            Average = 35,
                            Min = 33,
                            Max = 40,
                            Count = 220,
                            Cv = 0.75,
                            Sigma = 50,
                            Uniformity = 98.7,
                            Gain = 0,
                            Sex = Veit.Bat.Common.Sex.Male
                        },
                        new Stat
                        {
                            Day = 2,
                            TimeStamp = new DateTime(2019, 1, 11, 23, 59, 59),
                            Average = 45,
                            Min = 36,
                            Max = 48,
                            Count = 311,
                            Cv = 0.9,
                            Sigma = 60,
                            Uniformity = 97,
                            Gain = 10,
                            Sex = Veit.Bat.Common.Sex.Female
                        },
                        new Stat
                        {
                            Day = 2,
                            TimeStamp = new DateTime(2019, 1, 11, 23, 59, 59),
                            Average = 44,
                            Min = 36,
                            Max = 49,
                            Count = 331,
                            Cv = 0.9,
                            Sigma = 60,
                            Uniformity = 98,
                            Gain = 9,
                            Sex = Veit.Bat.Common.Sex.Male
                        },
                    }
                },
                new GroupData
                {
                    Id = Guid.NewGuid().ToString(),
                    Group = StatisticGroup.Device,
                    TimeSize = AggregationInterval.Day,
                    Values = new List<Stat>
                    {
                        new Stat
                        {
                            Day = 30,
                            TimeStamp = new DateTime(2019, 1, 10, 23, 59, 59),
                            Average = 1325,
                            Min = 1320,
                            Max = 1338,
                            Count = 2041,
                            Cv = 1,
                            Sigma = 10,
                            Uniformity = 99,
                            Gain = 25,
                            Sex = Veit.Bat.Common.Sex.Undefined
                        },
                        new Stat
                        {
                            Day = 31,
                            TimeStamp = new DateTime(2019, 1, 11, 23, 59, 59),
                            Average = 1335,
                            Min = 1330,
                            Max = 1340,
                            Count = 2001,
                            Cv = 0.75,
                            Sigma = 50,
                            Uniformity = 100,
                            Gain = 10,
                            Sex = Veit.Bat.Common.Sex.Undefined
                        },
                        new Stat
                        {
                            Day = 32,
                            TimeStamp = new DateTime(2019, 1, 12, 23, 59, 59),
                            Average = 1339,
                            Min = 1334,
                            Max = 1347,
                            Count = 1850,
                            Cv = 0.9,
                            Sigma = 60,
                            Uniformity = 97,
                            Gain = 9,
                            Sex = Veit.Bat.Common.Sex.Undefined
                        }
                    }
                }
            };
        }

        #endregion
    }
#pragma warning restore CA1308 // Normalize strings to uppercase
}
