﻿namespace Bird.Model.Enums
{
    public enum DateType
    {
        Day = 0,
        Week = 1
    }
}
