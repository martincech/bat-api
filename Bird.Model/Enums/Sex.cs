﻿namespace Bird.Model.Enums
{
    public enum Sex
    {
        Irrelevant = 0,
        Male = 1,
        Female = 2
    }
}
