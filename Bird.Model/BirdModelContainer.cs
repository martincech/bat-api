﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Bird.Model
{
    public class BirdModelContainer : DbContext, IDesignTimeDbContextFactory<BirdModelContainer>
    {
        #region Constructors

        public BirdModelContainer()
        {
        }

        public BirdModelContainer(DbContextOptions<BirdModelContainer> options)
            : base(options)
        {
        }

        #endregion

        public BirdModelContainer CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<BirdModelContainer>();
            builder.UseSqlServer("data source=localhost;initial catalog=VeitBatCloudBirds;integrated security=False;user id=sa;password=Passw0rd!;multipleactiveresultsets=True;application name=Web");
            return new BirdModelContainer(builder.Options);
        }


        public IQueryable<Models.Company> CompaniesLazy => Companies
            .Include(i => i.Products)
                .ThenInclude(tt => tt.Birds)
                    .ThenInclude(t => t.CurvePoints);
        public IQueryable<Models.Product> ProductsLazy => Products
            .Include(i => i.Birds)
                .ThenInclude(t => t.CurvePoints)
            .Include(i => i.Company);
        public IQueryable<Models.Bird> BirdsLazy => Birds
            .Include(i => i.CurvePoints)
            .Include(i => i.Product)
                .ThenInclude(tt => tt.Company);



        public virtual DbSet<Models.Company> Companies { get; set; }
        public virtual DbSet<Models.Product> Products { get; set; }
        public virtual DbSet<Models.Bird> Birds { get; set; }
    }
}
