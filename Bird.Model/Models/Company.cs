﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bird.Model.Models
{
    public partial class Company
    {
        public Guid Id { get; set; }

        public Company()
        {
            Products = new List<Product>();
        }


        [Required]
        public string Name { get; set; }


        public virtual ICollection<Product> Products { get; }
    }
}
