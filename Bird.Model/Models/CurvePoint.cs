﻿namespace Bird.Model.Models
{
    public partial class CurvePoint
    {
        public int Id { get; set; }

        public int Day { get; set; }
        public int Weight { get; set; }
    }
}
