﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Bird.Model.Enums;

namespace Bird.Model.Models
{
#pragma warning disable CA1724 // Type names should not match namespaces
    public partial class Bird
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        public Bird()
        {
            CurvePoints = new List<CurvePoint>();
        }

        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public Sex Sex { get; set; }
        public int Duration { get; set; }
        public DateType DateType { get; set; }


        public virtual Product Product { get; set; }
        public virtual ICollection<CurvePoint> CurvePoints { get; }
    }
}
