﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bird.Model.Models
{
    public partial class Product
    {
        public Guid Id { get; set; }

        public Product()
        {
            Birds = new List<Bird>();
        }

        [Required]
        public string Name { get; set; }


        public virtual Company Company { get; set; }
        public virtual ICollection<Bird> Birds { get; }
    }
}
