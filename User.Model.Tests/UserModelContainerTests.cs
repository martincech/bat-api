﻿using System;
using Microsoft.EntityFrameworkCore;
using User.Model.Models;
using Xunit;

namespace User.Model.Tests
{
    public sealed class UserModelContainerTests : IDisposable
    {
        private readonly UserModelContainer container;

        public UserModelContainerTests()
        {
            var options = new DbContextOptionsBuilder<UserModelContainer>()
                .UseInMemoryDatabase(databaseName: "UserModelContainerTests_InMemoryDB")
                .Options;

            container = new UserModelContainer(options);
        }

        public async void Dispose()
        {
            container.Roles.RemoveRange(container.Roles);
            container.Users.RemoveRange(container.Users);
            container.Companies.RemoveRange(container.Companies);

            await container.SaveChangesAsync();
            container.Dispose();
            GC.SuppressFinalize(this);
        }




        [Fact]
        public async void AddUserWithRolesAsync()
        {
            // Arrange
            var expectedRole1 = new Role { Name = "Administrator" };
            var expectedRole2 = new Role { Name = "Super Administrator" };

            var expectedUser = new Models.User();
            expectedUser.UserRoles.Add(new UserRole { Role = expectedRole1 });
            expectedUser.UserRoles.Add(new UserRole { Role = expectedRole2 });

            // Act
            container.Users.Add(expectedUser);
            container.SaveChanges();

            // Assert
            var actualUser = await container.Users.FirstOrDefaultAsync();
            Assert.NotNull(actualUser);
            Assert.Equal(2, actualUser.UserRoles.Count);
        }
    }
}
