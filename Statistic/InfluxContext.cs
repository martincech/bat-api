﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using InfluxData.Net.Common.Enums;
using InfluxData.Net.InfluxDb;
using InfluxData.Net.InfluxDb.Models.Responses;
using NLog;
using Statistic.Extensions;
using Statistic.Models;
using Statistic.Models.Enums;

namespace Statistic
{
    public sealed class InfluxContext : IStatisticContext, IDisposable
    {
        private readonly SemaphoreSlim readStatsLock = new SemaphoreSlim(1, 1);
        private readonly IInfluxDbClient client;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly InfluxParser parser = new InfluxParser();
        private static readonly string MicrosecondsPadding = new string('0', 6);

        private readonly Dictionary<string, string> retentionPolicies = new Dictionary<string, string>
        {
            { "1440_m", "260w" }    // 5 years
        };


        public InfluxContext(Uri influxUrl)
        {
            client = new InfluxDbClient(influxUrl.ToString(), "name", "pass", InfluxDbVersion.Latest);
        }


        public async Task<bool> ExistCompanyContextAsync(string companyId)
        {
            try
            {
                var databases = await client.Database.GetDatabasesAsync();
                return databases.Any(d => d.Name == companyId);
            }
            catch (Exception e)
            {
                logger.Error(e, "Error occurred when checking if influx db exist: {0}", companyId);
                return false;
            }
        }

        public async Task<bool> CreateCompanyContextAsync(string companyId)
        {
            logger.Info(CultureInfo.InvariantCulture, "Create influx database: {0}", companyId);
            try
            {
                var response = await client.Database.CreateDatabaseAsync(companyId);
                var result = response.StatusCode == HttpStatusCode.OK;
                if (result && !await CreateRetentionPoliciesAsync(companyId))
                {
                    return false;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Fatal(e, "Failed to create influx database {0}", companyId);
            }
            return false;
        }

        private async Task<bool> CreateRetentionPoliciesAsync(string companyId)
        {
            var isFirst = true;
            foreach (var policy in retentionPolicies)
            {
                var query = $"CREATE RETENTION POLICY \"{policy.Key}\" ON \"{companyId}\" DURATION {policy.Value} REPLICATION 1";
                if (isFirst)
                {
                    query += " DEFAULT";
                    isFirst = false;
                }

                var response = await client.RequestClient.PostQueryAsync(query, companyId);
                if (!response.Success)
                {
                    logger.Error($"Cannot create retention policy: {policy.Key}, for company: {companyId}, status code: {response.StatusCode}");
                    return false;
                }
            }
            return true;
        }

        public async Task<bool> DropCompanyContextAsync(string companyId)
        {
            logger.Info(CultureInfo.InvariantCulture, "Drop influx database: {0}", companyId);
            try
            {
                var response = await client.Database.DropDatabaseAsync(companyId);
                return response.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                logger.Error(e, "Exception occurred when deleting influx database: {0}", companyId);
                return false;
            }
        }


        #region Statistics

        public async Task<GroupData> ReadAllWeightStatisticsAsync(string companyId,
                                                                  Guid id,
                                                                  StatisticGroup group,
                                                                  AggregationInterval interval,
                                                                  DateTimeOffset? from,
                                                                  DateTimeOffset? till)
        {
            var queryModel = new QueryModel
            {
                Group = group,
                Interval = interval,
                From = from,
                Till = till,
                Type = SensorType.Weight,
                OnlyLastStat = false
            };
            queryModel.Ids.Add(id);
            var influxData = await ReadStatisticsAsync(companyId, queryModel);
            return parser.ParseWeightStatistics(influxData).FirstOrDefault();
        }

        public async Task<IEnumerable<GroupData>> ReadLastWeightStatisticsAsync(string companyId,
                                                               IEnumerable<Guid> ids,
                                                               StatisticGroup group,
                                                               AggregationInterval interval)
        {
            //for multiple ids is not restriction by time from, to => each device can have different last sample date time
            var queryModel = new QueryModel
            {
                Group = group,
                Interval = interval,
                From = null,
                Till = null,
                Type = SensorType.Weight,
                OnlyLastStat = true
            };
            foreach (var id in ids)
            {
                queryModel.Ids.Add(id);
            }

            var influxData = await ReadStatisticsAsync(companyId, queryModel);
            return parser.ParseWeightStatistics(influxData);
        }


        private async Task<IEnumerable<Serie>> ReadStatisticsAsync(string companyId, QueryModel model)
        {
            if (model?.Ids == null || !model.Ids.Any()) return null;
            //prepare query for influx
            var columns = string.Join(",", AggregationTypeExtensions.AggregationLowerNames().Select(s => $"\"{s}\""));
            var measurement = CreateMeasurementString(model.Ids, model.Group, model.Interval, model.Type);
            var where = CreateWhereClause(model.From, model.Till);
            var groupByParameter = model.Type == SensorType.Weight ? "sex" : "*";
            var last = model.OnlyLastStat ? " ORDER BY time DESC LIMIT 1" : string.Empty;

            var query = $"SELECT {columns} FROM {measurement}{where}{" GROUP BY " + groupByParameter}{last}";
            return await ReadStatisticsAsync(query, companyId);
        }


        private static string CreateWhereClause(DateTimeOffset? from, DateTimeOffset? to)
        {
            if (!from.HasValue && !to.HasValue)
            {
                return string.Empty;
            }

            var whereFrom = from.HasValue ? $"time>={from.Value.ToUnixTimeMilliseconds()}{MicrosecondsPadding}" : "";
            var whereTo = to.HasValue ? $"time<={to.Value.ToUnixTimeMilliseconds()}{MicrosecondsPadding}" : "";

            var union = from.HasValue && to.HasValue ? " AND " : string.Empty;
            return $" WHERE {whereFrom}{union}{whereTo}";
        }

        /// <summary>
        /// Create measurement(s) string for query in Influx DB.
        /// Example: \"1440_m\".\"device_916cbeba-056a-4dbb-966e-c703c911d21b_weight_1440m\"
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="group"></param>
        /// <param name="interval"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string CreateMeasurementString(IEnumerable<Guid> ids, StatisticGroup group, AggregationInterval interval, SensorType type)
        {
            if (ids is null)
            {
                return string.Empty;
            }

            var minutes = interval.MapToMinutes();
            var retentionPolicyName = $"\"{minutes}_m\"";
            var groupName = group.ToLowerInvariant();
            var typeName = type.ToLowerInvariant();

            var measurements = new List<string>();
            foreach (var id in ids)
            {
                measurements.Add($"{retentionPolicyName}.\"{groupName}_{id}_{typeName}_{minutes}m\"");
            }
            return string.Join(",", measurements);
        }


        private async Task<IEnumerable<Serie>> ReadStatisticsAsync(string query, string databaseName)
        {
            IEnumerable<Serie> influxData = null;
            await readStatsLock.WaitAsync();
            try
            {
                influxData = await client.Client.QueryAsync(query, databaseName);
            }
            catch (Exception e)
            {
                logger.Error(e, "Influx DB error");
                return null;
            }
            finally
            {
                readStatsLock.Release();
            }
            return influxData;
        }

        #endregion


        public void Dispose()
        {
            readStatsLock.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
