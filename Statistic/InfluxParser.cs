﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using InfluxData.Net.InfluxDb.Models.Responses;
using NLog;
using Statistic.Extensions;
using Statistic.Models;
using Statistic.Models.Enums;
using Veit.Bat.Common;

namespace Statistic
{
    public class InfluxParser
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private const char NAME_SEPARATOR = '_';
        private const int GROUP_NAME_PARTS = 4;
        private const int GROUP_NAME_TYPE_INDEX = 0;
        private const int GROUP_NAME_ID_INDEX = 1;
        private const int GROUP_NAME_DATA_TYPE_INDEX = 2;
        private const int GROUP_NAME_TIME_SIZE_INDEX = 3;
        private const int STATISTIC_VALUES_COUNT = 11;

        private int statDayIndex;
        private int statTimeIndex;
        private int statAverageIndex;
        private int statMinIndex;
        private int statMaxIndex;
        private int statCountIndex;
        private int statCvIndex;
        private int statSigmaIndex;
        private int statUniformityIndex;
        private int statGainIndex;
        private int statSexIndex;


        public IEnumerable<GroupData> ParseWeightStatistics(IEnumerable<Serie> series)
        {
            var data = new List<GroupData>();
            if (series is null || !series.Any())
            {
                return data;
            }

            foreach (var deviceGroup in series.GroupBy(g => g.Name))
            {
                data.Add(ParseDeviceSeries(deviceGroup));
            }
            return data;
        }

        private GroupData ParseDeviceSeries(IEnumerable<Serie> series)
        {
            var data = InitGroupData(series.FirstOrDefault().Name);
            data.Values = ParseSerieValues(series);
            return data;
        }

        private GroupData InitGroupData(string name)
        {
            var parts = name.Split(NAME_SEPARATOR);
            if (parts.Count() != GROUP_NAME_PARTS)
            {
                ParseError($"{name}: has no appropriate format for group data");
            }
            var sensorType = parts[GROUP_NAME_DATA_TYPE_INDEX];
            if (sensorType != SensorType.Weight.ToLowerInvariant())
            {
                ParseError($"Unexpected data type. Expected: weight, received: {sensorType}.");
            }
            var statisticGroup = MapStatisticGroup(parts[GROUP_NAME_TYPE_INDEX]);
            var minutes = int.Parse(parts[GROUP_NAME_TIME_SIZE_INDEX].TrimEnd('m'), CultureInfo.InvariantCulture);

            var idString = parts[GROUP_NAME_ID_INDEX];
            if (!Guid.TryParse(idString, out var id))
            {
                ParseError($"{idString}: is not valid {typeof(Guid)}");
            }

            return new GroupData
            {
                Id = id.ToString(),
                Group = statisticGroup,
                TimeSize = AggregationIntervalExtensions.MapMinutesToEnum(minutes)
            };
        }

        private StatisticGroup MapStatisticGroup(string item)
        {
            if (!Enum.TryParse<StatisticGroup>(item, true, out var value))
            {
                ParseError($"{item} is not valid statistic group");
            }
            return value;
        }


        private IEnumerable<Stat> ParseSerieValues(IEnumerable<Serie> series)
        {
            var stats = new List<Stat>();
            foreach (var serie in series)
            {
                ColumnMappings(serie.Columns);
                stats.AddRange(ParseSerieValuesToStat(serie.Values));
            }
            return stats;
        }

        private void ColumnMappings(IList<string> columns)
        {
            if (columns is null || !columns.Any())
            {
                ParseError("Statistics, columns are missing");
            }

            FindColumn(ref statDayIndex, AggregationType.Day, columns);
            FindColumn(ref statTimeIndex, AggregationType.Time, columns);
            FindColumn(ref statAverageIndex, AggregationType.Average, columns);
            FindColumn(ref statMinIndex, AggregationType.Min, columns);
            FindColumn(ref statMaxIndex, AggregationType.Max, columns);
            FindColumn(ref statCountIndex, AggregationType.Count, columns);
            FindColumn(ref statCvIndex, AggregationType.Cv, columns);
            FindColumn(ref statSigmaIndex, AggregationType.Sigma, columns);
            FindColumn(ref statUniformityIndex, AggregationType.Uniformity, columns);
            FindColumn(ref statGainIndex, AggregationType.Gain, columns);
            FindColumn(ref statSexIndex, AggregationType.Sex, columns);
        }

        private void FindColumn(ref int index, AggregationType type, IList<string> columns)
        {
            index = columns.IndexOf(type.ToLowerInvariant());
            if (index < 0)
            {
                ParseError($"Statistic parsing error. Column: {type} is missing");
            }
        }

        private IEnumerable<Stat> ParseSerieValuesToStat(IList<IList<object>> values)
        {
            if (values is null)
            {
                ParseError("Statistics has no values");
            }
            return values.Select(s => ParseSerieValuesToStat(s)).ToList();
        }

        private Stat ParseSerieValuesToStat(IList<object> values)
        {
            if (values.Count < STATISTIC_VALUES_COUNT)
            {
                ParseError("Invalid mount of values");
            }

            return new Stat
            {
                Day = CastValue<int?>(values[statDayIndex]),
                TimeStamp = CastTimeStamp(values[statTimeIndex]),
                Average = CastValue<double?>(values[statAverageIndex]),
                Min = CastValue<double?>(values[statMinIndex]),
                Max = CastValue<double?>(values[statMaxIndex]),
                Count = CastValue<int?>(values[statCountIndex]),
                Cv = CastValue<double?>(values[statCvIndex]),
                Sigma = CastValue<double?>(values[statSigmaIndex]),
                Uniformity = CastValue<double?>(values[statUniformityIndex]),
                Gain = CastValue<double?>(values[statGainIndex]),
                Sex = ParseSex(values[statSexIndex])
            };
        }

        private Sex? ParseSex(object data)
        {
            if (data == null)
            {
                return null;
            }

            if (!Enum.TryParse<Sex>(data.ToString(), true, out var sex))
            {
                logger.Warn($"Parse error, invalid sex value: {data}");
                return Sex.Undefined;
            }
            return sex;
        }

        private T CastValue<T>(object obj)
        {
            try
            {
                var t = typeof(T);
                var u = Nullable.GetUnderlyingType(t);

                if (u != null)
                {
                    return (obj == null) ? default(T) : (T)Convert.ChangeType(obj, u, CultureInfo.InvariantCulture);
                }
                return (T)Convert.ChangeType(obj, t, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                ParseError($"Parse error: {e}");
                return default(T);
            }
        }

        private static DateTimeOffset CastTimeStamp(object timeStamp)
        {
            if (timeStamp == null) return new DateTimeOffset();
            return typeof(DateTime).Equals(timeStamp.GetType()) ? (DateTime)timeStamp : (DateTimeOffset)timeStamp;
        }

        private void ParseError(string message)
        {
            logger.Error(message);
            throw new ArgumentException(message);
        }
    }
}
