﻿using System;
using System.Collections.Generic;
using Statistic.Models;

namespace Statistic
{
    public interface IGroupProducer
    {
        bool UpdateDeviceGroups(Dictionary<Guid, IEnumerable<FlockEvent>> dictionary);
    }
}
