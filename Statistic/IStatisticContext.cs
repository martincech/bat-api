﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Statistic.Models;
using Statistic.Models.Enums;

namespace Statistic
{
    public interface IStatisticContext
    {
        Task<bool> ExistCompanyContextAsync(string companyId);
        Task<bool> CreateCompanyContextAsync(string companyId);
        Task<bool> DropCompanyContextAsync(string companyId);


        Task<GroupData> ReadAllWeightStatisticsAsync(string companyId, Guid id, StatisticGroup group, AggregationInterval interval, DateTimeOffset? from, DateTimeOffset? till);
        Task<IEnumerable<GroupData>> ReadLastWeightStatisticsAsync(string companyId, IEnumerable<Guid> ids, StatisticGroup group, AggregationInterval interval);
    }
}
