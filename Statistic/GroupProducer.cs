﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Statistic.Models;
using Veit.Kafka;
using Veit.Kafka.Topics;

namespace Statistic
{
    /// <summary>
    /// Middle-ware to provide transformation of database relations between Devices and statistical groups (flocks) to Kafka producer.
    /// </summary>
    public class GroupProducer : IGroupProducer
    {
        private const string FLOCK_GROUP_FORMAT = "flock_{0}";
        private const string DEVICE_GROUP_FORMAT = "device_{0}";

        private readonly IKafkaSender producer;
        private readonly string company;

        public GroupProducer(IKafkaSender producer, string company)
        {
            this.producer = producer;
            this.company = company;
        }


        /// <summary>
        /// Update devices statistics groups.
        /// </summary>
        /// <param name="dictionary">key: device id, value: flocks that used device</param>
        /// <returns></returns>
        public bool UpdateDeviceGroups(Dictionary<Guid, IEnumerable<FlockEvent>> dictionary)
        {
            if (dictionary == null)
            {
                return true;
            }

            var groups = new List<DeviceGroup>();
            foreach (var item in dictionary)
            {
                groups.Add(CreateDeviceGroup(item.Key, item.Value));
            }
            return UpdateGroups(groups);
        }


        #region Private helpers

        private DeviceGroup CreateDeviceGroup(Guid deviceId, IEnumerable<FlockEvent> flocks)
        {
            var statGroups = new List<StatGroup>();
            var id = deviceId.ToString();
            foreach (var flock in flocks)
            {
                statGroups.Add(CreateStatGroup(id, flock.From, flock.To, DEVICE_GROUP_FORMAT));
                statGroups.Add(CreateStatGroup(flock.Id.ToString(), flock.From, flock.To, FLOCK_GROUP_FORMAT));
            }
            return new DeviceGroup { Database = company, Groups = statGroups, Uid = id };
        }



        private static StatGroup CreateStatGroup(string group, DateTimeOffset from, DateTimeOffset to, string format = null)
        {
            return new StatGroup { Group = format == null ? group : string.Format(CultureInfo.InvariantCulture, format, group), From = from, To = to };
        }

        /// <summary>
        /// Update device statistic groups in Kafka.
        /// </summary>
        /// <param name="groups"></param>
        /// <returns></returns>
        private bool UpdateGroups(List<DeviceGroup> groups)
        {
            if (groups == null) return true;
            return producer.Send(groups);
        }

        #endregion
    }
}
