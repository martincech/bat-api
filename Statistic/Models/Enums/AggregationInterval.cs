﻿using System;

namespace Statistic.Models.Enums
{
    public enum AggregationInterval
    {
        HalfHour,
        Day,
    }


    public static class AggregationIntervalExtensions
    {
        public static TimeSpan MapToTimeSpan(this AggregationInterval item)
        {
            switch (item)
            {
                case AggregationInterval.Day:
                    return new TimeSpan(1, 0, 0, 0);
                case AggregationInterval.HalfHour:
                    return new TimeSpan(0, 30, 0);
                default:
                    return new TimeSpan(0);
            }
        }


        public static int MapToMinutes(this AggregationInterval item)
        {
            return (int)item.MapToTimeSpan().TotalMinutes;
        }

        public static AggregationInterval MapMinutesToEnum(int minutes)
        {
            switch(minutes)
            {
                case 30:
                    return AggregationInterval.HalfHour;
                case 1440:
                    return AggregationInterval.Day;
                default:
                    throw new ArgumentException($"Argument: {nameof(minutes)} is out of range");
            }
        }
    }
}
