﻿using System;
using System.Collections.Generic;
using System.Linq;
using Statistic.Extensions;

namespace Statistic.Models.Enums
{
    public enum AggregationType
    {
        Time,
        Day,
        Average,
        Count,
        Min,
        Max,
        Sigma,
        Cv,
        Uniformity,
        Gain,
        Sex
    }

    public static class AggregationTypeExtensions
    {
        public static IEnumerable<AggregationType> AggregationTypes()
        {
            return Enum.GetValues(typeof(AggregationType)).Cast<AggregationType>();
        }

        public static IEnumerable<string> AggregationLowerNames()
        {
            return AggregationTypes().Select(s => s.ToLowerInvariant());
        }
    }
}
