﻿namespace Statistic.Models.Enums
{
    public enum SensorType
    {
        Weight,
        Temperature,
        Humidity,
        Co2
    }
}
