﻿using System;
using System.Collections.Generic;
using Statistic.Models.Enums;

namespace Statistic.Models
{
    public class QueryModel
    {
        public QueryModel()
        {
            Ids = new List<Guid>();
        }

        public ICollection<Guid> Ids { get; }


        public StatisticGroup Group { get; set; }
        public AggregationInterval Interval { get; set; }

        public DateTimeOffset? From { get; set; }
        public DateTimeOffset? Till { get; set; }
        public SensorType Type { get; set; }
        public bool OnlyLastStat { get; set; }
    }
}
