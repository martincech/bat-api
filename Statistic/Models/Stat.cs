﻿using System;
using Veit.Bat.Common;

namespace Statistic.Models
{
    public class Stat
    {
        public int? Day { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public double? Average { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }
        public int? Count { get; set; }
        public double? Cv { get; set; }
        public double? Sigma { get; set; }
        public double? Uniformity { get; set; }
        public double? Gain { get; set; }
        public Sex? Sex { get; set; }
    }
}
