﻿using System.Collections.Generic;
using Statistic.Models.Enums;

namespace Statistic.Models
{
    public class GroupData
    {
        public GroupData()
        {
            Values = new List<Stat>();
        }

        public string Id { get; set; }
        public StatisticGroup Group { get; set; }
        public AggregationInterval TimeSize { get; set; }


        public IEnumerable<Stat> Values { get; set; }
    }
}
