﻿using System;

namespace Statistic.Models
{
    public class FlockEvent
    {
        public Guid Id { get; set; }
        public DateTimeOffset From { get; set; }
        public DateTimeOffset To { get; set; }
    }
}
