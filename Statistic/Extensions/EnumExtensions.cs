﻿using System;

namespace Statistic.Extensions
{
    public static class EnumExtensions
    {
        public static string ToLowerInvariant(this Enum item)
        {
#pragma warning disable CA1308 // Normalize strings to uppercase
            return item.ToString().ToLowerInvariant();
#pragma warning restore CA1308 // Normalize strings to uppercase
        }
    }
}
