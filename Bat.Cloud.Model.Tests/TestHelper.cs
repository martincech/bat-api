﻿using System.Collections.Generic;
using System.Linq;

namespace Bat.Cloud.Model.Tests
{
    public static class TestHelper
    {
        public static bool PreCheckCollectionForNull<T>(IEnumerable<T> t1, IEnumerable<T> t2)
        {
            if (!PreCheckForNull(t1, t2))
            {
                return false;
            }
            if (t1.Count() != t2.Count())
            {
                return false;
            }
            return true;
        }

        public static bool PreCheckForNull<T>(T t1, T t2)
        {
            if (t1 == null && t2 == null)
            {
                return true;
            }
            if ((t1 == null && t2 != null) ||
                (t1 != null && t2 == null))
            {
                return false;
            }
            return true;
        }
    }
}
