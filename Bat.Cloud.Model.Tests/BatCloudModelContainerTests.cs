﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat.Cloud.Model.Models;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Bat.Cloud.Model.Tests
{
    /// <summary>
    /// Test relationships between tables. Cascading deletes and so on.
    /// </summary>
    public sealed class BatCloudModelContainerTests : IDisposable
    {
        private readonly BatCloudModelContainer container;

        public BatCloudModelContainerTests()
        {
            var options = new DbContextOptionsBuilder<BatCloudModelContainer>()
                .UseInMemoryDatabase(databaseName: "BatCloudModelContainerTests_InMemoryDB")
                .Options;

            container = new BatCloudModelContainer(options);
        }

        public async void Dispose()
        {
            container.Houses.RemoveRange(container.Houses);
            container.Farms.RemoveRange(container.Farms);

            await container.SaveChangesAsync();
            container.Dispose();
            GC.SuppressFinalize(this);
        }



        #region Farm tests

        [Fact]
        public async void DeleteFarm_CascadeDeleteItHousesAsync()
        {
            // Arrange
            var farm = new Farm
            {
                Id = Guid.NewGuid(),
                Name = "Delete farm test X",
                Address = "QWERTZ Street"
            };
            container.Farms.Add(farm);
            var houses = new List<House>
            {
                new House {Name = "H1", FarmId = farm.Id},
                new House {Name = "H2", FarmId = farm.Id},
            };
            container.Houses.AddRange(houses);
            container.SaveChanges();

            // Act
            container.Farms.Remove(farm);
            await container.SaveChangesAsync();

            // Assert
            Assert.Empty(container.Houses);
        }

        #endregion

        #region Lazy loading tests

        [Fact]
        public void Houses_LazyLoading()
        {
            // Arrange
            var expectedFarm = GetDemoFarm();
            var expectedHouse = GetDemoHouse();
            container.Farms.Add(expectedFarm);
            container.Houses.Add(expectedHouse);
            container.SaveChanges();

            // Act
            var house = container.HousesLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(house);
            Assert.True(IsHouseSame(expectedHouse, house));
        }

        [Fact]
        public void Devices_LazyLoading()
        {
            // Arrange
            var expectedDevice = GetDemoDevice();
            container.Devices.Add(expectedDevice);
            container.SaveChanges();

            // Act
            var device = container.DevicesLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(device);
            Assert.True(IsDeviceSame(expectedDevice, device));
        }


        [Fact]
        public void Flocks_LazyLoading()
        {
            // Arrange
            var expectedFlock = GetDemoFlock();
            container.Flocks.Add(expectedFlock);
            container.SaveChanges();

            // Act
            var flock = container.FlocksLazy.FirstOrDefault();

            // Assert
            Assert.NotNull(flock);
            Assert.True(IsFlockSame(expectedFlock, flock));
        }

        #region Demo data

        private static Flock GetDemoFlock()
        {
            return new Flock
            {
                Name = "Demo Flock Q",
                InitialAge = 45,
                InitialWeight = 2450,
                StartDate = new DateTime(2018, 11, 20),
                EndDate = null,
                BirdId = Guid.NewGuid(),
                House = GetDemoHouse(),
                Device = GetDemoDevice()
            };
        }

        private static Farm GetDemoFarm()
        {
            return new Farm
            {
                Name = "FFFF 01",
                ContactName = "Hans Zimmer",
                Email = "hz@mail.com",
                Phone = "+420 600 987 654"
            };
        }

        private static House GetDemoHouse()
        {
            return new House
            {
                Name = "House lazy",
                Farm = GetDemoFarm()
            };
        }

        private static Device GetDemoDevice()
        {
            return new Device
            {
                Name = "Device 666",
                Type = Enums.DeviceType.Bat2GsmScale,
                PhoneNumber = "+420123456789",
                Description = "some text ...",
                Farm = GetDemoFarm(),
                House = GetDemoHouse()
            };
        }

        private static bool IsFlockSame(Flock f1, Flock f2)
        {
            if (!TestHelper.PreCheckForNull(f1, f2))
            {
                return false;
            }

            return f1.Name == f2.Name &&
                   f1.InitialAge == f2.InitialAge &&
                   f1.InitialWeight == f2.InitialWeight &&
                   DateTimeOffset.Compare(f1.StartDate, f2.StartDate) == 0 &&
                   CmpNullableDateTime(f1.EndDate, f2.EndDate) &&
                   f1.BirdId == f2.BirdId &&
                   IsHouseSame(f1.House, f2.House) &&
                   IsDeviceSame(f1.Device, f2.Device);
        }

        private static bool CmpNullableDateTime(DateTimeOffset? d1, DateTimeOffset? d2)
        {
            if ((d1.HasValue && !d2.HasValue) ||
               (!d1.HasValue && d2.HasValue))
            {
                return false;
            }
            if (!d1.HasValue && !d2.HasValue)
            {
                return true;
            }
            return DateTimeOffset.Compare(d1.Value, d2.Value) == 0;
        }

        private static bool IsDeviceSame(Device d1, Device d2)
        {
            if (!TestHelper.PreCheckForNull(d1, d2))
            {
                return false;
            }
            return d1.Name == d2.Name &&
                   d2.Type == d2.Type &&
                   d1.Description == d2.Description &&
                   d1.PhoneNumber == d2.PhoneNumber &&
                   IsFarmSame(d1.Farm, d2.Farm) &&
                   IsHouseSame(d1.House, d2.House);
        }

        private static bool IsHouseSame(House h1, House h2)
        {
            if (!TestHelper.PreCheckForNull(h1, h2))
            {
                return false;
            }
            return h1.Name == h2.Name && IsFarmSame(h1.Farm, h2.Farm);
        }

        private static bool IsFarmSame(Farm f1, Farm f2)
        {
            if (!TestHelper.PreCheckForNull(f1, f2))
            {
                return false;
            }

            return f1.Name == f2.Name &&
                   f1.Address == f2.Address &&
                   f1.Country == f2.Country;
        }

        #endregion

        #endregion
    }
}
